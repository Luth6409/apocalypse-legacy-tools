﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace WarPatcherUI
{
    public class HttpUtil
    {
        public async Task<TResult> Request<TResult, TSend>(string address, TSend d)
        {
            string path = $@"http://{address}/{typeof(TSend).Name}";
            Log.Info($"Request {path}");
            var request = HttpWebRequest.Create(path);
            var body = GetDataArray(d);
            request.Proxy = null;
            request.ContentLength = (long)body.Length;
            request.ContentType = "application/json";
            request.Method = "POST";
            var bodyStream = request.GetRequestStream();
            bodyStream.Write(body, 0, body.Length);

            var response = await request.GetResponseAsync();

            using (var stream = response.GetResponseStream())
            {
                var reader = new StreamReader(stream);
                var result = (TResult)new JavaScriptSerializer() { MaxJsonLength = int.MaxValue }
                    .Deserialize(reader.ReadToEnd(), typeof(TResult));
                return result;
            }
        }

        public async Task<TResult> Request<TResult>(string address, params (string, object)[] parameters)
        {
            try
            {
                string req = $@"http://{address}/?";


                foreach (var t in parameters)
                {
                    req += t.Item1 + "=" + t.Item2 + "&";
                }

                Log.Info($"Request {req}");

                var request = HttpWebRequest.Create(req);

                request.Proxy = null;
                request.ContentType = "application/json";
                request.Method = "POST";

                var response = await request.GetResponseAsync();

                using (var stream = response.GetResponseStream())
                {
                    var reader = new StreamReader(stream);

                    return (TResult)new JavaScriptSerializer() { MaxJsonLength = int.MaxValue }
                        .Deserialize(reader.ReadToEnd(), typeof(TResult));
                }
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return default(TResult);
        }

        public  string Request(string address)
        {
            try
            {
                System.Net.ServicePointManager.DefaultConnectionLimit = 1600;
                string path = $@"http://{address}";
                Log.Info($"Request {path}");

                var request = HttpWebRequest.Create(path);

                request.Proxy = null;
                request.Method = "POST";
                request.ContentLength = 0;
                var response = request.GetResponse();

                using (var stream = response.GetResponseStream())
                {
                    var reader = new StreamReader(stream);
                    return reader.ReadToEnd();
                }
            }

            catch (Exception e)
            {
                Log.Exception(e.Message, e);
            }

            return "";
        }

        public async Task<string> RequestAsync(string address)
        {
            try
            {
                System.Net.ServicePointManager.DefaultConnectionLimit = 1600;
                string path = $@"http://{address}";
                Log.Info($"Request {path}");

                var request = HttpWebRequest.Create(path);

                request.Proxy = null;
                request.Method = "POST";
                request.ContentLength = 0;
                var response = await request.GetResponseAsync();

                using (var stream = response.GetResponseStream())
                {
                    var reader = new StreamReader(stream);
                    return reader.ReadToEnd();
                }
            }

            catch (Exception e)
            {
                Log.Exception(e.Message, e);
            }

            return "";
        }

        public async Task<TResult> Request<TResult>(string address)
        {
            try
            {
                System.Net.ServicePointManager.DefaultConnectionLimit = 1600;
                string path = $@"http://{address}/{typeof(TResult).Name}";

                var request = HttpWebRequest.Create(path);

                Log.Info($"Request {path}");

                request.Proxy = null;
                request.ContentType = "application/json";
                request.Method = "POST";
                request.ContentLength = 0;
                var response = await request.GetResponseAsync();

                using (var stream = response.GetResponseStream())
                {
                    var reader = new StreamReader(stream);
                    return (TResult)new JavaScriptSerializer().Deserialize(reader.ReadToEnd(), typeof(TResult));
                }
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return default(TResult);
        }

        public delegate void StreamProgress(long current, long total, int chunkSize);

        public async Task RequestStreamAsync<TSend>(string address, TSend data, Stream output, StreamProgress progress, int blockSize = 0x8FFF)
        {
            try
            {
                Log.Info(@"http://{address}/{typeof(TSend).Name}");
                var request = HttpWebRequest.Create($@"http://{address}/{typeof(TSend).Name}");
                var body = GetDataArray(data);
                request.ContentLength = (long)body.Length;
                request.ContentType = "application/json";
                request.Method = "POST";
                var bodyStream = request.GetRequestStream();
                bodyStream.Write(body, 0, body.Length);

                var response = await request.GetResponseAsync();

                using (var stream = response.GetResponseStream())
                {

                    long current = 0;
                    long total = response.ContentLength;
                    byte[] block = new byte[blockSize];
                    while (current < total)
                    {
                        int read = stream.Read(block, 0, blockSize);
                        if (read > 0)
                        {
                            output.Write(block, 0, read);
                            current += read;
                            progress(current, total, read);
                        }
                        else
                        {
                            progress(current, total, read);
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void RequestStream<TSend>(string address, TSend data, Stream output, StreamProgress progress, int blockSize = 0x8FFFF)
        {
            try
            {
                var request = HttpWebRequest.Create($@"http://{address}/{typeof(TSend).Name}");
                var body = GetDataArray(data);
                request.ContentLength = (long)body.Length;
                request.ContentType = "application/json";
                request.Method = "POST";
                var bodyStream = request.GetRequestStream();
                bodyStream.Write(body, 0, body.Length);

                var response = request.GetResponse();

                using (var stream = response.GetResponseStream())
                {

                    long current = 0;
                    long total = response.ContentLength;
                    byte[] block = new byte[blockSize];
                    while (current < total)
                    {
                        int read = stream.Read(block, 0, blockSize);
                        if (read > 0)
                        {
                            output.Write(block, 0, read);
                            current += read;
                            progress(current, total, read);
                        }
                        else
                        {
                            progress(current, total, read);
                            break;
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static byte[] GetDataArray(object data)
        {
            return System.Text.Encoding.UTF8.GetBytes(new JavaScriptSerializer().Serialize(data));
        }
    }
}
