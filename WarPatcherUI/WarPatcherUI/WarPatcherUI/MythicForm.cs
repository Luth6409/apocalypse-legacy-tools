﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarPatcherUI
{
    public class MythicForm : Form
    {
        public Bitmap _imgResource;
        public LayoutReader _layout;
        public Bitmap _bg;
        public Graphics _bgG;
        public virtual string _bgName { get; set; }
        public virtual string _windowName { get; set; }
        public FontFamily ButonFont { get; set; }
        PrivateFontCollection _fontCollection = new PrivateFontCollection();
        protected AlphaForm.AlphaFormTransformer alphaFormTransformer1;
        public Dictionary<string, string> _labels = new Dictionary<string, string>();

        private void LoadLabels(string labels)
        {
            var bytes = System.Text.UnicodeEncoding.Unicode.GetBytes(System.Text.UnicodeEncoding.Unicode.GetString(ReadResource(labels)));

            var str = new StreamReader(new MemoryStream(bytes));
            while (!str.EndOfStream)
            {
                var line = str.ReadLine();
                string key = "";
                string value = "";
                int split = line.IndexOf('\t');
                if (split > -1)
                {
                    key = line.Substring(0, split);
                    if(split+1 < line.Length)
                    value = line.Substring(split + 1);
                }
                else
                {
                    key = line;
                    value = "";
                }
                _labels[key] = value.Trim();
            }
        }
        public void InitResource(string labels, string xmlResourceName, string imgResourceName, string buttonFont)
        {
            LoadLabels(labels);
            _imgResource = new Bitmap(new MemoryStream(ReadResource(imgResourceName)));
            _layout = new LayoutReader(ReadResource(xmlResourceName));
            LoadLayout();

            ButonFont = LoadFont(ReadResource(buttonFont));
        }

        private unsafe FontFamily LoadFont(byte[] data)
        {
            fixed (byte* ptr = data)
            {
                _fontCollection.AddMemoryFont(new IntPtr(ptr), data.Length);
                return _fontCollection.Families[_fontCollection.Families.Length - 1];
            }

        }

        //protected override void OnPaint(PaintEventArgs e)
        //{
        //    if (DesignMode)
        //    {
        //        base.OnPaint(e);
        //        return;
        //    }

        //    if (_bgG == null)
        //        OnResize(null);

        //   // _bgG.Clear(Color.Gray);

        //    var srcRect = new Rectangle(_layout["source"][_bgName].X, _layout["source"][_bgName].Y,
        //        _layout["source"][_bgName].Width, _layout["source"][_bgName].Height);
        //    var dstRect = new Rectangle(0, 0, srcRect.Width, srcRect.Height);

        //   _bgG.DrawImage(_imgResource, dstRect, srcRect, GraphicsUnit.Pixel);

        //     e.Graphics.DrawImage(_bg, new Rectangle(0, 0, Width, Height), new Rectangle(0, 0, _bg.Width, _bg.Height), GraphicsUnit.Pixel);
        //}

        public void DrawBG(Graphics g, Rectangle rect, int destX, int destY)
        {
            var srcRect = new Rectangle(rect.X + _layout["source"][_bgName].X, rect.Y + _layout["source"][_bgName].Y, rect.Width, rect.Height);
            var dstRect = new Rectangle(destX, destY, rect.Width, rect.Height);

            g.DrawImage(_imgResource, dstRect, srcRect, GraphicsUnit.Pixel);

        }
        protected override void OnResize(EventArgs e)
        {
            if (DesignMode)
                return;

            if (_bgG != null)
            {
                _bg.Dispose();
                _bgG.Dispose();
            }
            _bg = new Bitmap(Width, Height);
            _bgG = Graphics.FromImage(_bg);
            _bgG.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            _bgG.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            base.OnResize(e);
            Invalidate();
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }

        protected virtual void LoadLayout()
        {
            Width = _layout["source"][_bgName].Width;
            Height = _layout["source"][_bgName].Height;

            var controls = new Dictionary<string, IControl>();
            foreach (var control in alphaFormTransformer1.Controls)
            {
                if (control is IControl)
                {
                    controls[((IControl)control).LayoutName] = (IControl)control;
                }
            }
            foreach (var element in _layout[_windowName].Elements.Values)
            {
                if (controls.ContainsKey(element.Name))
                {
                    var control = controls[element.Name];
                    int width = ((Control)control).Width;
                    int height = ((Control)control).Height;

              
                    ((Control)control).Left = _layout[_windowName][control.LayoutName].X;
                    ((Control)control).Top = _layout[_windowName][control.LayoutName].Y;

                    string text = ((Control)control).Text.Replace("[", "").Replace("]", "");

                    if (((Control)control).Text.StartsWith("[") && _labels.ContainsKey(text))
                    {
                        text = _labels[text];
                    }
                    if(!(control is WebBrowser))
                    ((Control)control).Text = text;


                    if (element.Name.Contains("button"))
                    {
                        width = _layout["source"][control.ResourceName]["default"].Width;
                        height = _layout["source"][control.ResourceName]["default"].Height;
                    }
                    else if (element.Name.Contains("progress"))
                    {
                        width = _layout["source"][control.ResourceName]["empty"].Width;
                        height = _layout["source"][control.ResourceName]["empty"].Height;
                    }
                    else if (element.Name.Contains("text"))
                    {
                        width = _layout[_windowName][control.LayoutName].Width;
                        height = _layout[_windowName][control.LayoutName].Height;
                        ((MLabel)control).AutoSize = false;
                        ((MLabel)control).Top += ((MLabel)control).PadTop;
                        ((MLabel)control).Left += ((MLabel)control).PadLeft;
                        ((MLabel)control).AutoEllipsis = true;

                    }

                    else if (element.Name.Contains("browserwindow"))
                    {
                        width = _layout[_windowName][control.LayoutName].Width;
                        height = _layout[_windowName][control.LayoutName].Height;
                    }

                    else if (element.Name.Contains("input"))
                    {
             
                    }

                    ((Control)control).Width = width;
                    ((Control)control).Height = height;
                }

            }

            var srcRect = new Rectangle(_layout["source"][_bgName].X, _layout["source"][_bgName].Y,
                _layout["source"][_bgName].Width, _layout["source"][_bgName].Height);
            var dstRect = new Rectangle(0, 0, srcRect.Width, srcRect.Height);

            var bg = new Bitmap(dstRect.Width, dstRect.Height);
            using (Graphics bgG = Graphics.FromImage(bg))
            {

                bgG.DrawImage(_imgResource, dstRect, srcRect, GraphicsUnit.Pixel);
            }

            alphaFormTransformer1.BackgroundImage = bg;
        }

        public static byte[] ReadResource(string name)
        {
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(name))
            {
                byte[] data = new byte[stream.Length];
                stream.Read(data, 0, data.Length);
                return data;
            }
        }

        protected void InitializeComponent()
        {
            this.alphaFormTransformer1 = new WarPatcherUI.AlphaForm.AlphaFormTransformer();
            this.SuspendLayout();
            // 
            // alphaFormTransformer1
            // 
            this.alphaFormTransformer1.BackgroundImage = global::WarPatcherUI.Properties.Resources.patcher;
            this.alphaFormTransformer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.alphaFormTransformer1.DragSleep = ((uint)(30u));
            this.alphaFormTransformer1.Location = new System.Drawing.Point(0, 0);
            this.alphaFormTransformer1.Name = "alphaFormTransformer1";
            this.alphaFormTransformer1.Size = new System.Drawing.Size(284, 262);
            this.alphaFormTransformer1.TabIndex = 0;
            this.alphaFormTransformer1.Paint += new System.Windows.Forms.PaintEventHandler(this.alphaFormTransformer1_Paint);
            // 
            // MythicForm
            // 
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.alphaFormTransformer1);
            this.Name = "MythicForm";
            this.ResumeLayout(false);

        }
        public MythicForm()
        {
            InitializeComponent();
        }

        private void alphaFormTransformer1_Paint(object sender, PaintEventArgs e)
        {

        }
    }

    public interface IControl
    {
        string ResourceName { get; set; }
        string LayoutName { get; set; }
    }

    public class MBrowser : WebBrowser, IControl
    {
        public string ResourceName { get; set; }
        public string LayoutName { get; set; }

    }
    public class MLabel : Label, IControl
    {
        public int PadLeft { get; set; }
        public int PadTop { get; set; }
        public string ResourceName { get; set; }
        public string LayoutName { get; set; }
        public override Color BackColor { get => Color.Transparent; set => base.BackColor = Color.Transparent; }
    }

    public class MProgressBar : Control, IControl
    {
        public string ResourceName { get; set; }
        public string LayoutName { get; set; }
        public float Value { get; set; } = 25.35f;
        private Graphics _g;
        private Bitmap _backBmp;
        private Graphics _backG;

        public MProgressBar()
        {
        //    SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
   
            SetStyle(ControlStyles.EnableNotifyMessage, true);

        
        }
        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            //base.OnPaintBackground(pevent);
        }
        private void InitG()
        {
            if (_g == null)
                _g = CreateGraphics();

            if (_backG != null)
            {
                _backG.Dispose();
                _backBmp.Dispose();
            }
            _backBmp = new Bitmap(Width, Height);
            _backG = Graphics.FromImage(_backBmp);
            _backG.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            _backG.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

        }
    
        const int WM_ERASEBKGND = 0x0014;
        protected override void WndProc(ref Message m)
        {
            if (m.Msg != WM_ERASEBKGND)
            {
                base.WndProc(ref m);
            }
            else
            {
                m.Result = IntPtr.Zero;
            }
        }

        protected override void OnInvalidated(InvalidateEventArgs e)
        {
            if (e.InvalidRect == this.Bounds)
                Paint();

            base.OnInvalidated(e);
        }



        protected override void OnNotifyMessage(Message m)

	    {

	        if (m.Msg != WM_ERASEBKGND)

	            base.OnNotifyMessage(m);

	    }

        public void Paint(Graphics g = null)
        {
            if (_g == null)
                InitG();

            if (g == null)
                g = _g;

            var form = FindForm() as MythicForm;
            if (form != null && form._layout != null)
            {

                var srcRect = new Rectangle(
                    form._layout["source"][ResourceName]["empty"].X,
                    form._layout["source"][ResourceName]["empty"].Y,
                    form._layout["source"][ResourceName]["empty"].Width,
                    form._layout["source"][ResourceName]["empty"].Height);

                var destRect = new Rectangle(0, 0,
                    form._layout["source"][ResourceName]["empty"].Width,
                    form._layout["source"][ResourceName]["empty"].Height);


                _backG.DrawImage(form._imgResource, destRect, srcRect, GraphicsUnit.Pixel);

                int width = (int)((float)srcRect.Width * Value / 100.0f);

                var srcRectVal = new Rectangle(
                   form._layout["source"][ResourceName]["full"].X,
                   form._layout["source"][ResourceName]["full"].Y,
                   width,
                   form._layout["source"][ResourceName]["full"].Height);

                var destRectVal = new Rectangle(0, 0,
                    width,
                    form._layout["source"][ResourceName]["full"].Height);

                _backG.DrawImage(form._imgResource, destRectVal, srcRectVal, GraphicsUnit.Pixel);

                _g.DrawImage(_backBmp, new Rectangle(0, 0, Width, Height), new Rectangle(0, 0, Width, Height), GraphicsUnit.Pixel);

                return;
            }
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            Paint(pevent.Graphics);
            //pevent.Graphics.Clear(Color.LightGray);
            if (DesignMode)
                base.OnPaint(pevent);

        }
    }

    public class VPanel : Panel
    {
        public TextBox TextBox;
        public override string Text { get => TextBox.Text; set => TextBox.Text = value; }
        public bool WordWrap { get => TextBox.WordWrap; set => TextBox.WordWrap = value; }
   
        public int MaxLength { get => TextBox.MaxLength; set => TextBox.MaxLength = value; }
        public override Color BackColor
        {
            get
            {
                return TextBox.BackColor;
            }
            set
            {
                base.BackColor = value;
                TextBox.BackColor = value;
                Invalidate();
            }
        }
        public override Color ForeColor { get => TextBox.ForeColor ; set => TextBox.ForeColor = value; }
        public char PasswordChar { get => TextBox.PasswordChar; set => TextBox.PasswordChar = value; }
        public VPanel()
        {
            TextBox = new TextBox();
            TextBox.Dock = DockStyle.Fill;
            TextBox.BorderStyle = BorderStyle.None;
            TextBox.Multiline = false;
            TextBox.AcceptsReturn = false;
            TextBox.AcceptsTab = false;
            Controls.Add(TextBox);
        }
    }
    public class MTextBox : VPanel, IControl
    {
        public string ResourceName { get; set; }
        public string LayoutName { get; set; }
    }

        public class MButton : Button, IControl
    {
        public string ResourceName { get; set; }
        public string LayoutName { get; set; }

        private string _state = "default";
        private Font _font = null;
        public Brush EnabledForeColor { get; set; } = Brushes.White;
        public Brush DisabledForeColor { get; set; } = Brushes.Gray;

        public MButton()
        {
         
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
           // if (DesignMode)
                base.OnPaintBackground(pevent);
            
        }

        protected override void OnMouseMove(MouseEventArgs mevent)
        {
            base.OnMouseMove(mevent);
            if (_state != "press")
            {
                _state = "over";
                Refresh();
            }
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            _state = "default";
            Refresh();
        }

        protected override void OnMouseDown(MouseEventArgs mevent)
        {
            base.OnMouseDown(mevent);
            _state = "press";
            Refresh();
        }

        protected override void OnMouseUp(MouseEventArgs mevent)
        {
            base.OnMouseUp(mevent);
            _state = "over";
            Refresh();
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
           
            var form = FindForm() as MythicForm;
            if (form != null && form._layout != null)
            {
                pevent.Graphics.Clear(Color.Black);
                if (_font == null)
                {
                    _font = new Font(form.ButonFont, Font.SizeInPoints, FontStyle.Bold);
                }
                var srcRect = new Rectangle(form._layout["source"][ResourceName][_state].X, form._layout["source"][ResourceName][_state].Y,
                    form._layout["source"][ResourceName][_state].Width, form._layout["source"][ResourceName][_state].Height);

                var destRect = new Rectangle(0, 0,
                    form._layout["source"][ResourceName][_state].Width, form._layout["source"][ResourceName][_state].Height);

                var txt = pevent.Graphics.MeasureString(Text, _font);
                var sz = pevent.Graphics.VisibleClipBounds.Size;
                int padding = 2;
                StringFormat format = new StringFormat();
      
                format.LineAlignment = StringAlignment.Center;
                format.Alignment = StringAlignment.Center;
                pevent.Graphics.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
                pevent.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                pevent.Graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                form.DrawBG(pevent.Graphics, new Rectangle(Left, Top, Width, Height), 0, 0);
                pevent.Graphics.DrawImage(form._imgResource, destRect, srcRect, GraphicsUnit.Pixel);

                var brush = EnabledForeColor;
                if (!Enabled)
                    brush = DisabledForeColor;

                pevent.Graphics.DrawString(Text, _font, brush, new RectangleF(padding, padding, sz.Width - padding, sz.Height - padding), format);

                return;
            }

            if (DesignMode)
                base.OnPaint(pevent);

        }
    }
}
