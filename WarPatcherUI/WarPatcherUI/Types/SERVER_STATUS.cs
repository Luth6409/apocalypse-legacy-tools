﻿using System;

namespace WarPatcherUI.Types
{
    public class SERVER_STATUS
    {
        public ServerStatus Status { get; set; }
        public String MOTD { get; set; }
    }
}

