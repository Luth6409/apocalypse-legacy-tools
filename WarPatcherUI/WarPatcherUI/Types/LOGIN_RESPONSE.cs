﻿using System;

namespace WarPatcherUI.Types
{
    public class LOGIN_RESPONSE
    {
        public LoginStatus Status { get; set; }
        public String Message { get; set; }
        public Guid LoginToken { get; set; }
    }
}
