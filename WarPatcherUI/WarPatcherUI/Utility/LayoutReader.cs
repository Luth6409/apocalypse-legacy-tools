﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WarPatcherUI
{
    public class LayoutReader
    {
        public Dictionary<string, VisualElement> Elements { get; set; } = new Dictionary<string, VisualElement>();
        public VisualElement this[string name]
        {
            get
            {
                return Elements[name];
            }
        }
        public class VisualElement
        {
            public string Name { get; set; }
            public int X { get; set; }
            public int Y { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
         
            public Dictionary<string, VisualElement> Elements { get; set; } = new Dictionary<string, VisualElement>();

            public VisualElement this[string name]
            {
                get
                {
                    return Elements[name];
                }
            }
            public override string ToString()
            {
                return Name;
            }
        }

        public LayoutReader(byte[] data)
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(new MemoryStream(data));
            var first = xml.GetElementsByTagName("layout")[0];
            foreach (XmlNode node in first.ChildNodes)
            {
                if (node is XmlElement)
                {
                    var ve = LoadElement((XmlElement)node);
                    Elements[ve.Name] = ve;
                }
            }
        }
        private static int GetInt32(string value)
        {
            if (value == "")
                return 0;
            return Int32.Parse(value);
        }
        private VisualElement LoadElement(XmlElement el)
        {
            var ve = new VisualElement()
            {
                Name = el.Name,
                X = GetInt32(el.GetAttribute("x")),
                Y = GetInt32(el.GetAttribute("y")),
                Width = GetInt32(el.GetAttribute("w")),
                Height = GetInt32(el.GetAttribute("h"))
            };

            foreach (XmlNode node in el.ChildNodes)
            {
                if (node is XmlElement)
                {
                    var child = LoadElement((XmlElement)node);
                    ve.Elements[child.Name] = child;
                }
            }

            return ve;
        }
    }
}
