﻿#region Using directives

using System.IO;

#endregion

namespace UnexpectedBytes
{
    internal class Program
    {
        private static int Main(string[] args)
        {
            System.Console.WriteLine("[!] Warhammer Online Geom to Wavefront Obj conversion tool v0.0.1");
            System.Console.WriteLine("[?] Written by Debuggin <andi.ireland@gmail.com>");
            System.Console.WriteLine("[.]");

            args = new string[2];
            args[0] = @"fg.0.0.emf_armor_wh_06.geom";
            args[1] = @"D:\Games\Warhammer Online - Age of Reckoning\geom\fg.0.0.emf_armor_wh_06.geom.obj";

            if (args.Length <= 0 || args.Length > 2)
            {
                System.Console.WriteLine("[*] Error: Invalid number of parameters.");
                System.Console.WriteLine("[.]");

                return 1;
            }

            if (args.Length == 1)
            {
                if (args[0] == "/?" || args[0] == "/h" || args[0] == "/help" || args[0] == "-h" || args[0] == "-help" || args[0] == "--h" || args[0] == "--help" || args[0] == "help")
                {
                    System.Console.WriteLine("[!] Converts Warhammer Online .geom to Wavefront .obj");
                    System.Console.WriteLine("[ ]");
                    System.Console.WriteLine("[!] GEOM2OBJ source [destination]");
                    System.Console.WriteLine("[ ]");
                    System.Console.WriteLine("[!] source           Specifies the file to convert.");
                    System.Console.WriteLine("[!] destination      Specifies the name of the new file.");
                    System.Console.WriteLine("[ ]");
                    System.Console.WriteLine("[?] Written by Debuggin <andi.ireland@gmail.com>");
                    System.Console.WriteLine("[.]");
                }
                return 0;
            }

            FileInfo fi = new FileInfo(args[0]);
            if (!fi.Exists)
            {
                System.Console.WriteLine("[*] Error: source does not exist.");
                System.Console.WriteLine("[.]");
                return 1;
            }
            else if (fi.Length > 8388608)
            {
                System.Console.WriteLine("[*] Error: source is larger than the 8388608 byte limit.");
                System.Console.WriteLine("[.]");
                return 1;
            }
            else if (fi.Length < 512)
            {
                System.Console.WriteLine("[*] Error: source is smaller than the 512 byte limit.");
                System.Console.WriteLine("[.]");
                return 1;
            }
            if (args[0] == args[1])
            {
                System.Console.WriteLine("[*] Error: source and destination are the same.");
                System.Console.WriteLine("[.]");
                return 1;
            }

            using (FileStream fs = new FileStream(args[0], FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    uint magic = br.ReadUInt32();
                    if (magic != 0x464C7367 && magic != 0x67734C46)
                    {
                        System.Console.WriteLine("[*] Error: source is not a Warhammer Online .geom file.");
                        System.Console.WriteLine("[.]");
                        return 1;
                    }
                    uint version = br.ReadUInt32();
                    if (version != 0x04)
                    {
                        System.Console.WriteLine("[*] Error: source is not a supported version.");
                        System.Console.WriteLine("[.]");
                        return 1;
                    }
                }
            }

            Formats.Geom geom;
            using (FileStream fs = new FileStream(args[0], FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    geom = new Formats.Geom(br);
                }
            }

            System.Console.WriteLine($"[!] {geom.Bones.Count.ToString()} bones & vertex weights not be exported.");
            //int bone_count = 0;
            //foreach (Formats.Geom.Bone bone in geom.Bones)
            //{

            //    if (bone_count != geom.Bones.Count - 1)
            //        System.Console.Write(bone.Name.Trim(' ', '\0', '\t', '\r', '\n') + ", ");
            //    else
            //        System.Console.Write(bone.Name.Trim(' ', '\0', '\t', '\r', '\n'));

            //    ++bone_count;
            //}
            //System.Console.WriteLine("");
            System.Console.WriteLine("[ ]");

            bool done = false;
            int attempt = 0;
            while (!Directory.Exists(Path.GetPathRoot(args[1])) && !done)
            { 
                Directory.CreateDirectory(Path.GetPathRoot(args[1]));
                if(attempt++ > 8)
                    done = true;
            }

            if (!Directory.Exists(Path.GetPathRoot(args[1])))
            {
                System.Console.WriteLine("[*] Error: could not create the output folder.");
                System.Console.WriteLine("[.]");
                return 1;
            }

            using (FileStream fs = new FileStream(args[1], FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter tw = new StreamWriter(fs))
                {
                    tw.WriteLine("# " + fi.Name);
                    tw.WriteLine("o " + geom.Header.ID.ToString());
                    uint mesh_count = 0;
                    uint vertex_count = 0;
                    foreach (Formats.Geom.Mesh mesh in geom.Meshes)
                    {
                        ++mesh_count;

                        tw.WriteLine("");
                        tw.WriteLine("# Positions");
                        tw.WriteLine("g " + mesh_count.ToString());
                        foreach (Formats.Geom.Vertex vertex in mesh.Vertices)
                        {
                            tw.WriteLine("v " + vertex.PositionX.ToString() + " " + vertex.PositionY.ToString() + " " + vertex.PositionZ.ToString());
                         }
                        tw.WriteLine("# " + mesh.Vertices.Count.ToString() + " Elements");
                        //System.Console.WriteLine(mesh.Vertices.Count.ToString() + " positions exported.");

                        tw.WriteLine("");
                        tw.WriteLine("# Texture coordinates");
                        tw.WriteLine("g " + mesh_count.ToString());
                        foreach (Formats.Geom.Vertex vertex in mesh.Vertices)
                        {
                            tw.WriteLine("vt " + vertex.TextureU.ToString() + " " + vertex.TextureV.ToString());
                        }
                        tw.WriteLine("# " + mesh.Vertices.Count.ToString() + " Elements");
                        //System.Console.WriteLine(mesh.Vertices.Count.ToString() + " texture coordinates exported.");

                        tw.WriteLine("");
                        tw.WriteLine("# Normals");
                        tw.WriteLine("g " + mesh_count.ToString());
                        foreach (Formats.Geom.Vertex vertex in mesh.Vertices)
                        {
                            tw.WriteLine("vn " + vertex.NormalX.ToString() + " " + vertex.NormalY.ToString() + " " + vertex.NormalZ.ToString());
                        }
                        tw.WriteLine("# " + mesh.Vertices.Count.ToString() + " Elements");
                        //System.Console.WriteLine(mesh.Vertices.Count.ToString() + " normals exported.");

                        tw.WriteLine("");
                        tw.WriteLine("# Faces");
                        tw.WriteLine("g " + mesh_count.ToString());
                        foreach (Formats.Geom.Triangle triangle in mesh.Triangles)
                        {
                            tw.WriteLine("f"
                                + " " + (triangle.VertexIndexA + 1 + vertex_count).ToString() + "/" + (triangle.VertexIndexA + 1 + vertex_count).ToString() + "/" + (triangle.VertexIndexA + 1 + vertex_count).ToString()
                                + " " + (triangle.VertexIndexB + 1 + vertex_count).ToString() + "/" + (triangle.VertexIndexB + 1 + vertex_count).ToString() + "/" + (triangle.VertexIndexB + 1 + vertex_count).ToString()
                                + " " + (triangle.VertexIndexC + 1 + vertex_count).ToString() + "/" + (triangle.VertexIndexC + 1 + vertex_count).ToString() + "/" + (triangle.VertexIndexC + 1 + vertex_count).ToString());
                        }
                        tw.WriteLine("# " + mesh.Triangles.Count.ToString() + " Elements");
                        //System.Console.WriteLine(mesh.Triangles.Count.ToString() + " triangles exported.");

                        System.Console.WriteLine("[!] Mesh group " + mesh_count.ToString() + " exported as " + mesh.Triangles.Count.ToString() + " triangle faces indexing " + mesh.Vertices.Count.ToString() + " positions, normals and texture coordinates.");
                        System.Console.WriteLine("[ ]");

                        vertex_count += mesh.VertexCount;
                    }
                }

                System.Console.WriteLine("[!] Export complete.");
                System.Console.WriteLine("[.]");

                return 0;
            }
        }
    }
}
