﻿#region Using directives



#endregion

namespace UnexpectedBytes.Math
{
    public static partial class Functions
    {
        #region Abs
        /// <summary>
        /// Calculates the absolute value of an integer.
        /// </summary>
        /// <param name="x">An integer.</param>
        /// <returns>The absolute value of <paramref name="x"/>.</returns>
        public static int Abs(int x)
        {
            return System.Math.Abs(x);
        }
        /// <summary>
        /// Calculates the absolute value of a single-precision floating point number.
        /// </summary>
        /// <param name="x">A single-precision floating point number.</param>
        /// <returns>The absolute value of <paramref name="x"/>.</returns>
        public static float Abs(float x)
        {
            return System.Math.Abs(x);
        }
        /// <summary>
        /// Calculates the absolute value of a double-precision floating point number.
        /// </summary>
        /// <param name="x">A double-precision floating point number.</param>
        /// <returns>The absolute value of <paramref name="x"/>.</returns>
        public static double Abs(double x)
        {
            return System.Math.Abs(x);
        }
        /// <summary>
        /// Creates a new array of integers whose element values are the
        /// result of applying the absolute function on the elements of the
        /// given integers array.
        /// </summary>
        /// <param name="array">An array of integers.</param>
        /// <returns>A new array of integers whose values are the result of applying the absolute function to each element in <paramref name="array"/></returns>
        public static int[] Abs(int[] array)
        {
            int[] result = new int[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                result[i] = Abs(array[i]);
            }
            return result;
        }
        /// <summary>
        /// Creates a new <see cref="float[]"/> whose element values are the
        /// result of applying the absolute function on the elements of the
        /// given floats array.
        /// </summary>
        /// <param name="array">An array of single-precision floating point values.</param>
        /// <returns>A new <see cref="FloatArrayList"/> whose values are the result of applying the absolute function to each element in <paramref name="array"/></returns>
        public static float[] Abs(float[] array)
        {
            float[] result = new float[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                result[i] = Abs(array[i]);
            }
            return result;
        }
        /// <summary>
        /// Creates a new <see cref="double[]"/> whose element values are the
        /// result of applying the absolute function on the elements of the
        /// given doubles array.
        /// </summary>
        /// <param name="array">An array of double-precision floating point values.</param>
        /// <returns>A new <see cref="double[]"/> whose values are the result of applying the absolute function to each element in <paramref name="array"/></returns>
        public static double[] Abs(double[] array)
        {
            double[] result = new double[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                result[i] = Abs(array[i]);
            }

            return result;
        }
        #endregion
    }
}
