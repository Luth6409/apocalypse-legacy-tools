﻿#region Using directives

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Numerics;
using System.Runtime.InteropServices;

#endregion
namespace UnexpectedBytes.Formats
{
    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    [TypeConverter(typeof(GeomConverter))]
    public class Geom : ICloneable
    {
        #region Private fields
        private FileHeader _file_header;
        private List<Bone> _bones;
        private List<Mesh> _meshes;
        #endregion

        #region Internal types
        public class FileHeader : ICloneable
        {
            #region Private fields
            private uint /*0x00*/ _magic;               // 0x464C7367 'gsLF' 
            private uint /*0x04*/ _version;             // 0x00000004
            private uint /*0x08*/ _file_size;
            private uint /*0x0C*/ _id;
            private uint /*0x10*/ _bone_count;
            private uint /*0x14*/ _bones_offset;
            private uint /*0x18*/ _mesh_count;
            private uint /*0x1C*/ _meshes_offset;
            #endregion

            #region Constructors
            public FileHeader() : this(0, 0, 0, 0, 0, 0, 0, 0)
            {
            }
            public FileHeader(FileHeader header) : this(header.Magic, header.Version, header.FileSize, header.ID, header.BoneCount, header.BonesOffset, header.MeshCount, header.MeshesOffset)
            {
            }
            public FileHeader(BinaryReader br) : this(br.ReadUInt32(), br.ReadUInt32(), br.ReadUInt32(), br.ReadUInt32(), br.ReadUInt32(), br.ReadUInt32(), br.ReadUInt32(), br.ReadUInt32())
            {
            }
            public FileHeader(uint magic, uint version, uint file_size, uint id, uint bone_count, uint bones_offset, uint mesh_count, uint meshes_offset)
            {
                _magic = magic;
                _version = version;
                _file_size = file_size;
                _id = id;
                _bone_count = bone_count;
                _bones_offset = bones_offset;
                _mesh_count = mesh_count;
                _meshes_offset = meshes_offset;
            }
            #endregion

            #region Constants
            public static readonly uint Size = 0x20;
            #endregion

            #region Public properties
            public uint Magic { get => _magic; protected set => _magic = value; }
            public uint Version { get => _version; protected set => _version = value; }
            public uint FileSize { get => _file_size; protected set => _file_size = value; }
            public uint ID { get => _id; protected set => _id = value; }
            public uint BoneCount { get => _bone_count; protected set => _bone_count = value; }
            public uint BonesOffset { get => _bones_offset; protected set => _bones_offset = value; }
            public uint MeshCount { get => _mesh_count; protected set => _mesh_count = value; }
            public uint MeshesOffset { get => _meshes_offset; protected set => _meshes_offset = value; }
            #endregion

            #region ICloneable members
            /// <summary>
            /// Creates an exact copy of this <see cref="Header"/> object.
            /// </summary>
            /// <returns>The <see cref="Header"/> object this method creates, cast as an object.</returns>
            object ICloneable.Clone()
            {
                return new FileHeader(this);
            }
            /// <summary>
            /// Creates an exact copy of this <see cref="Header"/> object.
            /// </summary>
            /// <returns>The <see cref="Header"/> object this method creates.</returns>
            public FileHeader Clone()
            {
                return new FileHeader(this);
            }
            #endregion
        }

        public class MeshHeader
        {
        }

        public class Mesh : ICloneable
        {
            #region Private fields
            private short /*0x00*/ _index;
            private ushort /*0x02*/ _vertex_count;
            private uint /*0x04*/ _vertices_offset;
            private uint /*0x08*/ _triangle_count;
            private uint /*0x0C*/ _triangles_offset;
            private uint /*0x10*/ _weights_limit;
            private uint /*0x14*/ _unk_dword_2;
            private uint /*0x18*/ _unk_dword_3;
            private uint /*0x1C*/ _unk_dword_4;
            private List<Vertex> _vertices;
            private List<Triangle> _triangles;
            private List<VertexWeight> _weights;
            #endregion

            #region Constructors
            public Mesh() : this(0, 0, 0, 0, 0, 0, 0, 0, 0, new List<Vertex>(), new List<Triangle>(), new List<VertexWeight>())
            {
            }
            public Mesh(Mesh mesh) : this(mesh.Index, mesh.VertexCount, mesh.VertexCount, mesh.VertexCount, mesh.TrianglesOffset, mesh.WeightsLimit, mesh.UnkDWORD2, mesh.UnkDWORD3, mesh.UnkDWORD4, mesh.Vertices, mesh.Triangles, mesh.Weights)
            {
            }
            public Mesh(BinaryReader br) : this(br.ReadInt16(), br.ReadUInt16(), br.ReadUInt32(), br.ReadUInt32(), br.ReadUInt32(), br.ReadUInt32(), br.ReadUInt32(), br.ReadUInt32(), br.ReadUInt32(), new List<Vertex>(), new List<Triangle>(), new List<VertexWeight>())
            {
            }
            public Mesh(short index, ushort vertex_count, uint vertices_offset, uint triangle_count, uint triangles_offset, uint weights_limit, uint unk_dword_2, uint unk_dword_3, uint unk_dword_4, List<Vertex> vertices, List<Triangle> triangles, List<VertexWeight> weights)
            {
                _index = index;
                _vertex_count = vertex_count;
                _vertices_offset = vertices_offset;
                _triangle_count = triangle_count;
                _triangles_offset = triangles_offset;
                _weights_limit = weights_limit;
                _unk_dword_2 = unk_dword_2;
                _unk_dword_3 = unk_dword_3;
                _unk_dword_4 = unk_dword_4;
                _vertices = vertices;
                _triangles = triangles;
                _weights = weights;
            }
            #endregion

            #region Constants
            public static readonly uint Size = 0x20;
            #endregion

            #region Public properties
            public short Index { get => _index; set => _index = value; }
            public ushort VertexCount { get => _vertex_count; set => _vertex_count = value; }
            public uint VerticesOffset { get => _vertices_offset; set => _vertices_offset = value; }
            public uint TriangleCount { get => _triangle_count; set => _triangle_count = value; }
            public uint TrianglesOffset { get => _triangles_offset; set => _triangles_offset = value; }
            public uint WeightsLimit { get => _weights_limit; set => _weights_limit = value; }
            public uint UnkDWORD2 { get => _unk_dword_2; set => _unk_dword_2 = value; }
            public uint UnkDWORD3 { get => _unk_dword_3; set => _unk_dword_3 = value; }
            public uint UnkDWORD4 { get => _unk_dword_4; set => _unk_dword_4 = value; }
            public List<Vertex> Vertices { get => _vertices; set => _vertices = value; }
            public List<Triangle> Triangles { get => _triangles; set => _triangles = value; }
            public List<VertexWeight> Weights { get => _weights; set => _weights = value; }
            #endregion

            #region ICloneable members
            /// <summary>
            /// Creates an exact copy of this <see cref="Mesh"/> object.
            /// </summary>
            /// <returns>The <see cref="Mesh"/> object this method creates, cast as an object.</returns>
            object ICloneable.Clone()
            {
                return new Mesh(this);
            }
            /// <summary>
            /// Creates an exact copy of this <see cref="Mesh"/> object.
            /// </summary>
            /// <returns>The <see cref="Mesh"/> object this method creates.</returns>
            public Mesh Clone()
            {
                return new Mesh(this);
            }
            #endregion

            #region Public methods
            /// <summary></summary>
            public void AddVertex(Vertex vertex)
            {
                _vertices.Add(vertex);
            }
            /// <summary></summary>
            public void AddTriangle(Triangle triangle)
            {
                _triangles.Add(triangle);
            }
            /// <summary></summary>
            public void AddVertexWeight(VertexWeight weight)
            {
                _weights.Add(weight);
            }
            #endregion
        }

        public class Vertex : ICloneable
        {
            #region Private fields
            private float /*0x00*/ _position_x;
            private float /*0x04*/ _position_y;
            private float /*0x08*/ _position_z;
            private float /*0x0C*/ _normal_x;
            private float /*0x10*/ _normal_y;
            private float /*0x14*/ _normal_z;
            private float /*0x18*/ _texture_u;
            private float /*0x1C*/ _texture_v;
            #endregion

            #region Constructors
            public Vertex() : this(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f)
            {
            }
            public Vertex(Vertex vertex) : this(vertex.PositionX, vertex.PositionY, vertex.PositionZ, vertex.NormalX, vertex.NormalY, vertex.NormalZ, vertex.TextureU, vertex.TextureV)
            {
            }
            public Vertex(BinaryReader br) : this(br.ReadSingle(), br.ReadSingle(), br.ReadSingle(), br.ReadSingle(), br.ReadSingle(), br.ReadSingle(), br.ReadSingle(), br.ReadSingle())
            {
            }
            public Vertex(float position_x, float position_y, float position_z, float normal_x, float normal_y, float normal_z, float texture_u, float texture_v)
            {
                _position_x = position_x;
                _position_y = position_z;
                _position_z = position_y;
                _normal_x = normal_x;
                _normal_y = normal_z;
                _normal_z = normal_y;
                _texture_u = texture_u;
                _texture_v = texture_v;
            }
            #endregion

            #region Constants
            public static readonly uint Size = 0x20;
            #endregion

            #region Public properties
            public float PositionX { get => _position_x; set => _position_x = value; }
            public float PositionY { get => _position_y; set => _position_y = value; }
            public float PositionZ { get => _position_z; set => _position_z = value; }
            public float NormalX { get => _normal_x; set => _normal_x = value; }
            public float NormalY { get => _normal_y; set => _normal_y = value; }
            public float NormalZ { get => _normal_z; set => _normal_z = value; }
            public float TextureU { get => _texture_u; set => _texture_u = value; }
            public float TextureV { get => _texture_v; set => _texture_v = value; }
            #endregion

            #region ICloneable members
            /// <summary>
            /// Creates an exact copy of this <see cref="Vertex"/> object.
            /// </summary>
            /// <returns>The <see cref="Vertex"/> object this method creates, cast as an object.</returns>
            object ICloneable.Clone()
            {
                return new Vertex(this);
            }
            /// <summary>
            /// Creates an exact copy of this <see cref="Vertex"/> object.
            /// </summary>
            /// <returns>The <see cref="Vertex"/> object this method creates.</returns>
            public Vertex Clone()
            {
                return new Vertex(this);
            }
            #endregion
        }

        public class Triangle : ICloneable
        {
            #region Private fields
            private ushort /*0x00*/ _vertex_index_a;
            private ushort /*0x02*/ _vertex_index_b;
            private ushort /*0x06*/ _vertex_index_c;
            #endregion

            #region Constructors
            public Triangle() : this(0, 0, 0)
            {
            }
            public Triangle(Triangle triangle) : this(triangle.VertexIndexA, triangle.VertexIndexB, triangle.VertexIndexC)
            {
            }
            public Triangle(BinaryReader br) : this(br.ReadUInt16(), br.ReadUInt16(), br.ReadUInt16())
            {
            }
            public Triangle(ushort vertex_index_a, ushort vertex_index_b, ushort vertex_index_c)
            {
                _vertex_index_a = vertex_index_a;
                _vertex_index_b = vertex_index_b;
                _vertex_index_c = vertex_index_c;
            }
            #endregion

            #region Constants
            public static readonly uint Size = 0x06;
            #endregion

            #region Public properties
            public ushort VertexIndexA { get => _vertex_index_a; set => _vertex_index_a = value; }
            public ushort VertexIndexB { get => _vertex_index_b; set => _vertex_index_b = value; }
            public ushort VertexIndexC { get => _vertex_index_c; set => _vertex_index_c = value; }
            #endregion

            #region ICloneable members
            /// <summary>
            /// Creates an exact copy of this <see cref="Triangle"/> object.
            /// </summary>
            /// <returns>The <see cref="Triangle"/> object this method creates, cast as an object.</returns>
            object ICloneable.Clone()
            {
                return new Triangle(this);
            }
            /// <summary>
            /// Creates an exact copy of this <see cref="Triangle"/> object.
            /// </summary>
            /// <returns>The <see cref="Triangle"/> object this method creates.</returns>
            public Triangle Clone()
            {
                return new Triangle(this);
            }
            #endregion
        }

        public class Bone : ICloneable
        {
            #region Private fields
            private uint /*0x00*/ _name_offset;

            //private readonly float /*0x04*/ _unk_dword_01;
            //private readonly float /*0x08*/ _unk_dword_02;
            //private readonly float /*0x0C*/ _unk_dword_03;
            //private readonly float /*0x10*/ _unk_dword_04;
            //private readonly float /*0x14*/ _unk_dword_05;
            //private readonly float /*0x18*/ _unk_dword_06;
            //private readonly float /*0x1C*/ _unk_dword_07;
            //private readonly float /*0x20*/ _unk_dword_08;
            //private readonly float /*0x24*/ _unk_dword_09;
            private Matrix4x4 /*0x04*/ munk1;

            //private readonly float /*0x28*/ _unk_dword_0A;
            //private readonly float /*0x2C*/ _unk_dword_0B;
            //private readonly float /*0x30*/ _unk_dword_0C;
            private Vector3 /*0x28*/ vunk1;

            //private readonly float /*0x34*/ _unk_dword_0D;
            //private readonly float /*0x38*/ _unk_dword_0E;
            //private readonly float /*0x3C*/ _unk_dword_0F;
            private Vector3 /*0x34*/ vunk2;

            //private readonly float /*0x40*/ _unk_dword_10;
            //private readonly float /*0x44*/ _unk_dword_11;
            //private readonly float /*0x48*/ _unk_dword_12;
            //private readonly float /*0x4C*/ _unk_dword_13;
            //private readonly float /*0x50*/ _unk_dword_14;
            //private readonly float /*0x54*/ _unk_dword_15;
            //private readonly float /*0x58*/ _unk_dword_16;
            //private readonly float /*0x5C*/ _unk_dword_17;
            //private readonly float /*0x60*/ _unk_dword_18;
            private Matrix4x4 /*0x40*/ munk2;

            //private readonly float /*0x64*/ _unk_dword_19;
            //private readonly float /*0x68*/ _unk_dword_1A;
            private Vector3 /*0x64*/ vunk3;

            private string _name;
            private uint _index;
            private uint _parent_index;
            #endregion

            #region Constructors
            public Bone() : this(0, "") { }
            public Bone(Bone bone) : this(bone.NameOffset, bone.Name) { }
            public Bone(BinaryReader br)
            {
                long saved_position = br.BaseStream.Position;
                _name_offset = br.ReadUInt32();

                munk1 = new Matrix4x4(br.ReadSingle(), br.ReadSingle(), br.ReadSingle(), 0.0f,
                                      br.ReadSingle(), br.ReadSingle(), br.ReadSingle(), 0.0f,
                                      br.ReadSingle(), br.ReadSingle(), br.ReadSingle(), 0.0f,
                                      0.0f,            0.0f,            0.0f,            0.0f);

                Console.WriteLine($"{munk1.ToString()},");
                
                vunk1 = new Vector3(br.ReadSingle(), br.ReadSingle(), br.ReadSingle());
                Console.WriteLine($"{vunk1.ToString()},");

                vunk2 = new Vector3(br.ReadSingle(), br.ReadSingle(), br.ReadSingle());
                Console.WriteLine($"{vunk2.ToString()},");

                munk2 = new Matrix4x4(br.ReadSingle(), br.ReadSingle(), br.ReadSingle(), 0.0f,
                                      br.ReadSingle(), br.ReadSingle(), br.ReadSingle(), 0.0f,
                                      br.ReadSingle(), br.ReadSingle(), br.ReadSingle(), 0.0f,
                                      0.0f,            0.0f,            0.0f,            1.0f);

                Console.WriteLine($"{munk2.ToString()},");

                vunk3 = new Vector3(br.ReadSingle(), br.ReadSingle(), 0.0f);

                Console.WriteLine($"{vunk3.ToString()},");


                //_unk_dword_01 = br.ReadSingle();
                //_unk_dword_02 = br.ReadSingle();
                //_unk_dword_03 = br.ReadSingle();
                //_unk_dword_04 = br.ReadSingle();

                //Console.WriteLine($"{FormatFloat(_unk_dword_01),8}, \t{FormatFloat(_unk_dword_02),8}, \t{FormatFloat(_unk_dword_03),8}, \t{FormatFloat(_unk_dword_04),8},");

                //_unk_dword_05 = br.ReadSingle();
                //_unk_dword_06 = br.ReadSingle();
                //_unk_dword_07 = br.ReadSingle();
                //_unk_dword_08 = br.ReadSingle();

                //Console.WriteLine($"{FormatFloat(_unk_dword_05),8}, \t{FormatFloat(_unk_dword_06),8}, \t{FormatFloat(_unk_dword_07),8}, \t{FormatFloat(_unk_dword_08),8},");

                //_unk_dword_09 = br.ReadSingle();
                //_unk_dword_0A = br.ReadSingle();
                //_unk_dword_0B = br.ReadSingle();
                //_unk_dword_0C = br.ReadSingle();

                //Console.WriteLine($"{FormatFloat(_unk_dword_09),8}, \t{FormatFloat(_unk_dword_0A),8}, \t{FormatFloat(_unk_dword_0B),8}, \t{FormatFloat(_unk_dword_0C),8},");

                //_unk_dword_0D = br.ReadSingle();
                //_unk_dword_0E = br.ReadSingle();
                //_unk_dword_0F = br.ReadSingle();
                //_unk_dword_10 = br.ReadSingle();

                //Console.WriteLine($"{FormatFloat(_unk_dword_0D),8}, \t{FormatFloat(_unk_dword_0E),8}, \t{FormatFloat(_unk_dword_0F),8}, \t{FormatFloat(_unk_dword_10),8}");

                //_unk_dword_11 = br.ReadSingle();
                //_unk_dword_12 = br.ReadSingle();
                //_unk_dword_13 = br.ReadSingle();
                //_unk_dword_14 = br.ReadSingle();

                //Console.WriteLine($"{FormatFloat(_unk_dword_11),8}, \t{FormatFloat(_unk_dword_12),8}, \t{FormatFloat(_unk_dword_13),8}, \t{FormatFloat(_unk_dword_14),8},");

                //_unk_dword_15 = br.ReadSingle();
                //_unk_dword_16 = br.ReadSingle();
                //_unk_dword_17 = br.ReadSingle();
                //_unk_dword_18 = br.ReadSingle();

                //Console.WriteLine($"{FormatFloat(_unk_dword_15),8}, \t{FormatFloat(_unk_dword_16),8}, \t{FormatFloat(_unk_dword_17),8}, \t{FormatFloat(_unk_dword_18),8},");

                //_unk_dword_19 = br.ReadSingle();
                //_unk_dword_1A = br.ReadSingle();

                //Console.WriteLine($"{FormatFloat(_unk_dword_19),8}, \t{FormatFloat(_unk_dword_1A),8}");

                br.BaseStream.Seek(saved_position + _name_offset, SeekOrigin.Begin);
                System.Text.StringBuilder bldr = new System.Text.StringBuilder();
                char nc = br.ReadChar();
                while(nc != '\0')
                {
                    bldr.Append(nc);
                    nc = br.ReadChar();
                }
                _name = bldr.ToString();
                Debug.WriteLine($"{_name}");
                Debug.WriteLine($"");

                br.BaseStream.Seek(saved_position + 0x6C, SeekOrigin.Begin);
            }
            public Bone(uint name_offset, string name)
            {
                _name_offset = name_offset;
                _name = name;
            }
            #endregion

            #region Constants
            public static readonly uint Size = 0x6C;
            #endregion

            #region Public properties
            public uint NameOffset { get => _name_offset; set => _name_offset = value; }
            public string Name { get => _name; set => _name = value; }
            #endregion

            #region ICloneable members
            /// <summary>
            /// Creates an exact copy of this <see cref="Bone"/> object.
            /// </summary>
            /// <returns>The <see cref="Bone"/> object this method creates, cast as an object.</returns>
            object ICloneable.Clone()
            {
                return new Bone(this);
            }
            /// <summary>
            /// Creates an exact copy of this <see cref="Bone"/> object.
            /// </summary>
            /// <returns>The <see cref="Bone"/> object this method creates.</returns>
            public Bone Clone()
            {
                return new Bone(this);
            }
            #endregion
        }

        public class VertexWeight
        {
            #region Private fields
            private uint /*0x00*/ _vertex_index;
            private float /*0x04*/ _weight;
            private int _bone_index;
            #endregion

            #region Constructors
            public VertexWeight() : this(0, 0, -1)
            {
            }
            public VertexWeight(VertexWeight vertex) : this(vertex.VertexIndex, vertex.Weight, vertex.BoneIndex)
            {
            }
            public VertexWeight(BinaryReader br) : this(br.ReadUInt32(), br.ReadSingle(), -1)
            {
            }
            public VertexWeight(uint vertex_index, float weight, int bone_index)
            {
                _vertex_index = vertex_index;
                _weight = weight;
                _bone_index = bone_index;
            }
            #endregion

            #region Constants
            public static readonly uint Size = 0x08;
            #endregion

            #region Public properties
            public uint VertexIndex { get => _vertex_index; set => _vertex_index = value; }
            public float Weight { get => _weight; set => _weight = value; }
            public int BoneIndex { get => _bone_index; set => _bone_index = value; }
            #endregion
        }
        #endregion

        #region Constructors
        public Geom()
        {
            _file_header = new FileHeader();
            _bones = new List<Bone>();
            _meshes = new List<Mesh>();
        }
        public Geom(Geom geom) : this()
        {
            _file_header = geom.Header;
            _bones = geom.Bones;
            _meshes = geom.Meshes;
        }
        public Geom(BinaryReader br) : this()
        {
            Load(br);
        }
        #endregion

        #region Public properties
        public FileHeader Header { get => _file_header; protected set => _file_header = value; }
        public List<Bone> Bones { get => _bones; protected set => _bones = value; }
        public List<Mesh> Meshes { get => _meshes; protected set => _meshes = value; }
        #endregion

        #region ICloneable members
        /// <summary>
        /// Creates an exact copy of this <see cref="Geom"/> object.
        /// </summary>
        /// <returns>The <see cref="Geom"/> object this method creates, cast as an object.</returns>
        object ICloneable.Clone()
        {
            return new Geom(this);
        }
        /// <summary>
        /// Creates an exact copy of this <see cref="Geom"/> object.
        /// </summary>
        /// <returns>The <see cref="Geom"/> object this method creates.</returns>
        public Geom Clone()
        {
            return new Geom(this);
        }
        #endregion

        #region Public Static Parse Methods
        /// <summary>
        /// Converts the specified string to its <see cref="Geom"/> equivalent.
        /// </summary>
        /// <param name="value">A string representation of a <see cref="Geom"/>.</param>
        /// <returns>A <see cref="Geom"/> that represents the vector specified by the <paramref name="value"/> parameter.</returns>
        public static Geom Parse(string value)
        {
            return new Geom();
        }

        /// <summary>
        /// Converts the specified string to its <see cref="Geom"/> equivalent.
        /// A return value indicates whether the conversion succeeded or failed.
        /// </summary>
        /// <param name="value">A string representation of a <see cref="Geom"/>.</param>
        /// <param name="result">
        /// When this method returns, if the conversion succeeded,
        /// contains a <see cref="Geom"/> representing the vector specified by <paramref name="value"/>.
        /// </param>
        /// <returns><see langword="true"/> if value was converted successfully; otherwise, <see langword="false"/>.</returns>
        public static bool TryParse(string value, out Geom result)
        {
            result = new Geom();
            return true;
        }
        #endregion

        #region Public methods
        /// <summary></summary>
        public void Load(BinaryReader br)
        {
            GetHeader(br);
            GetBones(br);
            GetMeshes(br);
        }
        /// <summary></summary>
        public void GetHeader(BinaryReader br)
        {
            br.BaseStream.Seek(0, SeekOrigin.Begin);
            _file_header = new FileHeader(br);
        }
        /// <summary></summary>
        public void GetBones(BinaryReader br)
        {
            br.BaseStream.Seek(_file_header.BonesOffset, SeekOrigin.Begin);
            for(int i = 0; i < _file_header.BoneCount; ++i)
            {
                Bone bone = new Bone(br);
                
                _bones.Add(bone);
            }
        }
        /// <summary></summary>
        public void GetMeshes(BinaryReader br)
        {
            br.BaseStream.Seek(_file_header.MeshesOffset, SeekOrigin.Begin);
            for(uint i = 0; i < _file_header.MeshCount; ++i)
            {
                Mesh mesh = new Mesh(br);
           
                long saved_position = br.BaseStream.Position;
                br.BaseStream.Seek(_file_header.MeshesOffset + mesh.VerticesOffset + mesh.Index * Mesh.Size, SeekOrigin.Begin);
                for(uint j = 0; j < mesh.VertexCount; ++j)
                {
                    mesh.AddVertex(new Vertex(br));
                }
                br.BaseStream.Seek(_file_header.MeshesOffset + mesh.TrianglesOffset + mesh.Index * Mesh.Size, SeekOrigin.Begin);
                for(uint j = 0; j < mesh.TriangleCount; ++j)
                {
                    mesh.AddTriangle(new Triangle(br));
                }
                for(int j = -1; j < _file_header.BoneCount; ++j)
                {
                    for(uint k = 0; k < mesh.WeightsLimit; ++k)
                    {
                        VertexWeight vw = new VertexWeight(br);
                        vw.BoneIndex = j;
                        mesh.AddVertexWeight(vw);
                    }
                }

                _meshes.Add(mesh);
                br.BaseStream.Seek(saved_position, SeekOrigin.Begin);
            }
        }
        /// <summary></summary>
        public static string FormatFloat(float a)
        {
            float b = ClampFloat(a);
            return $"{b,8}";
        }
        /// <summary></summary>
        public static float ClampFloat(float a)
        {
            if(a <= 0.005 && a >= -0.005)
            {
                return 0.0f;
            } else if(a <= 1.005 && a >= 0.995)
            {
                return 1.0f;
            } else if(a <= -0.995 && a >= -1.005)
            {
                return -1.0f;
            }
            return a;
        }
        #endregion
    }
}
