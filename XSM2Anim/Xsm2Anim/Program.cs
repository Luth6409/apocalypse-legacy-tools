﻿#region Using directives

using System.IO;

#endregion

namespace UnexpectedBytes
{
    internal class Program
    {
        private static int Main(string[] args)
        {
            System.Console.WriteLine("[!] Warhammer Online Xac to 3DS Max ms conversion tool v0.0.1");
            System.Console.WriteLine("[?] Written by Debuggin <andi.ireland@gmail.com>");
            System.Console.WriteLine("[ ]");

            //args = new string[2];
            //args[0] = @"D:\Games\Warhammer Online - Age of Reckoning\myp\art\skeletons\dwm_skeleton.xac";
            //args[1] = @"D:\Games\Warhammer Online - Age of Reckoning\myp\art\skeletons\dwm_skeleton.ms";

            if (args.Length <= 0 || args.Length > 2)
            {
                System.Console.WriteLine("[*] Error: Invalid number of parameters.");
                System.Console.WriteLine("[.] ");

                return 1;
            }

            if (args.Length == 1)
            {
                if (args[0] == "/?" || args[0] == "/h" || args[0] == "/help" || args[0] == "-h" || args[0] == "-help" || args[0] == "--h" || args[0] == "--help" || args[0] == "help")
                {
                    System.Console.WriteLine("[!] Converts Warhammer Online .xac to 3DS Max .ms");
                    System.Console.WriteLine("[ ]");
                    System.Console.WriteLine("[!] XAC2MS source [destination]");
                    System.Console.WriteLine("[ ]");
                    System.Console.WriteLine("[!] source           Specifies the file to convert.");
                    System.Console.WriteLine("[!] destination      Specifies the name of the ouput file.");
                    System.Console.WriteLine("[ ]");
                    System.Console.WriteLine("[?] Written by Debuggin <andi.ireland@gmail.com>");
                    System.Console.WriteLine("[.]");
                }
                return 0;
            }

            FileInfo fi = new FileInfo(args[0]);
            if (!fi.Exists)
            {
                System.Console.WriteLine("[*] Error: source does not exist.");
                System.Console.WriteLine("[.] ");
                return 1;
            }
            else if (fi.Length > 8388608)
            {
                System.Console.WriteLine("[*] Error: source is larger than the 8388608 byte limit.");
                System.Console.WriteLine("[.] ");
                return 1;
            }
            else if (fi.Length < 512)
            {
                System.Console.WriteLine("[*] Error: source is smaller than the 512 byte limit.");
                System.Console.WriteLine("[.] ");
                return 1;
            }
            if (args[0] == args[1])
            {
                System.Console.WriteLine("[*] Error: source and destination are the same.");
                System.Console.WriteLine("[.] ");
                return 1;
            }

            using (FileStream fs = new FileStream(args[0], FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    uint magic = br.ReadUInt32();
                    if (magic != 0x58414320 && magic != 0x20434158)
                    {
                        System.Console.WriteLine("[*] Error: source is not a Warhammer Online .xac file.");
                        System.Console.WriteLine("[.] ");
                        return 1;
                    }
                    uint version = br.ReadUInt32();
                    if (version != 0x01000001)
                    {
                        System.Console.WriteLine("[*] Error: source is not a supported version.");
                        System.Console.WriteLine("[.] ");
                        return 1;
                    }
                }
            }

            XAC xac = new XAC();
            Parser parser;
            using (FileStream fs = new FileStream(args[0], FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    parser = new Parser(br, xac);
                    parser.Parse();
                }
            }

            bool done = false;
            int attempt = 0;
            while (!Directory.Exists(Path.GetPathRoot(args[1])) && !done)
            {
                Directory.CreateDirectory(Path.GetPathRoot(args[1]));
                if (attempt++ > 8)
                    done = true;
            }

            if (!Directory.Exists(Path.GetPathRoot(args[1])))
            {
                System.Console.WriteLine("[*] Error: could not create the output folder.");
                System.Console.WriteLine("[.]");
                return 1;
            }

            using (FileStream fs = new FileStream(args[1], FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter tw = new StreamWriter(fs))
                {
                    tw.WriteLine("clearListener()");
                    tw.WriteLine("");
                    tw.WriteLine("struct sBoneNode(mName, mParentID, mPosition, mRotation, mScale, mBone)");
                    tw.WriteLine("");
                    tw.WriteLine("fn build =");
                    tw.WriteLine("(");
                    tw.WriteLine("\tboneArray = #()");
                    tw.WriteLine("");

                    if (xac.mXACData.mBonesList.Count > 0)
                    {
                        foreach (var node in xac.mXACData.mBonesList)
                        {
                            if (node.mParentID >= 0)
                                tw.WriteLine($"\tjoin boneArray #(sBoneNode mName:\"{node.mName}\" mParentID:{node.mParentID + 1} mPosition:[{node.mPosition.mX}, {node.mPosition.mY}, {node.mPosition.mZ}] mRotation:(quat {node.mRotation.mX} {node.mPosition.mY} {node.mPosition.mZ} {node.mRotation.mW}) mScale:[{node.mScale.mX}, {node.mScale.mY}, {node.mScale.mZ}])");
                            else if (node.mParentID == -1)
                                tw.WriteLine($"\tjoin boneArray #(sBoneNode mName:\"{node.mName}\" mPosition:[{node.mPosition.mX}, {node.mPosition.mY}, {node.mPosition.mZ}] mRotation:(quat {node.mRotation.mX} {node.mPosition.mY} {node.mPosition.mZ} {node.mRotation.mW}) mScale:[{node.mScale.mX}, {node.mScale.mY}, {node.mScale.mZ}])");
                        }
                        tw.WriteLine($"\tfor i = 1 to {xac.mXACData.mBonesList.Count} do");
                    }
                    else if (xac.mXACData.mNodeTree.mNodes.Count > 0)
                    {
                        foreach (var node in xac.mXACData.mNodeTree.mNodes)
                        {
                            if (node.mParentID >= 0)
                                tw.WriteLine($"\tjoin boneArray #(sBoneNode mName:\"{node.mName}\" mParentID:{node.mParentID + 1} mPosition:[{node.mPosition.mX}, {node.mPosition.mY}, {node.mPosition.mZ}] mRotation:(quat {node.mRotation.mX} {node.mPosition.mY} {node.mPosition.mZ} {node.mRotation.mW}) mScale:[{node.mScale.mX}, {node.mScale.mY}, {node.mScale.mZ}])");
                            else if (node.mParentID == -1)
                                tw.WriteLine($"\tjoin boneArray #(sBoneNode mName:\"{node.mName}\" mPosition:[{node.mPosition.mX}, {node.mPosition.mY}, {node.mPosition.mZ}] mRotation:(quat {node.mRotation.mX} {node.mPosition.mY} {node.mPosition.mZ} {node.mRotation.mW}) mScale:[{node.mScale.mX}, {node.mScale.mY}, {node.mScale.mZ}])");
                        }
                        tw.WriteLine($"\tfor i = 1 to {xac.mXACData.mNodeTree.mNodes.Count} do");
                    }

                    tw.WriteLine("\t(");
                    tw.WriteLine("\t\ttfm = boneArray[i].mRotation as matrix3");
                    tw.WriteLine("\t\ttfm.row4 = [boneArray[i].mPosition.x, boneArray[i].mPosition.z, boneArray[i].mPosition.y]");
                    tw.WriteLine("");
                    tw.WriteLine("\t\tboneArray[i].mBone = bonesys.createbone tfm.row4 (tfm.row4 + 0.01 * (normalize tfm.row1)) (normalize tfm.row3)");
                    tw.WriteLine("\t\tboneArray[i].mBone.name = boneArray[i].mName");
                    tw.WriteLine("\t\tboneArray[i].mBone.width = 0.01");
                    tw.WriteLine("\t\tboneArray[i].mBone.height = 0.01");
                    tw.WriteLine("\t\tboneArray[i].mBone.wirecolor = yellow");
                    tw.WriteLine("\t\tboneArray[i].mBone.transform = tfm");
                    tw.WriteLine("\t\tboneArray[i].mBone.setBoneEnable false 0");
                    tw.WriteLine("\t\tboneArray[i].mBone.showlinks = true");
                    tw.WriteLine("\t\tboneArray[i].mBone.pos.controller = TCB_position()");
                    tw.WriteLine("\t\tboneArray[i].mBone.rotation.controller = TCB_rotation()");
                    tw.WriteLine("");
                    tw.WriteLine("\t\tif (boneArray[i].mParentID != undefined) then ");
                    tw.WriteLine("\t\t(");
                    tw.WriteLine("\t\t\tboneArray[i].mBone.parent = boneArray[boneArray[i].mParentID].mBone");
                    tw.WriteLine("\t\t\tboneArray[i].mBone.transform = boneArray[i].mBone.transform * boneArray[i].mBone.parent.transform");
                    tw.WriteLine("\t\t)");
                    tw.WriteLine("\t)");
                    tw.WriteLine(")");

                    tw.WriteLine("build()");
                }

                System.Console.WriteLine("[ ]");
                System.Console.WriteLine("[!] Export complete.");
                System.Console.WriteLine("[.]");

                return 0;
            }
        }
    }
}
