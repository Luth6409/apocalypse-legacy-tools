﻿using System.IO;

namespace UnexpectedBytes
{

    public class NodesNode
    {
        private Quaternion mRotation;
        private Quaternion mScaleRotation;
        private Vector3F mLocation;
        private Vector3F mScale;
        private float mUnk1; //-1.0f
        private float mUnk2; //-1.0f
        private float mUnk3; //-1.0f
        private int mUnk4; //-1
        private int mUnk5; //-1
        private int mParentNodeId; //(index of parent node or -1 for root nodes)
        private int mChildNodesCount; //(number of nodes with parentId = this node's index)
        private int mIncludeInBoundsCalcFlag;
        private MatrixF44 mTransformation;
        private float mImportanceFactor;
        private string mName;

        public NodesNode()
        {
            mRotation = new Quaternion();
            mScaleRotation = new Quaternion();
            mLocation = new Vector3F();
            mScale = new Vector3F();
            mUnk1 = -1.0f;
            mUnk2 = -1.0f;
            mUnk3 = -1.0f;
            mUnk4 = -1;
            mUnk5 = -1;
            mParentNodeId = -1;
            mChildNodesCount = 0;
            mIncludeInBoundsCalcFlag = 0;
            mTransformation = new MatrixF44();
            mImportanceFactor = 0.0f;
            mName = string.Empty;
        }


        public void ReadIn(BinaryReader iStream, Chunk iChunk)
        {
            mRotation.ReadIn(iStream);
            mScaleRotation.ReadIn(iStream);
            mLocation.ReadIn(iStream);
            mScale.ReadIn(iStream);
            mUnk1 = iStream.ReadSingle();
            mUnk2 = iStream.ReadSingle();
            mUnk3 = iStream.ReadSingle();
            mUnk4 = iStream.ReadInt32();
            mUnk5 = iStream.ReadInt32();
            mParentNodeId = iStream.ReadInt32();
            mChildNodesCount = iStream.ReadInt32();
            mIncludeInBoundsCalcFlag = iStream.ReadInt32();
            mTransformation.ReadIn(iStream);
            //mImportanceFactor = iStream.ReadSingle();
            mName = Common.ReadString(iStream);
        }


        public void WriteOut(BinaryWriter iStream, Chunk iChunk)
        {
            mRotation.WriteOut(iStream);
            mScaleRotation.WriteOut(iStream);
            mLocation.WriteOut(iStream);
            mScale.WriteOut(iStream);
            iStream.Write(mUnk1);
            iStream.Write(mUnk2);
            iStream.Write(mUnk3);
            iStream.Write(mUnk4);
            iStream.Write(mUnk5);
            iStream.Write(mParentNodeId);
            iStream.Write(mChildNodesCount);
            iStream.Write(mIncludeInBoundsCalcFlag);
            mTransformation.WriteOut(iStream);
            iStream.Write(mImportanceFactor);
            Common.WriteString(iStream, mName);
        }

        public long GetSize()
        {
            long vSize = 0;
            vSize += mRotation.GetSize();
            vSize += mScaleRotation.GetSize();
            vSize += mLocation.GetSize();
            vSize += mScale.GetSize();
            vSize += sizeof(float);
            vSize += sizeof(float);
            vSize += sizeof(float);
            vSize += sizeof(int);
            vSize += sizeof(int);
            vSize += sizeof(int);
            vSize += sizeof(int);
            vSize += sizeof(int);
            vSize += mTransformation.GetSize();
            vSize += sizeof(float);
            vSize += mName.Length;
            return vSize;
        }

        public override string ToString()
        {
            string vName = string.Empty;
            if (!string.IsNullOrEmpty(vName))
            {
                vName = mName;
            }
            string vTheString = "XACNode Name: " + vName.ToString() +
                                "  ParentNodeId: " + mParentNodeId.ToString() +
                                "  ChildNodesCount: " + mChildNodesCount.ToString() +
                                "  Rotation: " + mRotation.ToString() +
                                "  ScaleRotation: " + mScaleRotation.ToString() +
                                "  Location: " + mLocation.ToString() +
                                "  Scale: " + mScale.ToString() +
                                "  IncludeInBoundsCalcFlag: " + mIncludeInBoundsCalcFlag.ToString() +
                                "  Transformation: " + mTransformation.ToString() +
                                "  ImportanceFactor: " + mImportanceFactor.ToString();

            return vTheString;
        }

    }


}