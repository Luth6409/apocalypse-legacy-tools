﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnexpectedBytes
{

    public class XACNormal
    {
        public float mX;
        public float mY;
        public float mZ;

        public XACNormal(float iX, float iY, float iZ)
        {
            mX = iX;
            mY = iY;
            mZ = iZ;
        }

        public override string ToString()
        {
            string vTheString = "XACNormal X:" + mX.ToString() + " Y:" + mY.ToString() + " Z:" + mZ.ToString(); ;
            return vTheString;
        }
    }

}