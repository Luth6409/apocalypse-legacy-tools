using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_JOIN_GROUP, FrameType.Game)]
    public class F_JOIN_GROUP : Frame
    {
        public F_JOIN_GROUP() : base((int)GameOp.F_JOIN_GROUP)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
