using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PLAYER_EXPERIENCE, FrameType.Game)]
    public class F_PLAYER_EXPERIENCE : Frame
    {
        public F_PLAYER_EXPERIENCE() : base((int)GameOp.F_PLAYER_EXPERIENCE)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
