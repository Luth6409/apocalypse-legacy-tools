using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_GET_CULTIVATION_INFO, FrameType.Game)]
    public class F_GET_CULTIVATION_INFO : Frame
    {
        public F_GET_CULTIVATION_INFO() : base((int)GameOp.F_GET_CULTIVATION_INFO)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
