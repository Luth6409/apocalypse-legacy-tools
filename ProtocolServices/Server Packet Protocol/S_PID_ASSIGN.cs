using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.S_PID_ASSIGN, FrameType.Game)]
    public class S_PID_ASSIGN : Frame
    {
        private ushort PID;
        public S_PID_ASSIGN() : base((int)GameOp.S_PID_ASSIGN)
        {
        }

        public static S_PID_ASSIGN Create(ushort pid)
        {
            return new S_PID_ASSIGN()
            {
                PID = pid
            };
        }
        protected override void SerializeInternal()
        {
            WriteUInt16R(PID);
        }
    }
}
