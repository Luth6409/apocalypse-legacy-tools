using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_WORLD_ENTER, FrameType.Game)]
    public class F_WORLD_ENTER : Frame
    {
        public F_WORLD_ENTER() : base((int)GameOp.F_WORLD_ENTER)
        {
        }

        public static F_WORLD_ENTER Create()
        {
            return new F_WORLD_ENTER();
        }

		protected override void SerializeInternal()
        {
            WriteUInt16(0x0608);
            Fill(0, 20);
            WriteString("38699", 5);
            WriteString("38700", 5);
            WriteString("0.0.0.0", 20);
        }

    }
}
