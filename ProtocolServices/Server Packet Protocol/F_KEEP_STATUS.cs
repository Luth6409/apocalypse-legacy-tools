using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_KEEP_STATUS, FrameType.Game)]
    public class F_KEEP_STATUS : Frame
    {
        public F_KEEP_STATUS() : base((int)GameOp.F_KEEP_STATUS)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
