using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.S_DATAGRAM_ESTABLISHED, FrameType.Game)]
    public class S_DATAGRAM_ESTABLISHED : Frame
    {
        public S_DATAGRAM_ESTABLISHED() : base((int)GameOp.S_DATAGRAM_ESTABLISHED)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
