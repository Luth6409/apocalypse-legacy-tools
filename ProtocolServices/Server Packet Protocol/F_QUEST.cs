using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_QUEST, FrameType.Game)]
    public class F_QUEST : Frame
    {
        public F_QUEST() : base((int)GameOp.F_QUEST)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
