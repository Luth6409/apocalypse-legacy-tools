using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_GUILD_DATA, FrameType.Game)]
    public class F_GUILD_DATA : Frame
    {
        public F_GUILD_DATA() : base((int)GameOp.F_GUILD_DATA)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
