using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.S_GAME_OPENED, FrameType.Game)]
    public class S_GAME_OPENED : Frame
    {
        public S_GAME_OPENED() : base((int)GameOp.S_GAME_OPENED)
        {
        }

        public static S_GAME_OPENED Create()
        {
            return new S_GAME_OPENED();
        }
		protected override void SerializeInternal()
        {
            WriteByte(0);
        }
    }
}
