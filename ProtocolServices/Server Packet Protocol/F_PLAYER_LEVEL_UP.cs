using System.Collections.Generic;
using WarServer.Game.Entities;
using WarShared;
using WarShared.Data;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PLAYER_LEVEL_UP, FrameType.Game)]
    public class F_PLAYER_LEVEL_UP : Frame
    {
        private PlayerData Player;
        private byte IncreaseBy = 1;
        public F_PLAYER_LEVEL_UP() : base((int)GameOp.F_PLAYER_LEVEL_UP)
        {
        }

        public static F_PLAYER_LEVEL_UP Create(Player player, byte increaseBy)
        {
            return new F_PLAYER_LEVEL_UP()
            {
                Player = player.Data,
                IncreaseBy = increaseBy
            };
        }

        protected override void SerializeInternal()
        {
            WriteUInt32(0);
            WriteByte((byte)Player.Career.Statistics.Count);//stat count

            Dictionary<BonusType, int> by = new Dictionary<BonusType, int>();

            for (int i = Player.Level - IncreaseBy + 1; i <= Player.Level; i++)
            {
                foreach (var stat in Player.Career.Statistics.Values)
                {
                    if (!by.ContainsKey(stat.BonusTypeID))
                        by[stat.BonusTypeID] = (int)0;

                    if (i % 2 == 0)
                        by[stat.BonusTypeID] += (int)stat.EvenIncreasePerLevel;
                    else
                        by[stat.BonusTypeID] += (int)stat.OddIncreasePerLevel;
                }
            }

            foreach (var stat in by.Keys)
            {
                WriteByte((byte)stat);
                WriteUInt16((ushort)(by[stat]));

                {
                }
            }
        }
    }
}
