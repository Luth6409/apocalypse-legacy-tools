using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.S_NOT_CONNECTED, FrameType.Game)]
    public class S_NOT_CONNECTED : Frame
    {
        public S_NOT_CONNECTED() : base((int)GameOp.S_NOT_CONNECTED)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
