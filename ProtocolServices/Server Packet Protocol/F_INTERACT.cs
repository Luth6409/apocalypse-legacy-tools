using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_INTERACT, FrameType.Game)]
    public class F_INTERACT : Frame
    {
        public ushort Unk;
        public ushort ObjectID;
        public ushort Menu;
        public byte Page;
        public byte Num;
        public ushort Count;

        public F_INTERACT() : base((int)GameOp.F_INTERACT)
        {
        }
		
        protected override void DeserializeInternal()
        {
            Unk = ReadUInt16();
            ObjectID = ReadUInt16();
            Menu = ReadUInt16();
            Page = ReadByte();
            Num = ReadByte();
            Count = ReadUInt16();
        }
    }
}
