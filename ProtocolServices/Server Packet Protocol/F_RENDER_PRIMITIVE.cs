using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_RENDER_PRIMITIVE, FrameType.Game)]
    public class F_RENDER_PRIMITIVE : Frame
    {
        public F_RENDER_PRIMITIVE() : base((int)GameOp.F_RENDER_PRIMITIVE)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
