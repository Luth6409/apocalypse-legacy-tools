using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_ENCRYPTKEY, FrameType.Game)]
    public class F_ENCRYPTKEY : Frame
    {
        public byte Cipher;
        public byte Application;
        public byte Major;
        public byte Minor;
        public byte Revision;
        public byte Unk1;
        public byte[] Key;

        public F_ENCRYPTKEY() : base((int)GameOp.F_ENCRYPTKEY)
        {
        }
		
        protected override void DeserializeInternal()
        {
            Cipher = ReadByte();
            Application = ReadByte();
            Major = ReadByte();
            Minor = ReadByte();
            Revision = ReadByte();
            Unk1 = ReadByte();

            if (Offset + 256 < Size)
            {
                Key = new byte[256];
                Read(Key, 0, 256);
            }
        }
    }
}
