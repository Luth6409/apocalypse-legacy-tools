﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class F_ANIMATION : MythicPacket
    {
        public ushort A03_ObjectID { get; set; }
        public ushort A05_AnimationID { get; set; }
        public ushort A07_Unk { get; set; }
        public ushort A08_Unk { get; set; }

        public F_ANIMATION(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_ANIMATION, ms, true)
        {
        }
    }
}
