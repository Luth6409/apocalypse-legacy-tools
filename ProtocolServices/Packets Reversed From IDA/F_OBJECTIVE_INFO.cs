﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;


namespace WarAdmin.Packets
{
    public class F_OBJECTIVE_INFO : MythicPacket
    {
        public uint ID { get; set; }
        public ushort Type { get; set; }
        public byte Unk1 { get; set; }
        public ushort Unk2 { get; set; }
        public string EncounterName { get; set; }
        public byte Unk3 { get; set; }
        public uint CurrentObjectiveID { get; set; }
        public byte Unk4 { get; set; }
        public byte ObjectiveCount { get; set; }
        public List<Objective> Objectives { get; set; }
        public byte Difficulty { get; set; }
        public byte Unk5 { get; set; }
        public string CurrentStageName { get; set; }
        public byte Unk6 { get; set; }
        public string CurrentStageDescription { get; set; }
        public ushort Unk7 { get; set; }
        public ushort Unk8Number { get; set; }
        public uint TimeLeft { get; set; }
        public uint Unk9 { get; set; }
        public byte Unk10 { get; set; }
        public uint Unk11 { get; set; }

        public class Objective
        {
            public byte Unk1 { get; set; }
            public ushort KillCount { get; set; }
            public ushort Count { get; set; }
            public byte Unk2 { get; set; }
            public string Description { get; set; }
            public void Load(MemoryStream ms)
            {
                Unk1 = FrameUtil.GetUint8(ms);
                KillCount = FrameUtil.GetUint16(ms);
                Count = FrameUtil.GetUint16(ms);
                Unk2 = FrameUtil.GetUint8(ms); //1
                Description = FrameUtil.GetPascalString(ms);
            }
        }

        public F_OBJECTIVE_INFO(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_CLIENT_DATA, ms, false)
        {
        }

        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
            Objectives = new List<Objective>();

            ID = FrameUtil.GetUint32(ms);
            Type = FrameUtil.GetUint16(ms);
            Unk1 = FrameUtil.GetUint8(ms);
            Unk2 = FrameUtil.GetUint16(ms); //1
            EncounterName = FrameUtil.GetPascalString(ms); 
            Unk3 = FrameUtil.GetUint8(ms); //2
            CurrentObjectiveID = FrameUtil.GetUint32(ms);
            Unk4 = FrameUtil.GetUint8(ms);
            ObjectiveCount = FrameUtil.GetUint8(ms);

            for (int i = 0; i < ObjectiveCount; i++)
            {
                var obj = new Objective();
                obj.Load(ms);
                Objectives.Add(obj);
            }

            Difficulty = FrameUtil.GetUint8(ms);
            Unk5 = FrameUtil.GetUint8(ms);
            CurrentStageName = FrameUtil.GetPascalString(ms);
            Unk6 = FrameUtil.GetUint8(ms);
            CurrentStageDescription = FrameUtil.GetPascalString(ms);
            Unk7 = FrameUtil.GetUint16(ms);
            Unk8Number = FrameUtil.GetUint16(ms);
            TimeLeft = FrameUtil.GetUint32(ms);
            Unk9 = FrameUtil.GetUint32(ms);
            Unk10 = FrameUtil.GetUint8(ms);
            Unk11 = FrameUtil.GetUint32(ms);

          //  Unk3 = FrameUtil.Ge(GetPascalString);
          //  Unk3 = FrameUtil.GetUint16(ms);
        }
    }
}
