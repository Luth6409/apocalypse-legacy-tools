﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class F_SET_ABILITY_TIMER : MythicPacket
    {
        public ushort A03_AbilityID { get; set; }

        public byte A04_Unk { get; set; }
        public byte A05_Unk { get; set; }
        public byte A06_Unk { get; set; }
        public byte A07_Unk { get; set; }
        public byte A08_Unk { get; set; }
        public byte A09_Unk { get; set; }
        public byte A10_Unk { get; set; }
        public byte A11_Unk { get; set; }
        public byte A12_Unk { get; set; }
        public byte A13_Unk { get; set; }
        public F_SET_ABILITY_TIMER()
        {
        }
        public F_SET_ABILITY_TIMER(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_SET_ABILITY_TIMER, ms, true)
        {
        }
    }
}
