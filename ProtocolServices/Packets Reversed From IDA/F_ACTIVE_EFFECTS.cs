﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;


namespace WarAdmin.Packets
{
    public class F_ACTIVE_EFFECTS : MythicPacket
    {
        public UInt16 ObjectID { get; set; }
        public byte Count { get; set; }
        public List<Effect> Effects { get; set; }
        public string Names { get; set; }
        public class EffectData
        {

            public byte Unk1 { get; set; }
            public byte Unk2 { get; set; }
            public byte Unk3 { get; set; }
            public int Unk4 { get; set; }
        }
        public class Effect
        {
            public ushort AbilityID { get; set; }
            public string AbilityName { get; set; }
            public ushort CasterOID { get; set; }
            public byte Count { get; set; }
            public List<EffectData> Data { get; set; }


            public void Load(MemoryStream ms)
            {
                ;
                AbilityID = FrameUtil.GetUint16R(ms);
                AbilityName = frmMain.Abilities[AbilityID].Name;
                CasterOID = FrameUtil.GetUint16R(ms);
                Count = FrameUtil.GetUint8(ms);
                Data = new List<EffectData>();

                for (int i = 0; i < Count; i++)
                {
                    Data.Add(new EffectData()
                    {
                        Unk1 = FrameUtil.GetUint8(ms),
                        Unk2 = FrameUtil.GetUint8(ms),
                        Unk3 = FrameUtil.GetUint8(ms),
                        Unk4 = FrameUtil.ReadVarInt(ms)
                     });
                }
            }

            public override string ToString()
            {
                return AbilityName + "(" + Data[0].Unk4 + ")";
            }
        }
        public F_ACTIVE_EFFECTS()
        {
        }
        public F_ACTIVE_EFFECTS(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_ACTIVE_EFFECTS, ms, true)
        {
        }

        public override void AutoLoad(MemoryStream ms)
        {
            ObjectID = FrameUtil.GetUint16(ms);
            Count = FrameUtil.GetUint8(ms);
            Effects = new List<Effect>();
            string str = "";
            for (int i = 0; i < Count; i++)
            {
                var effect = new Effect();
                effect.Load(ms);
                Effects.Add(effect);
                str += effect.AbilityName + " (" + effect.Data[0].Unk4 + ") ";
            }
            Names = str;
        }
    }
}
