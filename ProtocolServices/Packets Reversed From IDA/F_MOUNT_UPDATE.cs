﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace WarAdmin.Packets
{
    public class F_MOUNT_UPDATE:MythicPacket 
    {
        public ushort ObjectID { get; set; }
        public UInt32 MountID { get; set; }

        public F_MOUNT_UPDATE(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_MOUNT_UPDATE, ms, true)
        {
        }
    }
}
