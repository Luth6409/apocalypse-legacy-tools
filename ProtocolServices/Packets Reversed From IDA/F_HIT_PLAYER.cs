﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class F_HIT_PLAYER : MythicPacket
    {
        public ushort AttackerID { get; set; }
        public ushort CasterID { get; set; }


        public F_HIT_PLAYER(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_HIT_PLAYER, ms, true)
        {
        }
    }
}
