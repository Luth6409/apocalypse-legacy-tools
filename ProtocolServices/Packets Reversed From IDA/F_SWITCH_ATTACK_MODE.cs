﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class F_SWITCH_ATTACK_MODE:MythicPacket
    {
        public byte A03_AttackModeSet { get; set; }


        public F_SWITCH_ATTACK_MODE(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_SWITCH_ATTACK_MODE, ms, true)
        {
        }
    }
}
