﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;



namespace WarAdmin.Packets
{
    public class F_CAREER_CATEGORY : MythicPacket
    {
        public byte CategoryID { get; set; }
        public byte Unk1 { get; set; }
        public byte Unk2_1 { get; set; }
        public byte Unk2_2 { get; set; }
        public byte Unk2_3 { get; set; }
        public byte Unk2_4 { get; set; }

        public ushort Unk3 { get; set; }
        public ushort Unk4 { get; set; }
        public ushort Unk5 { get; set; }
        public ushort Unk6 { get; set; }
        public ushort Unk7 { get; set; }
        public string Name { get; set; }
        public ushort PackageCount { get; set; }
        public List<ushort> PackageData{get;set;}
        public byte Unk8 { get; set; }
        public byte Unk9 { get; set; }
        public byte Unk10 { get; set; }

        public F_CAREER_CATEGORY(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_CAREER_CATEGORY, ms, true)
        {
        }

        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
            byte[] data = ms.ToArray();
            
            CategoryID = FrameUtil.GetUint8(ms);
            Unk1 = FrameUtil.GetUint8(ms);
            Unk2_1 = FrameUtil.GetUint8(ms);
            Unk2_2 = FrameUtil.GetUint8(ms);
            Unk2_3 = FrameUtil.GetUint8(ms);
            Unk2_4 = FrameUtil.GetUint8(ms);
            Unk3 = FrameUtil.GetUint16(ms);
            Unk4 = FrameUtil.GetUint16(ms);
            Unk5 = FrameUtil.GetUint16(ms);
            Unk6 = FrameUtil.GetUint16(ms);
            Unk7 = FrameUtil.GetUint16(ms);
            Name = FrameUtil.GetPascalString(ms);
            PackageCount = FrameUtil.GetUint16(ms);

            PackageData = new List<ushort>();

            for (int i = 0; i < PackageCount; i++)
                PackageData.Add(FrameUtil.GetUint16(ms));

            Unk8 = FrameUtil.GetUint8(ms);
            Unk9 = FrameUtil.GetUint8(ms);
            Unk10 = FrameUtil.GetUint8(ms);
        }
    }
}
