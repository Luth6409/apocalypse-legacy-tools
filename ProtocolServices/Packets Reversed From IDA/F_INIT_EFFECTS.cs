﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;



namespace WarAdmin.Packets
{
    public class F_INIT_EFFECTS : MythicPacket
    {
        public class StatData
        {
            public byte StatIndex { get; set; }
            public int Value { get; set; }

            public override string ToString()
            {
                return StatIndex + ": " + Value.ToString();
            }
        }

        public class BuffData
        {

            public ushort BuffID { get; set; }
            public ushort AbilityID { get; set; }
            public uint Time { get; set; }
            public ushort CasterID { get; set; }
            public string AbilityName { get; set; }
            public byte ParamCount { get; set; }
            private List<StatData> _p = new List<StatData>();
            public List<StatData> Parameters
            {
                get
                {
                    return _p;
                }
                set
                {
                    _p = value; 
                }

            }

            public void DeSerialize(MemoryStream stream)
            {
                BuffID = FrameUtil.GetUint16R(stream);
                AbilityID = FrameUtil.GetUint16R(stream);

                if(frmMain.Abilities.ContainsKey(AbilityID))
                AbilityName = frmMain.Abilities[AbilityID].Name;

                Time = (uint)FrameUtil.ReadVarInt(stream);
                if (stream.Position < stream.Length)
                {
                    CasterID = FrameUtil.GetUint16R(stream);
                    ParamCount = FrameUtil.GetUint8(stream);

                    Parameters = new List<F_INIT_EFFECTS.StatData>();

                    for (int i = 0; i < ParamCount; i++)
                    {
                        F_INIT_EFFECTS.StatData stat = new F_INIT_EFFECTS.StatData();
                        stat.StatIndex = (byte)FrameUtil.GetUint8(stream);
                        stat.Value = FrameUtil.ReadVarInt(stream);
                        Parameters.Add(stat);
                    }
                }

            }

            public string ParmetersAsString()
            {
                string result = "";
                foreach(var p in Parameters)
                {
                    result += p.StatIndex + "=" + p.Value.ToString() + " ";
                }
                return result;
            }
            public override string ToString()
            {

                return AbilityName ;
            }
        }
        public byte A03_BuffCount { get; set; }
        public byte A04_Command { get; set; } //add, remove, update
        public byte A05_Unk { get; set; }
        public byte A06_Unk { get; set; }
        [MField(MFieldType.CASTER, 7)]
        public ushort A07_ObjectID { get; set; }
        public List<BuffData> Buffs { get; set; }

        public string BuffString
        {
            get
            {
                string str = "";

                foreach (var buff in Buffs)
                {
                    str += buff.AbilityName + " ";
                }
                return str;
            }
        }
    

        public F_INIT_EFFECTS()
        {
        }
        public F_INIT_EFFECTS(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_INIT_EFFECTS, ms, false)
        {
        }

        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
            A03_BuffCount = FrameUtil.GetUint8(ms);
            A04_Command = FrameUtil.GetUint8(ms);
            A05_Unk = FrameUtil.GetUint8(ms);
            A06_Unk = FrameUtil.GetUint8(ms);
            A07_ObjectID = FrameUtil.GetUint16(ms);

            Buffs = new List<BuffData>();
            for (int i = 0; i < A03_BuffCount && ms.Position < ms.Length; i++)
            {
                BuffData token = new BuffData();
                token.DeSerialize(ms);
                Buffs.Add(token);
            }
        }
    }
}
