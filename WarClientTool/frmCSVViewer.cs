﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarClientTool
{
    public partial class frmCSVViewer : Form
    {
        private MYP _myp;
        private string _path;
        public bool Changed => csvGrid1.Changed;
        public frmCSVViewer()
        {

            InitializeComponent();


        }
        public void LoadAsset(MYP myp, string path, byte[] data)
        {
            csvGrid1.LoadCSV(data);
            _myp = myp;
            _path = path;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _myp.UpdateAsset(_path, System.Text.ASCIIEncoding.ASCII.GetBytes(csvGrid1.CSV.ToText()), true);
        }
    }
}
