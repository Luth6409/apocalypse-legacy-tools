﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;

namespace UnexpectedBytes.War.Terrain
{
    public class Sector
    {
        public int ScaleFactor { get; set; }
        public int OffsetFactor { get; set; }

        public int SizeX { get; set; }
        public int SizeY { get; set; }

        public Sector() { }

        public Sector(string zones_path, int zone_id)
        {
            Load(zones_path, zone_id);
        }

        public void Load(string zones_path, int zone_id)
        {
            string sector = Path.Combine(zones_path, $"zone{zone_id:D3}", $"sector.dat");
            if (File.Exists(sector))
            {
                string text = File.ReadAllText(sector);
                string[] lines = text.Split((char)0x0A);

                foreach (string line in lines)
                {
                    if (line.StartsWith("scalefactor=", StringComparison.InvariantCultureIgnoreCase))
                    {
                        ScaleFactor = int.Parse(line.Split('=')[1]);
                    }
                    else if (line.StartsWith("offsetfactor=", StringComparison.InvariantCultureIgnoreCase))
                    {
                        OffsetFactor = int.Parse(line.Split('=')[1]);
                    }
                    else if (line.StartsWith("sizex=", StringComparison.InvariantCultureIgnoreCase))
                    {
                        OffsetFactor = int.Parse(line.Split('=')[1]);
                    }
                    else if (line.StartsWith("sizey=", StringComparison.InvariantCultureIgnoreCase))
                    {
                        OffsetFactor = int.Parse(line.Split('=')[1]);
                    }
                }
            }
        }
    }
}


//                if (line.StartsWith("[", StringComparison.InvariantCultureIgnoreCase) && line.EndsWith("", StringComparison.InvariantCultureIgnoreCase))
//                {
//                    string inner = line.Substring(line.IndexOf('['), line.LastIndexOf(']'));
//                    int index = inner.IndexOfAny(new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' });
//                    if (index > -1)
//                    {
//                        string lhs = inner.Substring(0, index);
//                        string rhs = inner.Substring(index);
//    if (line.StartsWith("scalefactor=", StringComparison.InvariantCultureIgnoreCase))
//        {
//            ScaleFactor = int.Parse(line.Split('=')[1]);

//        }
//        else if (line.StartsWith("offsetfactor=", StringComparison.InvariantCultureIgnoreCase))
//        {
//            ScaleFactor = int.Parse(line.Split('=')[1]);
//        }
//    }
//                                     //    if (line.StartsWith("scalefactor=", StringComparison.InvariantCultureIgnoreCase))
//        {
//            ScaleFactor = int.Parse(line.Split('=')[1]);

//        }
//        else if (line.StartsWith("offsetfactor=", StringComparison.InvariantCultureIgnoreCase))
//        {
//            ScaleFactor = int.Parse(line.Split('=')[1]);
//        }
//    }   PreviousBlock = line;
//                        Data.Add(PreviousBlock, new KeyValuePair<string, string>("", ""));
//                        Data.Add(PreviousBlock, new KeyValuePair<string, string>(lhs, rhs));
//                    }
//                    else
//                    {
//                        PreviousBlock = line;
//                        Data.Add(PreviousBlock, new KeyValuePair<string, string>("", ""));
//                    }
//                }
//                else
//                {
//                    string[] components = line.Split('=');
//                    if (components.Length > 1)
//                    {
//                        Data.Add(PreviousBlock, new KeyValuePair<string, string>(components[0], components[1]));
//                    }
//                    else
//                    {
//                        Data.Add(PreviousBlock, new KeyValuePair<string, string>(line, "?"));
//                    }
//                }
//            }





//            //lines = lines.Select(x => x.ToLower()).ToArray<string>();

//            //}

//        }
//    }
//}

//public class SimpleOrder : KeyedCollection<string, Section>
//{

//    // This is the only method that absolutely must be overridden,
//    // because without it the KeyedCollection cannot extract the
//    // keys from the items. The input parameter type is the 
//    // second generic type argument, in this case OrderItem, and 
//    // the return value type is the first generic type argument,
//    // in this case int.
//    //
//    protected override string GetKeyForItem(Section item)
//    {
//        // In this example, the key is the part number.
//        return item.Index;
//    }
//}



//public class Section
//{
//    public int _index;
//    public int _id;
//    public readonly string _parent;
//    public readonly string _name;
//    public readonly object _value;
//    public readonly string _source;

//    public Section(int index, string name, object value, string source)
//    {
//        this._index = index;
//        this._name = name;
//        this._value = value;
//        this._source = source;
//    }

//    public int Index { get => _index; set => _index = value; }

//    public int ID { get => _id; set => _id = value; }

//    public string Name { get => _name; }

//    public object Value { get => _value; }

//    public string Source { get => _source; }


//    public override string ToString()
//    {
//        return _source;
//    }
//}
