﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using WarFigleaf.Structs;

namespace WarFigleaf.Tables
{
    public class ArtSwitchTable : FigTable
    {
        public ArtSwitches[] Switches;
        public override List<object> Records => Switches.Select(e => (object)e).ToList();

        public override int RecordSize =>  Marshal.SizeOf(typeof(ArtSwitches));

        public ArtSwitchTable(FigleafDB db, int recordSize, byte[] tableData, BinaryReader reader) : base(TableType.ArtSwitches, db, tableData, reader)
        {
        }

        protected override int LoadInternal()
        {
            Switches = ReadStructs<ArtSwitches>(_fileData, Offset, (int)EntryCount);
            return RecordSize * (int)EntryCount;
        }

    }
}
