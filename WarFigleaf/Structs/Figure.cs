﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace WarFigleaf.Structs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Figure
    {
        public uint SourceIndex;
        public ushort Unk1a;
        public byte Unk1ba;
        public byte Unk1bb;
        public ushort Unk2a;
        public ushort Unk2b;
        public uint Unk3;
        public uint Unk4;
        public uint Unk5;
        public uint Count;
        public int DataStart;
        public uint Geometry;
        public int DataEnd;
        public uint Attachments;
        public uint Unk11;
    }

    public class FigurePartsView
    {
        public int Index { get; set; }
        public Figure Figure;
        public FigurePartsView(Figure figure)
        {
            Figure = figure;
        }

        public uint SourceIndex { get => Figure.SourceIndex; set => Figure.SourceIndex = value; }
        public string Name { get; set; }
        public ushort Unk1a { get => Figure.Unk1a; set => Figure.Unk1a = value; }
        public byte Unk1ba { get => Figure.Unk1ba; set => Figure.Unk1ba = value; }
        public byte Unk1bb { get => Figure.Unk1bb; set => Figure.Unk1bb = value; }
        public ushort Unk2a { get => Figure.Unk2a; set => Figure.Unk2a = value; }
        public ushort Unk2b { get => Figure.Unk2b; set => Figure.Unk2b = value; }
        public uint Unk3 { get => Figure.Unk3; set => Figure.Unk3 = value; }
        public uint Unk4 { get => Figure.Unk4; set => Figure.Unk4 = value; }
        public uint Unk5 { get => Figure.Unk5; set => Figure.Unk5 = value; }
        public uint Count { get => Figure.Count; set => Figure.Count = value; }
        public int DataStart { get => Figure.DataStart; set => Figure.DataStart = value; }
        public uint Geometry { get => Figure.Geometry; set => Figure.Geometry = value; }
        public int DataEnd { get => Figure.DataEnd; set => Figure.DataEnd = value; }
        public uint Attachments { get => Figure.Attachments; set => Figure.Attachments = value; }
        public uint Unk11 { get => Figure.Unk11; set => Figure.Unk11 = value; }
        public List<GeometryPartView> GeometryList;
        public List<FigurePartView> Parts;

    }

    [StructLayout(LayoutKind.Sequential)]
    public struct GeometryPart
    {
        public int Unk1 { get; set; }
        public int Unk2 { get; set; }
        public int Unk3 { get; set; }
        public int Unk4 { get; set; }
        public int Unk5 { get; set; }
        public int Unk6 { get; set; }
        public int Unk7 { get; set; }
        public int Unk8 { get; set; }
        public int Unk9 { get; set; }
        public int Unk10 { get; set; }
        public int Unk11 { get; set; }
        public int Unk12 { get; set; }
        public int Unk13 { get; set; }
        public int Unk14 { get; set; }
        public int Unk15 { get; set; }
        public int Unk16 { get; set; }
    }

    public class GeometryPartView
    {
        public GeometryPart GeometryPart;
        public GeometryPartView(GeometryPart part)
        {
            GeometryPart = part;
        }

        public int Unk1 { get => GeometryPart.Unk1; set => GeometryPart.Unk1 = value; }
        public int Unk2 { get => GeometryPart.Unk2; set => GeometryPart.Unk2 = value; }
        public int Unk3 { get => GeometryPart.Unk3; set => GeometryPart.Unk3 = value; }
        public int Unk4 { get => GeometryPart.Unk4; set => GeometryPart.Unk4 = value; }
        public int Unk5 { get => GeometryPart.Unk5; set => GeometryPart.Unk5 = value; }
        public int Unk6 { get => GeometryPart.Unk6; set => GeometryPart.Unk6 = value; }
        public int Unk7 { get => GeometryPart.Unk7; set => GeometryPart.Unk7 = value; }
        public int Unk8 { get => GeometryPart.Unk8; set => GeometryPart.Unk8 = value; }
        public int Unk9 { get => GeometryPart.Unk9; set => GeometryPart.Unk9 = value; }
        public int Unk10 { get => GeometryPart.Unk10; set => GeometryPart.Unk10 = value; }
        public int Unk11 { get => GeometryPart.Unk11; set => GeometryPart.Unk11 = value; }
        public int Unk12 { get => GeometryPart.Unk12; set => GeometryPart.Unk12 = value; }
        public int Unk13 { get => GeometryPart.Unk13; set => GeometryPart.Unk13 = value; }
        public int Unk14 { get => GeometryPart.Unk14; set => GeometryPart.Unk14 = value; }
        public int Unk15 { get => GeometryPart.Unk15; set => GeometryPart.Unk15 = value; }
        public int Unk16 { get => GeometryPart.Unk16; set => GeometryPart.Unk16 = value; }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct FigurePart
    {
        public int Unk1 { get; set; }
        public int Unk2 { get; set; }
        public int Unk3 { get; set; }
        public int Unk4 { get; set; }
        public int Unk5 { get; set; }
        public int Unk6 { get; set; }
        public int Unk7 { get; set; }
        public int Unk8 { get; set; }
        public int Unk9 { get; set; }
        public int Unk10 { get; set; }
        public int Unk11 { get; set; }
        public int Unk12 { get; set; }
        public int Unk13 { get; set; }
        public int Unk14 { get; set; }
        public int Unk15 { get; set; }
        public int Unk16 { get; set; }
        public int Unk17 { get; set; }
        public int Unk18 { get; set; }
        public int Unk19 { get; set; }
        public int Unk20 { get; set; }
        public int Unk21 { get; set; }
        public int Unk22 { get; set; }
        public int Unk23 { get; set; }
        public int Unk24 { get; set; }
    }

    public class FigurePartView
    {
        public FigurePart FigurePart;
        public FigurePartView(FigurePart part)
        {
            FigurePart = part;
        }

        public int Unk1 { get => FigurePart.Unk1; set => FigurePart.Unk1 = value; }
        public int Unk2 { get => FigurePart.Unk2; set => FigurePart.Unk2 = value; }
        public int Unk3 { get => FigurePart.Unk3; set => FigurePart.Unk3 = value; }
        public int Unk4 { get => FigurePart.Unk4; set => FigurePart.Unk4 = value; }
        public int Unk5 { get => FigurePart.Unk5; set => FigurePart.Unk5 = value; }
        public int Unk6 { get => FigurePart.Unk6; set => FigurePart.Unk6 = value; }
        public int Unk7 { get => FigurePart.Unk7; set => FigurePart.Unk7 = value; }
        public int Unk8 { get => FigurePart.Unk8; set => FigurePart.Unk8 = value; }
        public int Unk9 { get => FigurePart.Unk9; set => FigurePart.Unk9 = value; }
        public int Unk10 { get => FigurePart.Unk10; set => FigurePart.Unk10 = value; }
        public int Unk11 { get => FigurePart.Unk11; set => FigurePart.Unk11 = value; }
        public int Unk12 { get => FigurePart.Unk12; set => FigurePart.Unk12 = value; }
        public int Unk13 { get => FigurePart.Unk13; set => FigurePart.Unk13 = value; }
        public int Unk14 { get => FigurePart.Unk14; set => FigurePart.Unk14 = value; }
        public int Unk15 { get => FigurePart.Unk15; set => FigurePart.Unk15 = value; }
        public int Unk16 { get => FigurePart.Unk16; set => FigurePart.Unk16 = value; }
        public int Unk17 { get => FigurePart.Unk17; set => FigurePart.Unk17 = value; }
        public int Unk18 { get => FigurePart.Unk18; set => FigurePart.Unk18 = value; }
        public int Unk19 { get => FigurePart.Unk19; set => FigurePart.Unk19 = value; }
        public int Unk20 { get => FigurePart.Unk20; set => FigurePart.Unk20 = value; }
        public int Unk21 { get => FigurePart.Unk21; set => FigurePart.Unk21 = value; }
        public int Unk22 { get => FigurePart.Unk22; set => FigurePart.Unk22 = value; }
        public int Unk23 { get => FigurePart.Unk23; set => FigurePart.Unk23 = value; }
        public int Unk24 { get => FigurePart.Unk24; set => FigurePart.Unk24 = value; }
    }
}
