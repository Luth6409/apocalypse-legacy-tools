﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarHammerOnline.DataMining
{
    public class MYPManager : IDisposable
    {
        private Dictionary<PackageType, Myp> Packages = new Dictionary<PackageType, Myp>();
        private String _mypFolder;
        public readonly Dictionary<Int64, String> HashToString = null;
        public Dictionary<PackageType, Folder> PackageFolders = new Dictionary<PackageType, Folder>();
        private Dictionary<Language, Dictionary<String, StringTable>> _strings = new Dictionary<Language, Dictionary<String, StringTable>>();

        public Boolean Updated
        {
            get {
                foreach (Myp package in Packages.Values)
                    if (package.Changed)
                    {
                        return true;
                    }
                return false;
            }
        }

        public void Close()
        {
            foreach (Myp archive in Packages.Values)
            {
                archive.Dispose();
            }
        }

        public MYPManager(String mypFolder, Dictionary<Int64, String> dehashes) => _mypFolder = mypFolder;

        protected async Task LoadInternal(Boolean overlay, params PackageType[] include)
        {
            var tasks = new List<Task>();

            foreach (PackageType archive in Enum.GetValues(typeof(PackageType)))
            {
                if (archive == PackageType.Mft)
                    continue;

                if (include == null || !include.Contains(archive))
                    continue;

                String file = Path.Combine(_mypFolder, archive + ".myp");
                if (File.Exists(file))
                    tasks.Add(Task.Run(() => LoadPackage(file)));
            }
            await Task.WhenAll(tasks);
        }

        public Dictionary<Int64, PatcherAsset> GetAssets(PackageType package)
        {
            return Packages[package].Assets;
        }

        public List<PackageType> GetPackages()
        {
            return Packages.Keys.ToList();
        }

        public PatcherAsset GetAsset(PackageType package, Int64 hash)
        {
            if (Packages.ContainsKey(package) && Packages[package].Assets.ContainsKey(hash))
            {
                if (Packages[package].Assets[hash].Data == null)
                    Packages[package].Assets[hash].Data = Packages[package].GetAssetData(hash);
                return Packages[package].Assets[hash];
            }
            return null;
        }

        private void LoadPackage(String file)
        {
            var package = (PackageType)Enum.Parse(typeof(PackageType), Path.GetFileNameWithoutExtension(file).ToUpper());

            if (File.Exists(file))
            {
                try
                {
                    var m = new Myp(package);
                    m.Manager = this;
                    Packages[package] = m;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
        }

        public void Save()
        {
            foreach (Myp myp in Packages.Values.ToList())
            {
                if (myp.Changed)
                {
                    Console.WriteLine($"Saving archive '{myp.Filename}'");
                    myp.Save();
                }
            }

        }

        public void UpdateAsset(PatcherAsset asset)
        {
            Packages[asset.Package].UpdateAsset(asset);
        }

        public void InsertAsset(PatcherAsset asset)
        {
            Packages[asset.Package].UpdateAsset(asset);
        }

        public void DeleteAsset(PatcherAsset asset)
        {
            Packages[asset.Package].Delete(asset);
        }

        public Boolean HasAsset(PackageType package, Int64 hash)
        {
            return Packages.ContainsKey(package) && Packages[package].Assets.ContainsKey(hash);
        }

        public void Dispose()
        {
            foreach (Myp myp in Packages.Values.ToList())
            {
                myp.Dispose();
            }
        }


        public String GetAssetStringASCII(String assetName, PackageType? archive = null)
        {
            Int64 hash = Myp.HashWAR(assetName);
            if (archive == null)
            {
                foreach (PackageType package in GetPackages())
                {

                    if (HasAsset(package, hash))
                        return System.Text.Encoding.ASCII.GetString(GetAsset(package, hash).Data);
                }
            }
            else if (GetPackages().Contains(archive.Value))
            {
                return System.Text.Encoding.ASCII.GetString(GetAsset(archive.Value, hash).Data);
            }

            return "";
        }
        public String GetAssetStringUnicode(String assetName, PackageType? archive = null)
        {
            Int64 hash = Myp.HashWAR(assetName);
            if (archive == null)
            {
                foreach (PackageType package in GetPackages())
                {

                    if (HasAsset(package, hash))
                        return System.Text.Encoding.Unicode.GetString(GetAsset(package, hash).Data);
                }
            }
            else if (GetPackages().Contains(archive.Value))
            {
                return System.Text.Encoding.Unicode.GetString(GetAsset(archive.Value, hash).Data);
            }

            return "";
        }

        public PatcherAsset GetAsset(String assetName)
        {
            return GetAsset(Myp.HashWAR(assetName));
        }

        public PatcherAsset GetAsset(PatcherAsset asset)
        {
            asset.Data = GetAsset(asset.Package, asset.Hash).Data;
            return asset;
        }

        public PatcherAsset FindAsset(String assetName, PackageType package)
        {
            PatcherAsset asset = GetAsset(assetName, package);
            if (asset == null)
            {
                return GetAsset(Myp.HashWAR(assetName));
            }
            return asset;
        }

        public PatcherAsset GetAsset(String assetName, PackageType package)
        {
            Int64 hash = Myp.HashWAR(assetName);
            if (HasPackage(package) && HasAsset(package, hash))
            {
                return GetAsset(package, hash);
            }
            return null;
        }

        public PatcherAsset GetAsset(Int64 hash)
        {
            foreach (PackageType package in GetPackages())
            {
                PatcherAsset asset = GetAsset(package, hash);
                if (asset != null)
                    return asset;
            }
            return null;
        }

        public Boolean HasPackage(PackageType package)
        {
            return GetPackages().Contains(package);
        }

        public async Task Load(Boolean overlay, params PackageType[] include)
        {
            await LoadInternal(overlay, include);
            foreach (PackageType package in GetPackages())
            {
                foreach (PatcherAsset asset in GetAssets(package).Values)
                    AddToFolder(package, asset);
            }
        }

        protected void AddToFolder(PackageType package, PatcherAsset entry)
        {
            String packagename = package.ToString().ToLower();
            if (!PackageFolders.ContainsKey(package))
            {
                PackageFolders[package] = new Folder(this)
                {
                    Package = package,
                    Path = ""
                };
            }
            Folder folder = PackageFolders[package];

            if (!String.IsNullOrEmpty(entry.Name))
            {
                var folders = entry.Name.Split(new Char[] { '/' });
                String path = "";


                for (Int32 i = 0; i < folders.Length - 1; i++)
                {
                    path += folders[i];
                    if (!folder.Folders.ContainsKey(path))
                    {
                        folder.Folders[path] = new Folder(this)
                        {
                            Package = package,
                            Path = path,
                            PatcherFileID = entry.PatcherFileID,
                            Parent = folder,
                        };
                    }

                    folder = folder.Folders[path];
                    path += '/';
                }
                path += folders[folders.Length - 1];

                folder.Files[entry.FileTableEntry.Hash] = entry;
            }
            else
            {

                if (!folder.Folders.ContainsKey("unknown_hashes"))
                {
                    folder.Folders["unknown_hashes"] = new Folder(this)
                    {
                        Package = package,
                        Path = "unknown_hashes",
                        Dehashed = false,
                        PatcherFileID = entry.PatcherFileID,
                    };
                }
                folder.Folders["unknown_hashes"].Files[entry.FileTableEntry.Hash] = entry;
            }
        }

        public void UpdateAsset(PackageType package, String path, Byte[] data)
        {
            PatcherAsset asset = GetAsset(path, package);
            asset.Data = data;
            UpdateAsset(asset);
        }

        public String GetStringByKey(Language language, String tableName, String key, PackageType defaulPackage = PackageType.DEV)
        {
            String path = tableName.Replace($"data/strings/{language}/", "");
            String result = "";
            if (!_strings.ContainsKey(language))
                _strings[language] = new Dictionary<String, StringTable>();

            if (!_strings[language].ContainsKey(path))
            {
                PatcherAsset asset = GetAsset("data/strings/" + language + "/" + path, defaulPackage);
                if (asset == null)
                    asset = GetAsset("data/strings/" + language + "/" + path);

                if (asset != null)
                {
                    _strings[language][path] = new StringTable();
                    _strings[language][path].Load(asset.Data);
                }
            }

            if (_strings[language].ContainsKey(path))
                return _strings[language][path].GetKeyedValue(key);

            return result;
        }

        public String GetString(Language language, String tableName, Int32 row, PackageType defaulPackage = PackageType.DEV)
        {
            String path = tableName.Replace($"data/strings/{language}/", "");
            String result = "";
            if (!_strings.ContainsKey(language))
                _strings[language] = new Dictionary<String, StringTable>();

            if (!_strings[language].ContainsKey(path))
            {
                PatcherAsset asset = GetAsset("data/strings/" + language + "/" + path, defaulPackage);
                if (asset == null)
                    asset = GetAsset("data/strings/" + language + "/" + path);

                if (asset != null)
                {
                    _strings[language][path] = new StringTable();
                    _strings[language][path].Load(asset.Data);
                }
            }

            if (_strings[language].ContainsKey(path))
                return _strings[language][path].GetValue(row);

            return result;
        }

        public String GetString(Language language, String tableName)
        {
            String path = tableName.Replace($"data/strings/{language}/", "");
            if (!_strings.ContainsKey(language))
                _strings[language] = new Dictionary<String, StringTable>();
            if (!_strings[language].ContainsKey(path))
            {
                GetStringTable(language, path);
            }

            if (_strings[language].ContainsKey(path))
                return _strings[language][path].ToDocument();

            return "";
        }

        public Byte[] GetStringUnicode(Language language, String tableName)
        {
            String path = tableName.Replace($"data/strings/{language}/", "");

            if (!_strings.ContainsKey(language))
                _strings[language] = new Dictionary<String, StringTable>();

            if (!_strings[language].ContainsKey(path))
            {
                GetStringTable(language, path);
            }

            if (_strings[language].ContainsKey(path))
                return System.Text.UnicodeEncoding.Unicode.GetPreamble().Concat(System.Text.UnicodeEncoding.Unicode.GetBytes(_strings[language][path].ToDocument())).ToArray();

            return System.Text.UnicodeEncoding.Unicode.GetPreamble().Concat(System.Text.UnicodeEncoding.Unicode.GetBytes("")).ToArray();
        }

        public List<String> GetStringTable(Language language, String tableName, PackageType defaulPackage = PackageType.Dev)
        {
            String path = tableName.Replace($"data/strings/{language}/", "");

            if (!_strings.ContainsKey(language))
                _strings[language] = new Dictionary<String, StringTable>();


            if (!_strings[language].ContainsKey(path))
            {
                PatcherAsset asset = GetAsset($"data/strings/{language}/{path}", defaulPackage);
                if (asset == null)
                    asset = GetAsset("data/strings/" + language + "/" + path);

                if (asset != null)
                {
                    _strings[language][path] = new StringTable();
                    _strings[language][path].Load(asset.Data);
                }
            }

            if (_strings[language].ContainsKey(path))
                return _strings[language][path].List;

            return new List<String>();

        }

        public StringTable GetStringTable2(Language language, String tableName, PackageType defaulPackage = PackageType.DEV)
        {
            String path = tableName.Replace($"data/strings/{language}/", "");

            if (!_strings.ContainsKey(language))
                _strings[language] = new Dictionary<String, StringTable>();


            if (!_strings[language].ContainsKey(path))
            {
                PatcherAsset asset = GetAsset($"data/strings/{language}/{path}", defaulPackage);
                if (asset == null)
                    asset = GetAsset("data/strings/" + language + "/" + path);

                if (asset != null)
                {
                    _strings[language][path] = new StringTable();
                    _strings[language][path].Load(asset.Data);
                }
            }

            if (_strings[language].ContainsKey(path))
                return _strings[language][path];

            return null;

        }

        public void SetString(Language language, String tableName, Int32 label, String value, PackageType defaulPackage = PackageType.DEV)
        {
            String path = tableName.Replace($"data/strings/{language}/", "");
            if (!_strings.ContainsKey(language))
                _strings[language] = new Dictionary<String, StringTable>();

            if (!_strings[language].ContainsKey(path))
            {
                PatcherAsset asset = GetAsset(path, defaulPackage);
                if (asset == null)
                    asset = GetAsset(path);

                if (asset != null)
                {
                    _strings[language][path] = new StringTable();
                    _strings[language][path].Load(asset.Data);
                }
            }

            if (_strings[language].ContainsKey(path))
            {
                _strings[language][path].SetValue(label, value);
            }

        }

        public String GetStringFormat(Language language, String label, params Object[] vals)
        {
            String str = GetStringByKey(language, "/default.txt", label);

            for (Int32 i = 0; i < vals.Length; i++)
            {
                str = str.Replace("<<" + (i + 1).ToString() + ">>", vals[i].ToString());
            }
            return str.Replace("<BR>", "\r\n");
        }

        //public bool HasDehash(string path)
        //{
        //    return HashToString.ContainsKey(Myp.HashWAR(path));
        //}
    }

}
