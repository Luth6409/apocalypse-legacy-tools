﻿namespace UnexpectedBytes
{

    public class XACVerticeColor
    {
        public byte mA;
        public byte mB;
        public byte mC;
        public byte mD;

        public XACVerticeColor(byte iA, byte iB, byte iC, byte iD)
        {
            mA = iA;
            mB = iB;
            mC = iC;
            mD = iD;
        }

        public override string ToString()
        {
            string vTheString = "XACVerticeColorBytes A:" + mA.ToString() + " B:" + mB.ToString() + " C:" + mC.ToString() + " D:" + mD.ToString();
            return vTheString;
        }
    }
}
