﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarClientTool
{
    public partial class csv_grid : UserControl
    {
        private CSV _csv;
        private Int32 _skip = 1;
        private Dictionary<Int32, ListViewItem> _lvicache = new Dictionary<Int32, ListViewItem>();
        private Boolean _disableupdates;
        private Int32 _currentrow = 0;
        private Int32 _currentcol = 0;
        //private Boolean _changed = false;
        //PatcherAsset _asset;

        public delegate void AssetDelegate(PatcherAsset asset, Byte[] data);
        //public event AssetDelegate OnSave;

        public Boolean Changed
        {
            get {
                LoadCSV(txtRaw.Text);
                return _csvorig != _csv.ToText();
            }
        }

        public csv_grid()
        {
            InitializeComponent();

            listView1.Controls.Add(_txt);
            listView1.FullRowSelect = true;
            _txt.Leave += Txt_Leave;
        }

        private void Txt_Leave(Object sender, EventArgs e)
        {

            _txt.Visible = false;
            _csv.RowIndex = _currentrow;
            _csv.WriteCol(_currentcol, _txt.Text);
            _disableupdates = true;

            txtRaw.Text = _csv.ToText();
            //CheckChanged();
            if(_lvicache.ContainsKey(_currentrow))
            {
                _lvicache[_currentrow].SubItems[_currentcol].Text = _txt.Text;
            }

            _disableupdates = false;
        }

        //private void CheckChanged()
        //{
        //    if(_csv != null)
        //    {
        //        _changed = _csvorig != _csv.ToText();
        //    }
        //}

        private readonly TextBox _txt = new TextBox { BorderStyle = BorderStyle.FixedSingle, Visible = false };
        private String _csvorig = "";

        public delegate void ColumnInfoDelegate(csv_grid grid, Int32 colIndex, out Int32 width);
        public event ColumnInfoDelegate OnGetColumnWidth;

        public void LoadCSV(Byte[] data)
        {
            listView1.VirtualMode = true;

            _csv = new CSV(System.Text.ASCIIEncoding.ASCII.GetString(data));
            _csvorig = _csv.ToText();
            LoadCSV(_csv);
        }

        public CSV CSV
        {
            get {
                TxtRaw_Leave(null, null);
                return _csv;
            }
        }
        public void LoadCSV(String text)
        {
            _csv = new CSV(text);
            LoadCSV(_csv);
        }
        public List<Int32> GetColWidths()
        {
            var list = new List<Int32>();
            for(Int32 i = 0; i < listView1.Columns.Count; i++)
            {
                list.Add(listView1.Columns[i].Width);
            }
            return list;
        }

        public void LoadCSV(CSV csv)
        {

            listView1.Items.Clear();
            listView1.Columns.Clear();
            _lvicache = new Dictionary<Int32, ListViewItem>();

            _csv = csv;
            _disableupdates = true;
            if(csv.Lines.Count == 0)
            {
                listView1.VirtualListSize = 0;
                return;
            }

            var counts = new Dictionary<String, Int32>();
            Int32 index = 0;
            foreach(String cCol in _csv.Row)
            {
                String csvCol = cCol;
                var col = new ColumnHeader();


                col.Text = csvCol;
                if(!counts.ContainsKey(csvCol))
                {
                    counts[csvCol] = 1;
                } else
                {
                    counts[csvCol]++;
                    csvCol = csvCol + counts[csvCol];
                }

                Int32 width = col.Width;
                OnGetColumnWidth?.Invoke(this, index, out width);
                index++;
                if(width != 0)
                    col.Width = width;

                listView1.Columns.Add(col);

            }
            _csv.NextRow();
            listView1.VirtualMode = true;
            try
            {
                listView1.VirtualListSize = _csv.Lines.Count;

            } catch
            {
            }
            txtRaw.Text = _csv.ToText();
            _disableupdates = false;
            //_changed = false;
        }

        private void ListView1_RetrieveVirtualItem(Object sender, RetrieveVirtualItemEventArgs e)
        {
            if(_disableupdates)
                return;

            if(_csv != null)
            {
                if(!_lvicache.ContainsKey(e.ItemIndex + _skip))
                {
                    ListView1_CacheVirtualItems(null, new CacheVirtualItemsEventArgs(e.ItemIndex, e.ItemIndex));
                }
                e.Item = _lvicache[e.ItemIndex];
            }
        }

        private void ListView1_CacheVirtualItems(Object sender, CacheVirtualItemsEventArgs e)
        {
            if(_csv != null)
            {
                for(Int32 c = e.StartIndex; c <= e.EndIndex; c++)
                {

                    _csv.RowIndex = c;
                    if(!_lvicache.ContainsKey(_csv.RowIndex))
                    {
                        List<String> rowData = _csv.Row;


                        var row = new ListViewItem();
                        row.Tag = c;

                        if(rowData.Count > 0)
                            row.Text = rowData[0];

                        for(Int32 i = 1; i < listView1.Columns.Count; i++)
                        {
                            if(i < rowData.Count)
                                row.SubItems.Add(rowData[i]);
                            else
                                row.SubItems.Add("");
                        }
                        if(c % 2 == 0)
                        {
                            row.BackColor = Color.FromArgb(255, 240, 240, 240);
                        }
                        _lvicache[c] = row;
                    }
                }
            }
        }

        private void ListView1_MouseDoubleClick(Object sender, MouseEventArgs e)
        {
            ListViewHitTestInfo hit = listView1.HitTest(e.Location);

            Rectangle rowBounds = hit.SubItem.Bounds;
            Rectangle labelBounds = hit.Item.GetBounds(ItemBoundsPortion.Label);

            _currentrow = (Int32)hit.Item.Tag;
            _currentcol = hit.Item.SubItems.IndexOf(hit.SubItem);
            Int32 leftMargin = labelBounds.Left - 1;
            _txt.Bounds = new Rectangle(rowBounds.Left + leftMargin, rowBounds.Top, rowBounds.Width - leftMargin - 1, rowBounds.Height);
            _txt.Text = hit.SubItem.Text;
            _txt.SelectAll();
            _txt.Visible = true;
            _txt.Focus();
        }

        private void TxtRaw_TextChanged(Object sender, EventArgs e)
        {

        }
        public delegate void ChangeDelegate();
        // public event ChangeDelegate OnChanged;


        private void DeleteToolStripMenuItem_Click(Object sender, EventArgs e)
        {
            _disableupdates = true;
            var toRemove = new List<Int32>();
            foreach(Int32 s in listView1.SelectedIndices)
            {
                toRemove.Add(s);
            }

            _currentrow = 0;
            _csv.Remove(toRemove);
            LoadCSV(_csv.ToText());
            _disableupdates = false;
        }

        private void TxtRaw_Leave(Object sender, EventArgs e) => _csv = new CSV(txtRaw.Text);

        private void AddRowToolStripMenuItem_Click(Object sender, EventArgs e)
        {
            _currentrow = listView1.SelectedIndices.Count > 0 ? listView1.Items[listView1.SelectedIndices[0]].Index : _csv.Lines.Count - 1;

            _csv.RowIndex = _currentrow;

            _csv.NewRow();
            LoadCSV(_csv);
        }

        private void ListView1_SelectedIndexChanged(Object sender, EventArgs e)
        {
            if(listView1.SelectedIndices.Count > 0)
                _currentrow = listView1.Items[listView1.SelectedIndices[0]].Index;
            _csv.RowIndex = _currentrow;
        }

        private void SaveToolStripMenuItem_Click(Object sender, EventArgs e)
        {

        }

        private void TabControl1_SelectedIndexChanged(Object sender, EventArgs e)
        {
            if(tabControl1.SelectedIndex == 0)
            {
                LoadCSV(txtRaw.Text);
            }
        }
    }
}
