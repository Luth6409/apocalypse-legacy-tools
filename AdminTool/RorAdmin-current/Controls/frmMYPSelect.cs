﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.frmMYPSelect
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using MypLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
  public class frmMYPSelect : Form
  {
    private IContainer components = (IContainer) null;
    private Label label1;
    private ComboBox comboBox1;
    private Button btnCancel;
    private Button btnOK;
    private Label label2;

    public List<MythicPackage> Packages { get; set; }

    public MythicPackage SelectedPackage
    {
      get
      {
        if (this.comboBox1.SelectedIndex > -1)
          return (MythicPackage) this.comboBox1.SelectedItem;
        return MythicPackage.NONE;
      }
    }

    public frmMYPSelect()
    {
      this.InitializeComponent();
    }

    private void frmMYPSelect_Load(object sender, EventArgs e)
    {
      foreach (int package in this.Packages)
        this.comboBox1.Items.Add((object) (MythicPackage) package);
      if (this.comboBox1.Items.Count <= 0)
        return;
      this.comboBox1.SelectedIndex = 0;
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.OK;
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;
    }

    private void frmMYPSelect_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Escape)
        this.btnCancel_Click((object) null, (EventArgs) null);
      if (e.KeyCode != Keys.Return)
        return;
      this.btnOK_Click((object) null, (EventArgs) null);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.label1 = new Label();
      this.comboBox1 = new ComboBox();
      this.btnCancel = new Button();
      this.btnOK = new Button();
      this.label2 = new Label();
      this.SuspendLayout();
      this.label1.AutoSize = true;
      this.label1.Location = new Point(21, 38);
      this.label1.Name = "label1";
      this.label1.Size = new Size(30, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "MYP";
      this.comboBox1.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Location = new Point(57, 35);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new Size(368, 21);
      this.comboBox1.TabIndex = 1;
      this.btnCancel.Location = new Point(140, 63);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new Size(75, 23);
      this.btnCancel.TabIndex = 7;
      this.btnCancel.Text = "&Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
      this.btnOK.Location = new Point(221, 63);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new Size(75, 23);
      this.btnOK.TabIndex = 6;
      this.btnOK.Text = "&Select";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new EventHandler(this.btnOK_Click);
      this.label2.AutoSize = true;
      this.label2.Location = new Point(21, 14);
      this.label2.Name = "label2";
      this.label2.Size = new Size(297, 13);
      this.label2.TabIndex = 8;
      this.label2.Text = "Source contains multiple mythic packages, please select one:";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(437, 94);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.btnCancel);
      this.Controls.Add((Control) this.btnOK);
      this.Controls.Add((Control) this.comboBox1);
      this.Controls.Add((Control) this.label1);
      this.Name = nameof (frmMYPSelect);
      this.Text = "MYP Select";
      this.Load += new EventHandler(this.frmMYPSelect_Load);
      this.KeyDown += new KeyEventHandler(this.frmMYPSelect_KeyDown);
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
