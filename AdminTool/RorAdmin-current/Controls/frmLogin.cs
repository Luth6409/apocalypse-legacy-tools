﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.frmLogin
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
  public class frmLogin : Form
  {
    private IContainer components = (IContainer) null;
    private Label label1;
    private TextBox txtUsername;
    private TextBox txtPassword;
    private Label label2;
    private Button btnLogin;
    private Button btnCancel;

    public frmLogin()
    {
      this.InitializeComponent();
    }

    public string Username
    {
      get
      {
        return this.txtUsername.Text;
      }
    }

    public string Password
    {
      get
      {
        return this.txtPassword.Text;
      }
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;
    }

    private void btnLogin_Click(object sender, EventArgs e)
    {
      if (this.txtUsername.Text.Trim().Length == 0)
      {
        int num = (int) MessageBox.Show("Username required", "Error");
        this.txtUsername.Focus();
      }
      else
      {
        Program.UIConfig["username", ""] = (object) this.Username;
        Program.UIConfig["password", ""] = (object) this.Password;
        Program.UIConfig.Save();
        this.DialogResult = DialogResult.OK;
      }
    }

    private void frmLogin_Load(object sender, EventArgs e)
    {
      this.txtUsername.Text = (string) (Program.UIConfig["username", ""] ?? (object) "");
      this.txtPassword.Text = (string) (Program.UIConfig["password", ""] ?? (object) "");
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.label1 = new Label();
      this.txtUsername = new TextBox();
      this.txtPassword = new TextBox();
      this.label2 = new Label();
      this.btnLogin = new Button();
      this.btnCancel = new Button();
      this.SuspendLayout();
      this.label1.AutoSize = true;
      this.label1.Location = new Point(25, 18);
      this.label1.Name = "label1";
      this.label1.Size = new Size(55, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Username";
      this.txtUsername.Location = new Point(86, 15);
      this.txtUsername.Name = "txtUsername";
      this.txtUsername.Size = new Size(260, 20);
      this.txtUsername.TabIndex = 1;
      this.txtPassword.Location = new Point(86, 41);
      this.txtPassword.Name = "txtPassword";
      this.txtPassword.PasswordChar = '*';
      this.txtPassword.Size = new Size(260, 20);
      this.txtPassword.TabIndex = 3;
      this.label2.AutoSize = true;
      this.label2.Location = new Point(25, 44);
      this.label2.Name = "label2";
      this.label2.Size = new Size(53, 13);
      this.label2.TabIndex = 2;
      this.label2.Text = "Password";
      this.btnLogin.Location = new Point(202, 72);
      this.btnLogin.Name = "btnLogin";
      this.btnLogin.Size = new Size(75, 23);
      this.btnLogin.TabIndex = 4;
      this.btnLogin.Text = "&Launch";
      this.btnLogin.UseVisualStyleBackColor = true;
      this.btnLogin.Click += new EventHandler(this.btnLogin_Click);
      this.btnCancel.Location = new Point(121, 72);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new Size(75, 23);
      this.btnCancel.TabIndex = 5;
      this.btnCancel.Text = "&Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(371, 104);
      this.Controls.Add((Control) this.btnCancel);
      this.Controls.Add((Control) this.btnLogin);
      this.Controls.Add((Control) this.txtPassword);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.txtUsername);
      this.Controls.Add((Control) this.label1);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.Name = nameof (frmLogin);
      this.Text = nameof (frmLogin);
      this.Load += new EventHandler(this.frmLogin_Load);
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
