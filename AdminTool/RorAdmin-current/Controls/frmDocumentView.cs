﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.frmDocumentView
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using MypLib;
using ScintillaNET;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
  public class frmDocumentView : BaseViewer
  {
    private bool _disableUpdates = false;
    private IContainer components = (IContainer) null;
    private Scintilla scintilla;

    public frmDocumentView()
    {
      this.InitializeComponent();
    }

    private void SetLuaColors()
    {
      string str1 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
      string str2 = "0123456789";
      string str3 = "ŠšŒœŸÿÀàÁáÂâÃãÄäÅåÆæÇçÈèÉéÊêËëÌìÍíÎîÏïÐðÑñÒòÓóÔôÕõÖØøÙùÚúÛûÜüÝýÞþßö";
      this.scintilla.StyleResetDefault();
      this.scintilla.Styles[32].Font = "Consolas";
      this.scintilla.Styles[32].Size  = 10;
      this.scintilla.StyleClearAll();
      this.scintilla.Styles[0].ForeColor = Color.Silver;
      this.scintilla.Styles[1].ForeColor = Color.Green;
      this.scintilla.Styles[2].ForeColor = Color.Green;
      this.scintilla.Styles[4].ForeColor = Color.Olive;
      this.scintilla.Styles[5].ForeColor = Color.Blue;
      this.scintilla.Styles[13].ForeColor = Color.BlueViolet;
      this.scintilla.Styles[14].ForeColor = Color.DarkSlateBlue;
      this.scintilla.Styles[15].ForeColor = Color.DarkSlateBlue;
      this.scintilla.Styles[6].ForeColor = Color.Red;
      this.scintilla.Styles[7].ForeColor = Color.Red;
      this.scintilla.Styles[8].ForeColor = Color.Red;
      this.scintilla.Styles[12].BackColor = Color.Pink;
      this.scintilla.Styles[10].ForeColor = Color.Purple;
      this.scintilla.Styles[9].ForeColor = Color.Maroon;
      this.scintilla.Lexer = (Lexer)15;
      this.scintilla.WordChars = str1 + str2 + str3;
      this.scintilla.SetKeywords(0, "and break do else elseif end for function if in local nil not or repeat return then until while false true goto");
      this.scintilla.SetKeywords(1, "assert collectgarbage dofile error _G getmetatable ipairs loadfile next pairs pcall print rawequal rawget rawset setmetatable tonumber tostring type _VERSION xpcall string table math coroutine io os debug getfenv gcinfo load loadlib loadstring require select setfenv unpack _LOADED LUA_PATH _REQUIREDNAME package rawlen package bit32 utf8 _ENV");
      this.scintilla.SetKeywords(2, "string.byte string.char string.dump string.find string.format string.gsub string.len string.lower string.rep string.sub string.upper table.concat table.insert table.remove table.sort math.abs math.acos math.asin math.atan math.atan2 math.ceil math.cos math.deg math.exp math.floor math.frexp math.ldexp math.log math.max math.min math.pi math.pow math.rad math.random math.randomseed math.sin math.sqrt math.tan string.gfind string.gmatch string.match string.reverse string.pack string.packsize string.unpack table.foreach table.foreachi table.getn table.setn table.maxn table.pack table.unpack table.move math.cosh math.fmod math.huge math.log10 math.modf math.mod math.sinh math.tanh math.maxinteger math.mininteger math.tointeger math.type math.ult bit32.arshift bit32.band bit32.bnot bit32.bor bit32.btest bit32.bxor bit32.extract bit32.replace bit32.lrotate bit32.lshift bit32.rrotate bit32.rshift utf8.char utf8.charpattern utf8.codes utf8.codepoint utf8.len utf8.offset");
      this.scintilla.SetKeywords(3, "coroutine.create coroutine.resume coroutine.status coroutine.wrap coroutine.yield io.close io.flush io.input io.lines io.open io.output io.read io.tmpfile io.type io.write io.stdin io.stdout io.stderr os.clock os.date os.difftime os.execute os.exit os.getenv os.remove os.rename os.setlocale os.time os.tmpname coroutine.isyieldable coroutine.running io.popen module package.loaders package.seeall package.config package.searchers package.searchpath require package.cpath package.loaded package.loadlib package.path package.preload");
      this.scintilla.SetProperty("fold", "1");
      this.scintilla.SetProperty("fold.compact", "1");
      this.scintilla.Margins[2].Type = (MarginType)0;
      this.scintilla.Margins[2].Mask = 4261412864U;
      this.scintilla.Margins[2].Sensitive  = true;
      this.scintilla.Margins[2].Width = 20;
      for (int index = 25; index <= 31; ++index)
      {
        this.scintilla.Markers[index].SetForeColor(SystemColors.ControlLightLight);
        this.scintilla.Markers[index].SetBackColor(SystemColors.ControlDark);
      }
      this.scintilla.Markers[30].Symbol = (MarkerSymbol)12;
      this.scintilla.Markers[31].Symbol = (MarkerSymbol)14;
      this.scintilla.Markers[25].Symbol = (MarkerSymbol)13;
      this.scintilla.Markers[27].Symbol = (MarkerSymbol)11;
      this.scintilla.Markers[26].Symbol = (MarkerSymbol)15;
      this.scintilla.Markers[29].Symbol = (MarkerSymbol)9;
      this.scintilla.Markers[28].Symbol = (MarkerSymbol)10;
      this.scintilla.AutomaticFold = (AutomaticFold)7;
    }

    private void SetXmlColors()
    {
      this.scintilla.StyleResetDefault();
      this.scintilla.Styles[32].Font = "Consolas";
      this.scintilla.Styles[32].Size = 10;
      this.scintilla.StyleClearAll();
      this.scintilla.Lexer = (Lexer)5;
      this.scintilla.Margins[0].Width = 20;
      this.scintilla.SetProperty("fold", "1");
      this.scintilla.SetProperty("fold.compact", "1");
      this.scintilla.SetProperty("fold.html", "1");
      this.scintilla.Margins[2].Type = (MarginType)0;
      this.scintilla.Margins[2].Mask = 4261412864U;
      this.scintilla.Margins[2].Sensitive = true;
      this.scintilla.Margins[2].Width = 20;
      for (int index = 25; index <= 31; ++index)
      {
        this.scintilla.Markers[index].SetForeColor(SystemColors.ControlLightLight);
        this.scintilla.Markers[index].SetBackColor(SystemColors.ControlDark);
      }
      this.scintilla.Markers[30].Symbol = (MarkerSymbol) 12;
      this.scintilla.Markers[30].SetBackColor(SystemColors.ControlText);
      this.scintilla.Markers[31].Symbol = (MarkerSymbol) 14;
      this.scintilla.Markers[25].Symbol = (MarkerSymbol) 13;
      this.scintilla.Markers[25].SetBackColor(SystemColors.ControlText);
      this.scintilla.Markers[27].Symbol = (MarkerSymbol) 11;
      this.scintilla.Markers[26].Symbol = (MarkerSymbol) 15;
      this.scintilla.Markers[29].Symbol = (MarkerSymbol) 9;
      this.scintilla.Markers[28].Symbol = (MarkerSymbol) 10;
      this.scintilla.AutomaticFold  = (AutomaticFold)7;
      this.scintilla.StyleResetDefault();
      this.scintilla.Styles[32].Font = "Courier";
      this.scintilla.Styles[32].Size = 10;
      this.scintilla.StyleClearAll();
      this.scintilla.Styles[3].ForeColor = Color.Red;
      this.scintilla.Styles[10].ForeColor = Color.Red;
      this.scintilla.Styles[9].ForeColor = Color.Green;
      this.scintilla.Styles[1].ForeColor = Color.Blue;
      this.scintilla.Styles[11].ForeColor = Color.Blue;
      this.scintilla.Styles[6].ForeColor = Color.DeepPink;
      this.scintilla.Styles[7].ForeColor = Color.DeepPink;
    }

    public override void LoadEntry(MYPManager manager, MYP.AssetInfo entry)
    {
      this._disableUpdates = true;
      base.LoadEntry(manager, entry);
      if (entry.Name.ToUpper().EndsWith(".XML") || entry.Name.ToUpper().EndsWith(".MOD"))
        this.SetXmlColors();
      else if (entry.Name.ToUpper().EndsWith(".LUA"))
        this.SetLuaColors();
      ((Control) this.scintilla).Text = Encoding.ASCII.GetString(manager.GetAsset(entry));
      this._disableUpdates = false;
    }

    public override void Save()
    {
      this.Manager.UpdateAsset(this._entry, Encoding.ASCII.GetBytes(((Control) this.scintilla).Text));
    }

    private void scintilla_TextChanged(object sender, EventArgs e)
    {
      if (this._disableUpdates)
        return;
      this.Manager.UpdateAsset(this._entry, Encoding.ASCII.GetBytes(((Control) this.scintilla).Text));
    }

    private void InitializeComponent()
    {
      this.scintilla = new Scintilla();
      this.SuspendLayout();
      this.scintilla.BorderStyle = BorderStyle.FixedSingle;
      ((Control) this.scintilla).Dock = DockStyle.Fill;
      this.scintilla.Lexer = (Lexer) 5;
      ((Control) this.scintilla).Location = new Point(0, 0);
      ((Control) this.scintilla).Name = "scintilla";
      ((Control) this.scintilla).Size = new Size(820, 497);
      ((Control) this.scintilla).TabIndex = 2;
      ((Control) this.scintilla).TextChanged += new EventHandler(this.scintilla_TextChanged);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Controls.Add((Control) this.scintilla);
      this.Name = nameof (frmDocumentView);
      this.ResumeLayout(false);
    }
  }
}
