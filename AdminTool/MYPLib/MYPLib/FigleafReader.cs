﻿// Decompiled with JetBrains decompiler
// Type: MypLib.FigleafReader
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MypLib
{
  public class FigleafReader
  {
    public byte[] Header = new byte[4];
    public List<string> Records1 = new List<string>();
    public List<string> Records2 = new List<string>();
    public Dictionary<byte, FigleafReader.RecordList> Records = new Dictionary<byte, FigleafReader.RecordList>();
    public uint TocCount;
    public uint FileSize;
    public static MYPManager Manager;
    public static uint Last;

    public FigleafReader(MYPManager manager)
    {
      FigleafReader.Manager = manager;
    }

    public StringBuilder PrintRecordsBinaryText(byte type)
    {
      Console.WriteLine("Generating " + (object) type);
      StringBuilder stringBuilder = new StringBuilder();
      foreach (FigleafReader.Record record in this.Records[type].Records)
      {
        foreach (byte num in record.Data)
          stringBuilder.Append(num.ToString("X").PadLeft(2, '0') + " ");
        stringBuilder.AppendLine();
      }
      if (this.Records[type].UnkData != null)
      {
        stringBuilder.AppendLine("--------UNK---------");
        int num1 = 0;
        foreach (byte num2 in this.Records[type].UnkData)
        {
          ++num1;
          stringBuilder.Append(num2.ToString("X").PadLeft(2, '0') + " ");
          if (num1 % this.Records[type].Records[0].Data.Length == 0 && (uint) num1 > 0U)
            stringBuilder.AppendLine();
        }
      }
      return stringBuilder;
    }

    public byte[] RawData(byte type)
    {
      MemoryStream memoryStream = new MemoryStream();
      foreach (FigleafReader.Record record in this.Records[type].Records)
        memoryStream.Write(record.Data, 0, record.Data.Length);
      if (this.Records[type].UnkData != null)
        memoryStream.Write(this.Records[type].UnkData, 0, this.Records[type].UnkData.Length);
      return memoryStream.ToArray();
    }

    public StringBuilder PrintRecordsString(byte type, int offset = 0)
    {
      Console.WriteLine("Generating " + (object) type);
      StringBuilder stringBuilder = new StringBuilder();
      foreach (FigleafReader.Record record in this.Records[type].Records)
        stringBuilder.AppendLine(record.ToString(offset));
      return stringBuilder;
    }

    public void Load(Stream stream)
    {
      BinaryReader reader = new BinaryReader(stream);
      stream.Read(this.Header, 0, 4);
      this.TocCount = reader.ReadUInt32();
      this.FileSize = reader.ReadUInt32();
      this.LoadRecords1(reader);
      this.LoadRecords2(reader);
      this.LoadRecords<FigleafReader.CharacterArt>(reader, (byte) 2, 36U, 72);
      this.LoadRecords<FigleafReader.FigureParts>(reader, (byte) 3, 48U, 48);
      this.LoadRecords<FigleafReader.ArtSwitches>(reader, (byte) 4, 60U, 164);
      this.LoadRecords<FigleafReader.CharacterDecals>(reader, (byte) 5, 72U, 60);
      this.LoadRecords<FigleafReader.CharacterMeshes>(reader, (byte) 6, 84U, 20);
      this.LoadRecords<FigleafReader.Textures>(reader, (byte) 7, 96U, 60);
      this.LoadRecords<FigleafReader.Record>(reader, (byte) 8, 108U, 60);
      this.LoadRecords<FigleafReader.Fixtures>(reader, (byte) 9, 120U, 72);
      this.LoadRecords<FigleafReader.Record>(reader, (byte) 10, 132U, 40);
      this.LoadRecords<FigleafReader.Record11>(reader, (byte) 11, 144U, 12);
      this.LoadRecords<FigleafReader.Regions>(reader, (byte) 12, 156U, 32);
      this.LoadVarRecords(reader, (byte) 13, 168U);
      this.LoadVarRecords(reader, (byte) 14, 180U);
      this.LoadVarRecords(reader, (byte) 15, 192U);
      this.LoadVarRecords(reader, (byte) 16, 216U);
    }

    public bool Compare(FigleafReader fig)
    {
      if (this.Records.Keys.Count != fig.Records.Keys.Count)
      {
        Console.WriteLine("TOC Entry count mismatch");
        return false;
      }
      foreach (byte key in this.Records.Keys)
      {
        if (this.Records[key].Records.Count != fig.Records[key].Records.Count)
        {
          Console.WriteLine("Type: " + (object) key + " asset count mismatch");
          return false;
        }
        for (int index1 = 0; index1 < this.Records[key].Records.Count; ++index1)
        {
          if (this.Records[key].Records[index1].Data.Length != fig.Records[key].Records[index1].Data.Length)
          {
            Console.WriteLine("Type: " + (object) key + " asset index: " + (object) index1 + " data length mismatch");
            return false;
          }
          for (int index2 = 0; index2 < this.Records[key].Records[index1].Data.Length; ++index2)
          {
            if ((int) this.Records[key].Records[index1].Data[index2] != (int) fig.Records[key].Records[index1].Data[index2])
            {
              Console.WriteLine("Type: " + (object) key + " asset index: " + (object) index1 + " data value mismatch at offset:" + (object) index2);
              return false;
            }
          }
        }
      }
      return true;
    }

    public uint SwapBytes(uint x)
    {
      x = x >> 16 | x << 16;
      return (x & 4278255360U) >> 8 | (uint) (((int) x & 16711935) << 8);
    }

    public void WriteTo(Stream stream)
    {
      BinaryWriter writer = new BinaryWriter(stream);
      stream.Write(new byte[828], 0, 828);
      stream.Position = 0L;
      writer.Write(new char[4]{ 'b', 'd', 'L', 'F' });
      writer.Write(32U);
      writer.Write(0U);
      this.WriteRecords(writer, (byte) 0, 12U, 828U, 0U, false, (byte[]) null);
      writer.Write((byte) 0);
      this.WriteRecords(writer, (byte) 1, 20U, (uint) stream.Position, 16426720U, true, (byte[]) null);
      this.WriteRecords(writer, (byte) 2, 36U, (uint) stream.Position, 0U, true, (byte[]) null);
      this.WriteRecords(writer, (byte) 3, 48U, (uint) stream.Position, 0U, true, (byte[]) null);
      this.WriteRecords(writer, (byte) 4, 60U, (uint) stream.Position, 0U, true, (byte[]) null);
      this.WriteRecords(writer, (byte) 5, 72U, (uint) stream.Position, 0U, true, (byte[]) null);
      this.WriteRecords(writer, (byte) 6, 84U, (uint) stream.Position, 0U, true, (byte[]) null);
      this.WriteRecords(writer, (byte) 7, 96U, (uint) stream.Position, 0U, true, (byte[]) null);
      this.WriteRecords(writer, (byte) 8, 108U, (uint) stream.Position, 0U, true, (byte[]) null);
      this.WriteRecords(writer, (byte) 9, 120U, (uint) stream.Position, 0U, true, (byte[]) null);
      this.WriteRecords(writer, (byte) 10, 132U, (uint) stream.Position, 0U, true, (byte[]) null);
      this.WriteRecords(writer, (byte) 11, 144U, (uint) stream.Position, 0U, true, FigleafReader.Record11.Serialize(this.Records[(byte) 11].Records.Select<FigleafReader.Record, FigleafReader.Record11>((Func<FigleafReader.Record, FigleafReader.Record11>) (e => (FigleafReader.Record11) e)).ToList<FigleafReader.Record11>()));
      this.WriteRecords(writer, (byte) 12, 156U, (uint) stream.Position, 0U, true, FigleafReader.Regions.Serialize(this.Records[(byte) 12].Records.Select<FigleafReader.Record, FigleafReader.Regions>((Func<FigleafReader.Record, FigleafReader.Regions>) (e => (FigleafReader.Regions) e)).ToList<FigleafReader.Regions>()));
      this.WriteVarRecords(writer, (byte) 13, 168U, (uint) stream.Position);
      this.WriteVarRecords(writer, (byte) 14, 180U, (uint) stream.Position);
      this.WriteVarRecords(writer, (byte) 15, 192U, (uint) stream.Position);
      this.WriteVarRecords(writer, (byte) 16, 216U, (uint) stream.Position);
      stream.Position = 8L;
      writer.Write((uint) stream.Length);
    }

    public void WriteRecords(
      BinaryWriter writer,
      byte type,
      uint tocOffset,
      uint startAt = 828,
      uint unk = 0,
      bool writeSize = true,
      byte[] data = null)
    {
      if (!this.Records.ContainsKey(type))
        return;
      writer.BaseStream.Position = (long) tocOffset;
      if (unk > 0U)
        writer.Write(unk);
      writer.Write((uint) this.Records[type].Records.Count);
      writer.Write(startAt);
      if (writeSize)
      {
        if (data == null)
        {
          int num1 = this.Records[type].Records.Sum<FigleafReader.Record>((Func<FigleafReader.Record, int>) (e => e.Data.Length));
          uint num2 = 0;
          if (this.Records[type].UnkData != null)
            num2 = (uint) this.Records[type].UnkData.Length;
          int num3 = (int) num2;
          uint num4 = (uint) (num1 + num3);
          writer.Write(num4);
        }
        else
          writer.Write((uint) data.Length);
      }
      writer.BaseStream.Position = (long) startAt;
      if (data == null)
      {
        foreach (FigleafReader.Record record in this.Records[type].Records)
          writer.BaseStream.Write(record.Data, 0, record.Data.Length);
        if (this.Records[type].UnkData == null)
          return;
        writer.BaseStream.Write(this.Records[type].UnkData, 0, this.Records[type].UnkData.Length);
      }
      else
        writer.BaseStream.Write(data, 0, data.Length);
    }

    public void WriteVarRecords(BinaryWriter writer, byte type, uint tocOffset, uint startAt)
    {
      if (!this.Records.ContainsKey(type))
        return;
      writer.BaseStream.Position = (long) tocOffset;
      writer.Write((uint) this.Records[type].Records.Count);
      writer.Write(startAt);
      uint num = (uint) this.Records[type].Records.Sum<FigleafReader.Record>((Func<FigleafReader.Record, int>) (e => e.Data.Length));
      writer.Write(num);
      writer.BaseStream.Position = (long) startAt;
      foreach (FigleafReader.Record record in this.Records[type].Records)
        writer.BaseStream.Write(record.Data, 0, record.Data.Length);
    }

    public void LoadRecords1(BinaryReader reader)
    {
      uint num1 = reader.ReadUInt32();
      uint num2 = reader.ReadUInt32();
      reader.BaseStream.Position = (long) num2;
      this.Records[(byte) 0] = new FigleafReader.RecordList();
      this.Records[(byte) 0].Records = new List<FigleafReader.Record>();
      for (int index = 0; (long) index < (long) num1; ++index)
      {
        long position = reader.BaseStream.Position;
        reader.BaseStream.Position += 5L;
        byte num3 = reader.ReadByte();
        int num4 = 0;
        for (; num3 > (byte) 0; num3 = reader.ReadByte())
          ++num4;
        byte[] numArray = new byte[num4 + 6 + 1];
        reader.BaseStream.Position = position;
        reader.Read(numArray, 0, numArray.Length);
        FigleafReader.Strings1 strings1 = new FigleafReader.Strings1();
        strings1.SetData(this, numArray);
        this.Records[(byte) 0].Records.Add((FigleafReader.Record) strings1);
      }
    }

    public void LoadRecords2(BinaryReader reader)
    {
      reader.BaseStream.Position = 20L;
      int num1 = (int) reader.ReadUInt32();
      uint num2 = reader.ReadUInt32();
      uint num3 = reader.ReadUInt32();
      uint num4 = reader.ReadUInt32();
      this.Records[(byte) 1] = new FigleafReader.RecordList();
      this.Records[(byte) 1].Records = new List<FigleafReader.Record>();
      reader.BaseStream.Position = (long) num3;
      int num5 = 0;
      for (int index = 0; (long) index < (long) num2; ++index)
      {
        long position = reader.BaseStream.Position;
        byte num6 = reader.ReadByte();
        int num7 = 0;
        for (; num6 > (byte) 0; num6 = reader.ReadByte())
          ++num7;
        byte[] numArray = new byte[num7 + 1];
        reader.BaseStream.Position = position;
        reader.Read(numArray, 0, num7 + 1);
        num5 += numArray.Length;
        FigleafReader.Strings2 strings2 = new FigleafReader.Strings2();
        strings2.SetData(this, numArray);
        this.Records[(byte) 1].Records.Add((FigleafReader.Record) strings2);
      }
      Console.WriteLine("EntryCount:" + (object) num2 + " offset:" + (object) num3 + " DataSize:" + (object) num4 + " size:" + (object) (reader.BaseStream.Position - (long) num3));
    }

    public void LoadRecords<T>(BinaryReader reader, byte type, uint tocOffset, int size) where T : FigleafReader.Record, new()
    {
      reader.BaseStream.Position = (long) tocOffset;
      uint num1 = reader.ReadUInt32();
      uint num2 = reader.ReadUInt32();
      uint num3 = reader.ReadUInt32();
      reader.BaseStream.Position = (long) num2;
      this.Records[type] = new FigleafReader.RecordList();
      this.Records[type].Records = new List<FigleafReader.Record>();
      int tableOffset = 0;
      byte[] numArray1 = new byte[size];
      for (int index = 0; (long) index < (long) num1; ++index)
      {
        byte[] numArray2 = new byte[size];
        reader.Read(numArray2, 0, size);
        tableOffset += size;
        new T();
        T obj1 = new T();
        obj1.Type = type;
        obj1.Counted = (long) index < (long) num1;
        T obj2 = obj1;
        obj2.SetData(this, numArray2);
        this.Records[type].Records.Add((FigleafReader.Record) obj2);
      }
      if ((long) num3 != reader.BaseStream.Position - (long) num2)
      {
        this.Records[type].UnkData = new byte[(long) num3 - (reader.BaseStream.Position - (long) num2)];
        reader.Read(this.Records[type].UnkData, 0, this.Records[type].UnkData.Length);
      }
      for (int recordIndex = 0; recordIndex < this.Records[type].Records.Count; ++recordIndex)
        this.Records[type].Records[recordIndex].SetTableData(this.Records[type].UnkData, tableOffset, recordIndex);
      this.Records[type].TableSize = (uint) tableOffset;
      Console.WriteLine("EntryCount:" + (object) num1 + " offset:" + (object) num2 + " DataSize:" + (object) num3 + " size:" + (object) (reader.BaseStream.Position - (long) num2) + " recSize:" + (object) size);
    }

    public void LoadVarRecords(BinaryReader reader, byte type, uint tocOffset)
    {
      reader.BaseStream.Position = (long) tocOffset;
      uint num1 = reader.ReadUInt32();
      uint num2 = reader.ReadUInt32();
      uint num3 = reader.ReadUInt32();
      reader.BaseStream.Position = (long) num2;
      this.Records[type] = new FigleafReader.RecordList();
      this.Records[type].Records = new List<FigleafReader.Record>();
      for (int index = 0; (long) index < (long) num1; ++index)
      {
        long position = reader.BaseStream.Position;
        reader.BaseStream.Position += 16L;
        int length = (int) reader.ReadUInt32();
        reader.BaseStream.Position = position;
        byte[] numArray = new byte[length];
        reader.Read(numArray, 0, numArray.Length);
        FigleafReader.Record record = new FigleafReader.Record(numArray)
        {
          Type = type
        };
        this.Records[type].Records.Add(record);
      }
      Console.WriteLine("VAR EntryCount:" + (object) num1 + " offset:" + (object) num2 + " DataSize:" + (object) num3 + " size:" + (object) (reader.BaseStream.Position - (long) num2));
    }

    public class TableData
    {
    }

    public class Record
    {
      public List<FigleafReader.TableData> TableData = new List<FigleafReader.TableData>();
      public bool Counted = true;
      public byte Type;
      private byte[] _data;
      private byte[] _tableData;
      protected FigleafReader Fr;

      public byte[] Data
      {
        get
        {
          return this._data;
        }
        set
        {
          this._data = value;
        }
      }

      public override string ToString()
      {
        return Encoding.ASCII.GetString(this.Data);
      }

      public string ToString(int offset)
      {
        return Encoding.ASCII.GetString(this.Data, offset, this.Data.Length - offset);
      }

      public Record()
      {
      }

      public virtual void SetData(FigleafReader reader, byte[] data)
      {
        this.Fr = reader;
        this._data = data;
      }

      public virtual void SetTableData(byte[] data, int tableOffset, int recordIndex)
      {
        this._tableData = data;
      }

      public Record(byte[] data)
      {
        this._data = data;
      }
    }

    public class Strings1 : FigleafReader.Record
    {
      public uint Unk1 { get; set; }

      public byte Type { get; set; }

      public string Name { get; set; }

      public byte Unk2 { get; set; }

      public byte Unk3 { get; set; }

      public override void SetData(FigleafReader fr, byte[] data)
      {
        base.SetData(fr, data);
        MemoryStream memoryStream = new MemoryStream(data);
        BinaryReader binaryReader = new BinaryReader((Stream) memoryStream);
        this.Unk1 = binaryReader.ReadUInt32();
        this.Type = binaryReader.ReadByte();
        this.Name = Encoding.ASCII.GetString(data, 5, data.Length - 5);
        memoryStream.Position = (long) (5 + this.Name.Length - 2);
        this.Unk2 = binaryReader.ReadByte();
        this.Unk3 = binaryReader.ReadByte();
      }
    }

    public class Strings2 : FigleafReader.Record
    {
      public string Name { get; set; }

      public override void SetData(FigleafReader fr, byte[] data)
      {
        base.SetData(fr, data);
        BinaryReader binaryReader = new BinaryReader((Stream) new MemoryStream(data));
        this.Name = Encoding.ASCII.GetString(data);
      }
    }

    public class CharacterArt : FigleafReader.Record
    {
      public uint SourceIndex { get; set; }

      public uint Unk1 { get; set; }

      public uint Unk2 { get; set; }

      public uint Unk3 { get; set; }

      public uint Race { get; set; }

      public uint Gender { get; set; }

      public uint Unk6 { get; set; }

      public uint Unk7 { get; set; }

      public uint Animations { get; set; }

      public uint Unk9 { get; set; }

      public uint PartCount { get; set; }

      public uint Unk11 { get; set; }

      public uint Unk12 { get; set; }

      public uint Unk13 { get; set; }

      public uint Unk14 { get; set; }

      public uint Unk15 { get; set; }

      public uint Unk16 { get; set; }

      public uint Unk17 { get; set; }

      public string Name { get; set; }

      public override void SetData(FigleafReader fr, byte[] data)
      {
        base.SetData(fr, data);
        BinaryReader binaryReader = new BinaryReader((Stream) new MemoryStream(data));
        this.SourceIndex = binaryReader.ReadUInt32();
        this.Unk1 = binaryReader.ReadUInt32();
        this.Unk2 = binaryReader.ReadUInt32();
        this.Unk3 = binaryReader.ReadUInt32();
        this.Race = binaryReader.ReadUInt32();
        this.Gender = binaryReader.ReadUInt32();
        this.Unk6 = binaryReader.ReadUInt32();
        this.Unk7 = binaryReader.ReadUInt32();
        this.Animations = binaryReader.ReadUInt32();
        this.Unk9 = binaryReader.ReadUInt32();
        this.PartCount = binaryReader.ReadUInt32();
        this.Unk11 = binaryReader.ReadUInt32();
        this.Unk12 = binaryReader.ReadUInt32();
        this.Unk13 = binaryReader.ReadUInt32();
        this.Unk14 = binaryReader.ReadUInt32();
        this.Unk15 = binaryReader.ReadUInt32();
        this.Unk16 = binaryReader.ReadUInt32();
        this.Unk17 = binaryReader.ReadUInt32();
        this.Name = fr.Records[(byte) 0].Records[(int) this.SourceIndex].ToString(5);
      }
    }

    public class FigureParts : FigleafReader.Record
    {
      public uint SourceIndex { get; set; }

      public uint Unk1 { get; set; }

      public uint Unk2 { get; set; }

      public uint Unk3 { get; set; }

      public uint Unk4 { get; set; }

      public uint Unk5 { get; set; }

      public uint Count { get; set; }

      public int DataStart { get; set; }

      public uint Geometry { get; set; }

      public int DataEnd { get; set; }

      public uint Attachments { get; set; }

      public uint Unk11 { get; set; }

      public int Diff { get; set; }

      public string Name { get; set; }

      public override void SetData(FigleafReader fr, byte[] data)
      {
        base.SetData(fr, data);
        BinaryReader binaryReader = new BinaryReader((Stream) new MemoryStream(data));
        this.SourceIndex = binaryReader.ReadUInt32();
        this.Unk1 = binaryReader.ReadUInt32();
        this.Unk2 = binaryReader.ReadUInt32();
        this.Unk3 = binaryReader.ReadUInt32();
        this.Unk4 = binaryReader.ReadUInt32();
        this.Unk5 = binaryReader.ReadUInt32();
        this.Count = binaryReader.ReadUInt32();
        this.DataEnd = binaryReader.ReadInt32();
        this.Geometry = binaryReader.ReadUInt32();
        this.DataStart = binaryReader.ReadInt32();
        this.Attachments = binaryReader.ReadUInt32();
        this.Unk11 = binaryReader.ReadUInt32();
        this.Diff = this.DataStart - this.DataEnd;
        this.Name = fr.Records[(byte) 0].Records[(int) this.SourceIndex].ToString(5);
      }

      public override void SetTableData(byte[] data, int tableOffset, int recordIndex)
      {
        base.SetTableData(data, tableOffset, recordIndex);
        if (this.DataStart == 0)
          return;
        MemoryStream memoryStream = new MemoryStream(data);
        memoryStream.Position = (long) (this.DataStart - tableOffset);
        BinaryReader reader = new BinaryReader((Stream) memoryStream);
        for (int index = 0; (long) index < (long) this.Count; ++index)
        {
          FigleafReader.FigureParts.FigurePart figurePart = new FigleafReader.FigureParts.FigurePart();
          figurePart.Load(reader);
          this.TableData.Add((FigleafReader.TableData) figurePart);
        }
      }

      public class FigurePart : FigleafReader.TableData
      {
        public int Unk1 { get; set; }

        public int Unk2 { get; set; }

        public int Unk3 { get; set; }

        public int Unk4 { get; set; }

        public int Unk5 { get; set; }

        public int Unk6 { get; set; }

        public int Unk7 { get; set; }

        public int Unk8 { get; set; }

        public int Unk9 { get; set; }

        public int Unk10 { get; set; }

        public int Unk11 { get; set; }

        public int Unk12 { get; set; }

        public int Unk13 { get; set; }

        public int Unk14 { get; set; }

        public int Unk15 { get; set; }

        public int Unk16 { get; set; }

        public int Unk17 { get; set; }

        public int Unk18 { get; set; }

        public int Unk19 { get; set; }

        public int Unk20 { get; set; }

        public int Unk21 { get; set; }

        public int Unk22 { get; set; }

        public int Unk23 { get; set; }

        public int Unk24 { get; set; }

        public void Load(BinaryReader reader)
        {
          this.Unk1 = reader.ReadInt32();
          this.Unk2 = reader.ReadInt32();
          this.Unk3 = reader.ReadInt32();
          this.Unk4 = reader.ReadInt32();
          this.Unk5 = reader.ReadInt32();
          this.Unk6 = reader.ReadInt32();
          this.Unk7 = reader.ReadInt32();
          this.Unk8 = reader.ReadInt32();
          this.Unk9 = reader.ReadInt32();
          this.Unk10 = reader.ReadInt32();
          this.Unk11 = reader.ReadInt32();
          this.Unk12 = reader.ReadInt32();
          this.Unk13 = reader.ReadInt32();
          this.Unk14 = reader.ReadInt32();
          this.Unk15 = reader.ReadInt32();
          this.Unk16 = reader.ReadInt32();
          this.Unk17 = reader.ReadInt32();
          this.Unk18 = reader.ReadInt32();
          this.Unk19 = reader.ReadInt32();
          this.Unk20 = reader.ReadInt32();
          this.Unk21 = reader.ReadInt32();
          this.Unk22 = reader.ReadInt32();
          this.Unk23 = reader.ReadInt32();
          this.Unk24 = reader.ReadInt32();
        }
      }
    }

    public enum SwitchType
    {
      Mesh,
      Decal,
      Color,
    }

    public class ArtSwitches : FigleafReader.Record
    {
      public FigleafReader.SwitchType SwitchType { get; set; }

      public string Name { get; set; }

      public string NoneName { get; set; }

      public string MaleName { get; set; }

      public string FemaleName { get; set; }

      public string DwarfNoneName { get; set; }

      public string DwarfmaleName { get; set; }

      public string DwarffemaleName { get; set; }

      public string HumanNoneName { get; set; }

      public string HumanmaleName { get; set; }

      public string HumanfemaleName { get; set; }

      public string ChaosHumanNoneName { get; set; }

      public string ChaosHumanmaleName { get; set; }

      public string ChaosHumanfemaleName { get; set; }

      public string ElfNoneName { get; set; }

      public string ElfmaleName { get; set; }

      public string ElffemaleName { get; set; }

      public string DarkElfNoneName { get; set; }

      public string DarkElfmaleName { get; set; }

      public string DarkElffemaleName { get; set; }

      public string OrcNoneName { get; set; }

      public string OrcmaleName { get; set; }

      public string OrcfemaleName { get; set; }

      public string GoblinNoneName { get; set; }

      public string GoblinmaleName { get; set; }

      public string GoblinfemaleName { get; set; }

      public string BeastmanNoneName { get; set; }

      public string BeastmanmaleName { get; set; }

      public string BeastmanfemaleName { get; set; }

      public string SkavenNoneName { get; set; }

      public string SkavenmaleName { get; set; }

      public string SkavenfemaleName { get; set; }

      public string OgreNoneName { get; set; }

      public string OgremaleName { get; set; }

      public string OgrefemaleName { get; set; }

      public string ChaosWarriorNoneName { get; set; }

      public string ChaosWarriormaleName { get; set; }

      public string ChaosWarriorfemaleName { get; set; }

      public uint SourceIndex { get; set; }

      public uint Unk2 { get; set; }

      public uint Unk3 { get; set; }

      public uint Unk4 { get; set; }

      public uint Unk5 { get; set; }

      public uint Unk6 { get; set; }

      public uint Unk7 { get; set; }

      public uint Unk8 { get; set; }

      public uint Unk9 { get; set; }

      public uint Unk10 { get; set; }

      public uint Unk11 { get; set; }

      public uint Unk12 { get; set; }

      public uint Unk13 { get; set; }

      public uint Unk14 { get; set; }

      public uint Unk15 { get; set; }

      public uint Unk16 { get; set; }

      public uint Unk17 { get; set; }

      public uint Unk18 { get; set; }

      public uint Unk19 { get; set; }

      public uint Unk20 { get; set; }

      public uint Unk21 { get; set; }

      public uint Unk22 { get; set; }

      public uint Unk23 { get; set; }

      public uint Unk24 { get; set; }

      public uint Unk25 { get; set; }

      public uint Unk26 { get; set; }

      public uint Unk27 { get; set; }

      public uint Unk28 { get; set; }

      public uint Unk29 { get; set; }

      public uint Unk30 { get; set; }

      public uint Unk31 { get; set; }

      public uint Unk32 { get; set; }

      public uint Unk33 { get; set; }

      public uint Unk34 { get; set; }

      public uint Unk35 { get; set; }

      public uint Unk36 { get; set; }

      public uint Unk37 { get; set; }

      public uint Unk38 { get; set; }

      public uint Unk39 { get; set; }

      public uint Unk40 { get; set; }

      public override void SetData(FigleafReader fr, byte[] data)
      {
        base.SetData(fr, data);
        BinaryReader binaryReader = new BinaryReader((Stream) new MemoryStream(data));
        this.SwitchType = (FigleafReader.SwitchType) binaryReader.ReadUInt32();
        this.SourceIndex = binaryReader.ReadUInt32();
        this.Unk2 = binaryReader.ReadUInt32();
        this.Unk3 = binaryReader.ReadUInt32();
        this.Unk4 = binaryReader.ReadUInt32();
        this.Unk5 = binaryReader.ReadUInt32();
        this.Unk6 = binaryReader.ReadUInt32();
        this.Unk7 = binaryReader.ReadUInt32();
        this.Unk8 = binaryReader.ReadUInt32();
        this.Unk9 = binaryReader.ReadUInt32();
        this.Unk10 = binaryReader.ReadUInt32();
        this.Unk11 = binaryReader.ReadUInt32();
        this.Unk12 = binaryReader.ReadUInt32();
        this.Unk13 = binaryReader.ReadUInt32();
        this.Unk14 = binaryReader.ReadUInt32();
        this.Unk15 = binaryReader.ReadUInt32();
        this.Unk16 = binaryReader.ReadUInt32();
        this.Unk17 = binaryReader.ReadUInt32();
        this.Unk18 = binaryReader.ReadUInt32();
        this.Unk19 = binaryReader.ReadUInt32();
        this.Unk20 = binaryReader.ReadUInt32();
        this.Unk21 = binaryReader.ReadUInt32();
        this.Unk22 = binaryReader.ReadUInt32();
        this.Unk23 = binaryReader.ReadUInt32();
        this.Unk24 = binaryReader.ReadUInt32();
        this.Unk25 = binaryReader.ReadUInt32();
        this.Unk26 = binaryReader.ReadUInt32();
        this.Unk27 = binaryReader.ReadUInt32();
        this.Unk28 = binaryReader.ReadUInt32();
        this.Unk29 = binaryReader.ReadUInt32();
        this.Unk30 = binaryReader.ReadUInt32();
        this.Unk31 = binaryReader.ReadUInt32();
        this.Unk32 = binaryReader.ReadUInt32();
        this.Unk33 = binaryReader.ReadUInt32();
        this.Unk34 = binaryReader.ReadUInt32();
        this.Unk35 = binaryReader.ReadUInt32();
        this.Unk36 = binaryReader.ReadUInt32();
        this.Unk37 = binaryReader.ReadUInt32();
        this.Unk38 = binaryReader.ReadUInt32();
        this.Unk39 = binaryReader.ReadUInt32();
        this.Unk40 = binaryReader.ReadUInt32();
        this.Name = fr.Records[(byte) 0].Records[(int) this.SourceIndex].ToString(5);
        if (this.SwitchType == FigleafReader.SwitchType.Color)
          return;
        if ((long) this.Unk5 < (long) fr.Records[(byte) 0].Records.Count)
          this.NoneName = fr.Records[(byte) 0].Records[(int) this.Unk5].ToString(5);
        if ((long) this.Unk6 < (long) fr.Records[(byte) 0].Records.Count)
          this.MaleName = fr.Records[(byte) 0].Records[(int) this.Unk6].ToString(5);
        if ((long) this.Unk7 < (long) fr.Records[(byte) 0].Records.Count)
          this.FemaleName = fr.Records[(byte) 0].Records[(int) this.Unk7].ToString(5);
        if ((long) this.Unk8 < (long) fr.Records[(byte) 0].Records.Count)
          this.DwarfNoneName = fr.Records[(byte) 0].Records[(int) this.Unk8].ToString(5);
        if ((long) this.Unk9 < (long) fr.Records[(byte) 0].Records.Count)
          this.DwarfmaleName = fr.Records[(byte) 0].Records[(int) this.Unk9].ToString(5);
        if ((long) this.Unk10 < (long) fr.Records[(byte) 0].Records.Count)
          this.DwarffemaleName = fr.Records[(byte) 0].Records[(int) this.Unk10].ToString(5);
        if ((long) this.Unk11 < (long) fr.Records[(byte) 0].Records.Count)
          this.HumanNoneName = fr.Records[(byte) 0].Records[(int) this.Unk11].ToString(5);
        if ((long) this.Unk12 < (long) fr.Records[(byte) 0].Records.Count)
          this.HumanmaleName = fr.Records[(byte) 0].Records[(int) this.Unk12].ToString(5);
        if ((long) this.Unk13 < (long) fr.Records[(byte) 0].Records.Count)
          this.HumanfemaleName = fr.Records[(byte) 0].Records[(int) this.Unk13].ToString(5);
        if ((long) this.Unk14 < (long) fr.Records[(byte) 0].Records.Count)
          this.ChaosHumanNoneName = fr.Records[(byte) 0].Records[(int) this.Unk14].ToString(5);
        if ((long) this.Unk15 < (long) fr.Records[(byte) 0].Records.Count)
          this.ChaosHumanmaleName = fr.Records[(byte) 0].Records[(int) this.Unk15].ToString(5);
        if ((long) this.Unk16 < (long) fr.Records[(byte) 0].Records.Count)
          this.ChaosHumanfemaleName = fr.Records[(byte) 0].Records[(int) this.Unk16].ToString(5);
        if ((long) this.Unk17 < (long) fr.Records[(byte) 0].Records.Count)
          this.ElfNoneName = fr.Records[(byte) 0].Records[(int) this.Unk17].ToString(5);
        if ((long) this.Unk18 < (long) fr.Records[(byte) 0].Records.Count)
          this.ElfmaleName = fr.Records[(byte) 0].Records[(int) this.Unk18].ToString(5);
        if ((long) this.Unk19 < (long) fr.Records[(byte) 0].Records.Count)
          this.ElffemaleName = fr.Records[(byte) 0].Records[(int) this.Unk19].ToString(5);
        if ((long) this.Unk20 < (long) fr.Records[(byte) 0].Records.Count)
          this.DarkElfNoneName = fr.Records[(byte) 0].Records[(int) this.Unk20].ToString(5);
        if ((long) this.Unk21 < (long) fr.Records[(byte) 0].Records.Count)
          this.DarkElfmaleName = fr.Records[(byte) 0].Records[(int) this.Unk21].ToString(5);
        if ((long) this.Unk22 < (long) fr.Records[(byte) 0].Records.Count)
          this.DarkElffemaleName = fr.Records[(byte) 0].Records[(int) this.Unk22].ToString(5);
        if ((long) this.Unk23 < (long) fr.Records[(byte) 0].Records.Count)
          this.OrcNoneName = fr.Records[(byte) 0].Records[(int) this.Unk23].ToString(5);
        if ((long) this.Unk24 < (long) fr.Records[(byte) 0].Records.Count)
          this.OrcmaleName = fr.Records[(byte) 0].Records[(int) this.Unk24].ToString(5);
        if ((long) this.Unk25 < (long) fr.Records[(byte) 0].Records.Count)
          this.OrcfemaleName = fr.Records[(byte) 0].Records[(int) this.Unk25].ToString(5);
        if ((long) this.Unk26 < (long) fr.Records[(byte) 0].Records.Count)
          this.GoblinNoneName = fr.Records[(byte) 0].Records[(int) this.Unk26].ToString(5);
        if ((long) this.Unk27 < (long) fr.Records[(byte) 0].Records.Count)
          this.GoblinmaleName = fr.Records[(byte) 0].Records[(int) this.Unk27].ToString(5);
        if ((long) this.Unk28 < (long) fr.Records[(byte) 0].Records.Count)
          this.GoblinfemaleName = fr.Records[(byte) 0].Records[(int) this.Unk28].ToString(5);
        if ((long) this.Unk29 < (long) fr.Records[(byte) 0].Records.Count)
          this.BeastmanNoneName = fr.Records[(byte) 0].Records[(int) this.Unk29].ToString(5);
        if ((long) this.Unk30 < (long) fr.Records[(byte) 0].Records.Count)
          this.BeastmanmaleName = fr.Records[(byte) 0].Records[(int) this.Unk30].ToString(5);
        if ((long) this.Unk31 < (long) fr.Records[(byte) 0].Records.Count)
          this.BeastmanfemaleName = fr.Records[(byte) 0].Records[(int) this.Unk31].ToString(5);
        if ((long) this.Unk32 < (long) fr.Records[(byte) 0].Records.Count)
          this.SkavenNoneName = fr.Records[(byte) 0].Records[(int) this.Unk32].ToString(5);
        if ((long) this.Unk33 < (long) fr.Records[(byte) 0].Records.Count)
          this.SkavenmaleName = fr.Records[(byte) 0].Records[(int) this.Unk33].ToString(5);
        if ((long) this.Unk34 < (long) fr.Records[(byte) 0].Records.Count)
          this.SkavenfemaleName = fr.Records[(byte) 0].Records[(int) this.Unk34].ToString(5);
        if ((long) this.Unk35 < (long) fr.Records[(byte) 0].Records.Count)
          this.OgreNoneName = fr.Records[(byte) 0].Records[(int) this.Unk35].ToString(5);
        if ((long) this.Unk36 < (long) fr.Records[(byte) 0].Records.Count)
          this.OgremaleName = fr.Records[(byte) 0].Records[(int) this.Unk36].ToString(5);
        if ((long) this.Unk37 < (long) fr.Records[(byte) 0].Records.Count)
          this.OgrefemaleName = fr.Records[(byte) 0].Records[(int) this.Unk37].ToString(5);
        if ((long) this.Unk38 < (long) fr.Records[(byte) 0].Records.Count)
          this.ChaosWarriorNoneName = fr.Records[(byte) 0].Records[(int) this.Unk38].ToString(5);
        if ((long) this.Unk39 < (long) fr.Records[(byte) 0].Records.Count)
          this.ChaosWarriormaleName = fr.Records[(byte) 0].Records[(int) this.Unk39].ToString(5);
        if ((long) this.Unk40 >= (long) fr.Records[(byte) 0].Records.Count)
          return;
        this.ChaosWarriorfemaleName = fr.Records[(byte) 0].Records[(int) this.Unk40].ToString(5);
      }
    }

    public class Fixtures : FigleafReader.Record
    {
      public uint SourceIndex { get; set; }

      public uint A04 { get; set; }

      public uint A08 { get; set; }

      public string Name { get; set; }

      public override void SetData(FigleafReader fr, byte[] data)
      {
        base.SetData(fr, data);
        BinaryReader binaryReader = new BinaryReader((Stream) new MemoryStream(data));
        this.SourceIndex = binaryReader.ReadUInt32();
        this.A04 = binaryReader.ReadUInt32();
        this.A08 = binaryReader.ReadUInt32();
        this.Name = fr.Records[(byte) 0].Records[(int) this.SourceIndex].ToString(5);
      }
    }

    public class CharacterDecals : FigleafReader.Record
    {
      public uint A01 { get; set; }

      public uint A02 { get; set; }

      public uint A03 { get; set; }

      public uint A04 { get; set; }

      public uint A05 { get; set; }

      public uint A06 { get; set; }

      public uint A07 { get; set; }

      public uint A08 { get; set; }

      public uint A09 { get; set; }

      public uint A10 { get; set; }

      public uint A11 { get; set; }

      public uint A12 { get; set; }

      public uint A13 { get; set; }

      public uint A14 { get; set; }

      public uint A15 { get; set; }

      public string Name { get; set; }

      public override void SetData(FigleafReader fr, byte[] data)
      {
        base.SetData(fr, data);
        BinaryReader binaryReader = new BinaryReader((Stream) new MemoryStream(data));
        this.A01 = binaryReader.ReadUInt32();
        this.A02 = binaryReader.ReadUInt32();
        this.A03 = binaryReader.ReadUInt32();
        this.A04 = binaryReader.ReadUInt32();
        this.A05 = binaryReader.ReadUInt32();
        this.A06 = binaryReader.ReadUInt32();
        this.A07 = binaryReader.ReadUInt32();
        this.A08 = binaryReader.ReadUInt32();
        this.A09 = binaryReader.ReadUInt32();
        this.A10 = binaryReader.ReadUInt32();
        this.A11 = binaryReader.ReadUInt32();
        this.A12 = binaryReader.ReadUInt32();
        this.A13 = binaryReader.ReadUInt32();
        this.A14 = binaryReader.ReadUInt32();
        this.A15 = binaryReader.ReadUInt32();
      }
    }

    public class CharacterMeshes : FigleafReader.Record
    {
      public uint SourceIndex { get; set; }

      public uint A02 { get; set; }

      public uint A03 { get; set; }

      public uint A04 { get; set; }

      public uint A05 { get; set; }

      public string Name { get; set; }

      public override void SetData(FigleafReader fr, byte[] data)
      {
        base.SetData(fr, data);
        BinaryReader binaryReader = new BinaryReader((Stream) new MemoryStream(data));
        this.SourceIndex = binaryReader.ReadUInt32();
        this.A02 = binaryReader.ReadUInt32();
        this.A03 = binaryReader.ReadUInt32();
        this.A04 = binaryReader.ReadUInt32();
        this.A05 = binaryReader.ReadUInt32();
        this.Name = fr.Records[(byte) 0].Records[(int) this.SourceIndex].ToString(5);
      }
    }

    public class Textures : FigleafReader.Record
    {
      public uint SourceIndex { get; set; }

      public uint A02 { get; set; }

      public uint A03 { get; set; }

      public uint A04 { get; set; }

      public uint Width { get; set; }

      public uint Height { get; set; }

      public uint A07 { get; set; }

      public uint MipMaps { get; set; }

      public ulong A09 { get; set; }

      public ulong A10 { get; set; }

      public uint A11 { get; set; }

      public uint A12 { get; set; }

      public uint A13 { get; set; }

      public uint A14 { get; set; }

      public string Name { get; set; }

      public override void SetData(FigleafReader fr, byte[] data)
      {
        base.SetData(fr, data);
        BinaryReader binaryReader = new BinaryReader((Stream) new MemoryStream(data));
        this.SourceIndex = binaryReader.ReadUInt32();
        this.A02 = binaryReader.ReadUInt32();
        this.A03 = binaryReader.ReadUInt32();
        this.A04 = binaryReader.ReadUInt32();
        this.Width = (uint) binaryReader.ReadUInt16();
        this.Height = (uint) binaryReader.ReadUInt16();
        this.A07 = binaryReader.ReadUInt32();
        this.MipMaps = binaryReader.ReadUInt32();
        this.A09 = binaryReader.ReadUInt64();
        this.A10 = binaryReader.ReadUInt64();
        this.A11 = binaryReader.ReadUInt32();
        this.A12 = binaryReader.ReadUInt32();
        this.A13 = binaryReader.ReadUInt32();
        this.A14 = binaryReader.ReadUInt32();
        this.Name = fr.Records[(byte) 0].Records[(int) this.SourceIndex].ToString(5);
      }
    }

    public class Record11 : FigleafReader.Record
    {
      public ushort Unk1 { get; set; }

      public ushort Unk2 { get; set; }

      public uint DataStart { get; set; }

      public byte X { get; set; }

      public byte Y { get; set; }

      public ushort ZoneID { get; set; }

      public uint Diff { get; set; }

      public FigleafReader.Record11 Clone(ushort forZoneID)
      {
        FigleafReader.Record11 record11 = new FigleafReader.Record11()
        {
          Unk1 = this.Unk1,
          Unk2 = this.Unk2,
          ZoneID = forZoneID,
          X = this.X,
          Y = this.Y
        };
        for (int index = 0; index < this.TableData.Count; ++index)
        {
          FigleafReader.Record11.Record11Data record11Data = (FigleafReader.Record11.Record11Data) this.TableData[index];
          record11.TableData.Add((FigleafReader.TableData) new FigleafReader.Record11.Record11Data()
          {
            Unk1 = record11Data.Unk1,
            Unk2 = record11Data.Unk2,
            Unk3 = record11Data.Unk3,
            Unk4 = record11Data.Unk4,
            Unk5 = record11Data.Unk5,
            Unk6 = record11Data.Unk6,
            Unk7 = record11Data.Unk7,
            Unk8 = record11Data.Unk8
          });
        }
        return record11;
      }

      public override void SetData(FigleafReader fr, byte[] data)
      {
        base.SetData(fr, data);
        BinaryReader binaryReader = new BinaryReader((Stream) new MemoryStream(data));
        this.Unk1 = binaryReader.ReadUInt16();
        this.Unk2 = binaryReader.ReadUInt16();
        this.DataStart = binaryReader.ReadUInt32();
        this.ZoneID = binaryReader.ReadUInt16();
        this.X = binaryReader.ReadByte();
        this.Y = binaryReader.ReadByte();
        if (FigleafReader.Last > 0U)
          this.Diff = this.DataStart - FigleafReader.Last;
        FigleafReader.Last = this.DataStart;
      }

      public override void SetTableData(byte[] data, int tableOffset, int recordIndex)
      {
        base.SetTableData(data, tableOffset, recordIndex);
        if (this.DataStart == 0U)
          return;
        MemoryStream memoryStream = new MemoryStream(data);
        memoryStream.Position = (long) this.DataStart - (long) tableOffset + (long) (12 * recordIndex);
        BinaryReader reader = new BinaryReader((Stream) memoryStream);
        long position1 = memoryStream.Position;
        for (int index = 0; index < (int) this.Unk1; ++index)
        {
          FigleafReader.Record11.Record11Data record11Data = new FigleafReader.Record11.Record11Data();
          record11Data.Load(reader);
          this.TableData.Add((FigleafReader.TableData) record11Data);
          record11Data.LightmapName = this.Fr.Records[(byte) 0].Records[(int) record11Data.Unk1].ToString(5);
        }
        long position2 = memoryStream.Position;
      }

      public static byte[] Serialize(List<FigleafReader.Record11> lightData)
      {
        MemoryStream memoryStream = new MemoryStream();
        BinaryWriter dataWriter = new BinaryWriter((Stream) memoryStream);
        memoryStream.Write(new byte[12 * lightData.Count], 0, 12 * lightData.Count);
        for (int index = 0; index < lightData.Count; ++index)
        {
          FigleafReader.Record11 record11 = lightData[index];
          long num = dataWriter.BaseStream.Position - (long) (index * 12);
          foreach (FigleafReader.Record11.Record11Data record11Data in record11.TableData)
            record11Data.Save(dataWriter);
          long position = dataWriter.BaseStream.Position;
          dataWriter.BaseStream.Position = (long) (index * 12);
          dataWriter.Write(record11.Unk1);
          dataWriter.Write(record11.Unk2);
          dataWriter.Write((uint) num);
          dataWriter.Write(record11.ZoneID);
          dataWriter.Write(record11.X);
          dataWriter.Write(record11.Y);
          dataWriter.BaseStream.Position = position;
        }
        return memoryStream.ToArray();
      }

      public class Record11Data : FigleafReader.TableData
      {
        public ushort Unk1 { get; set; }

        public ushort Unk2 { get; set; }

        public ushort Unk3 { get; set; }

        public ushort Unk4 { get; set; }

        public ushort Unk5 { get; set; }

        public ushort Unk6 { get; set; }

        public ushort Unk7 { get; set; }

        public ushort Unk8 { get; set; }

        public float FTest { get; set; }

        public string LightmapName { get; set; }

        public void Load(BinaryReader reader)
        {
          this.LightmapName = "";
          this.Unk1 = reader.ReadUInt16();
          this.Unk2 = reader.ReadUInt16();
          this.Unk3 = reader.ReadUInt16();
          this.Unk4 = reader.ReadUInt16();
          this.Unk5 = reader.ReadUInt16();
          this.Unk6 = reader.ReadUInt16();
          this.Unk7 = reader.ReadUInt16();
          this.Unk8 = reader.ReadUInt16();
        }

        public void Save(BinaryWriter dataWriter)
        {
          dataWriter.Write(this.Unk1);
          dataWriter.Write(this.Unk2);
          dataWriter.Write(this.Unk3);
          dataWriter.Write(this.Unk4);
          dataWriter.Write(this.Unk5);
          dataWriter.Write(this.Unk6);
          dataWriter.Write(this.Unk7);
          dataWriter.Write(this.Unk8);
        }
      }
    }

    public class Zone : FigleafReader.TableData
    {
      public ushort ZoneID { get; set; }

      public string ZoneName { get; set; }

      public ushort MiniMapID { get; set; }

      public uint ZoneXOff { get; set; }

      public uint ZoneYOff { get; set; }

      public uint _ZoneX { get; set; }

      public uint _ZoneY { get; set; }

      public uint Unk1 { get; set; }

      public uint Unk2 { get; set; }

      public uint Unk3 { get; set; }

      public uint Unk4 { get; set; }

      public uint Unk5 { get; set; }

      public uint Unk6 { get; set; }

      public uint Unk7 { get; set; }

      public uint Unk8 { get; set; }

      public uint Unk9 { get; set; }

      public uint Unk10 { get; set; }

      public uint Unk11 { get; set; }

      public Zone()
      {
        this.ZoneName = "";
      }

      public void Load(BinaryReader reader)
      {
        this.ZoneID = reader.ReadUInt16();
        this.MiniMapID = reader.ReadUInt16();
        this.ZoneXOff = (uint) reader.ReadUInt16();
        this.ZoneYOff = (uint) reader.ReadUInt16();
        this._ZoneX = this.ZoneXOff * 8192U;
        this._ZoneY = this.ZoneYOff * 8192U;
        this.Unk1 = (uint) reader.ReadUInt16();
        this.Unk2 = (uint) reader.ReadUInt16();
        this.Unk3 = (uint) reader.ReadUInt16();
        this.Unk4 = (uint) reader.ReadUInt16();
        this.Unk5 = (uint) reader.ReadUInt16();
        this.Unk6 = (uint) reader.ReadUInt16();
        this.Unk7 = (uint) reader.ReadUInt16();
        this.Unk8 = (uint) reader.ReadUInt16();
        this.Unk9 = (uint) reader.ReadUInt16();
        this.Unk10 = (uint) reader.ReadUInt16();
        this.Unk11 = (uint) reader.ReadUInt16();
        this.ZoneName = FigleafReader.Manager.GetString(Language.english, "zones/zone_names.txt", (int) this.ZoneID, MythicPackage.DEV).Replace("^p,in", "").Replace("^n,in", "");
      }

      public void Save(BinaryWriter dataWriter)
      {
        dataWriter.Write(this.ZoneID);
        dataWriter.Write(this.MiniMapID);
        dataWriter.Write((ushort) this.ZoneXOff);
        dataWriter.Write((ushort) this.ZoneYOff);
        dataWriter.Write((ushort) this.Unk1);
        dataWriter.Write((ushort) this.Unk2);
        dataWriter.Write((ushort) this.Unk3);
        dataWriter.Write((ushort) this.Unk4);
        dataWriter.Write((ushort) this.Unk5);
        dataWriter.Write((ushort) this.Unk6);
        dataWriter.Write((ushort) this.Unk7);
        dataWriter.Write((ushort) this.Unk8);
        dataWriter.Write((ushort) this.Unk9);
        dataWriter.Write((ushort) this.Unk10);
        dataWriter.Write((ushort) this.Unk11);
      }
    }

    public class Regions : FigleafReader.Record
    {
      public List<FigleafReader.Regions.A04Data> A04List = new List<FigleafReader.Regions.A04Data>();
      public byte[] Data;

      public uint RegionID { get; set; }

      public uint A00 { get; set; }

      public uint A04 { get; set; }

      public uint A16 { get; set; }

      public uint A20 { get; set; }

      public uint Zones { get; set; }

      public int ZoneStartOffset { get; set; }

      public int ZoneEndOffset { get; set; }

      public List<FigleafReader.Zone> ZoneList
      {
        get
        {
          return this.TableData.Select<FigleafReader.TableData, FigleafReader.Zone>((Func<FigleafReader.TableData, FigleafReader.Zone>) (e => (FigleafReader.Zone) e)).ToList<FigleafReader.Zone>();
        }
      }

      public FigleafReader.Regions Clone(uint newRegionID)
      {
        FigleafReader.Regions regions = new FigleafReader.Regions()
        {
          RegionID = newRegionID,
          A00 = this.A00,
          A04 = this.A04,
          A16 = this.A16,
          A20 = this.A20,
          Zones = this.Zones
        };
        foreach (FigleafReader.Regions.A04Data a04 in this.A04List)
          regions.A04List.Add(new FigleafReader.Regions.A04Data()
          {
            Unk1 = a04.Unk1,
            Unk2 = a04.Unk2,
            Unk3 = a04.Unk3,
            Unk4 = a04.Unk4
          });
        foreach (FigleafReader.Zone zone in this.TableData)
          regions.TableData.Add((FigleafReader.TableData) new FigleafReader.Zone()
          {
            ZoneID = zone.ZoneID,
            MiniMapID = zone.MiniMapID,
            ZoneXOff = zone.ZoneXOff,
            ZoneYOff = zone.ZoneYOff,
            Unk1 = zone.Unk1,
            Unk2 = zone.Unk2,
            Unk3 = zone.Unk3,
            Unk4 = zone.Unk4,
            Unk5 = zone.Unk5,
            Unk6 = zone.Unk6,
            Unk7 = zone.Unk7,
            Unk8 = zone.Unk8,
            Unk9 = zone.Unk9,
            Unk10 = zone.Unk10,
            Unk11 = zone.Unk11
          });
        return regions;
      }

      public override void SetData(FigleafReader fr, byte[] data)
      {
        base.SetData(fr, data);
        BinaryReader binaryReader = new BinaryReader((Stream) new MemoryStream(data));
        this.Data = data;
        this.A00 = binaryReader.ReadUInt32();
        this.A04 = binaryReader.ReadUInt32();
        this.ZoneStartOffset = binaryReader.ReadInt32();
        this.RegionID = binaryReader.ReadUInt32();
        this.A16 = binaryReader.ReadUInt32();
        this.A20 = binaryReader.ReadUInt32();
        this.Zones = binaryReader.ReadUInt32();
        this.ZoneEndOffset = binaryReader.ReadInt32();
      }

      public override void SetTableData(byte[] data, int tableOffset, int recordIndex)
      {
        base.SetTableData(data, tableOffset, recordIndex);
        this.ZoneStartOffset -= tableOffset;
        this.ZoneEndOffset -= tableOffset;
        MemoryStream memoryStream = new MemoryStream(data);
        memoryStream.Position = (long) (this.ZoneStartOffset + 32 * recordIndex);
        BinaryReader reader = new BinaryReader((Stream) memoryStream);
        for (int index = 0; (long) index < (long) this.A04; ++index)
          this.A04List.Add(new FigleafReader.Regions.A04Data()
          {
            Unk1 = reader.ReadUInt32(),
            Unk2 = reader.ReadUInt32(),
            Unk3 = reader.ReadUInt32(),
            Unk4 = reader.ReadUInt32()
          });
        for (int index = 0; (long) index < (long) this.Zones; ++index)
        {
          FigleafReader.Zone zone = new FigleafReader.Zone();
          zone.Load(reader);
          this.TableData.Add((FigleafReader.TableData) zone);
        }
      }

      public static byte[] Serialize(List<FigleafReader.Regions> regions)
      {
        MemoryStream memoryStream = new MemoryStream();
        BinaryWriter dataWriter = new BinaryWriter((Stream) memoryStream);
        memoryStream.Write(new byte[32 * regions.Count], 0, 32 * regions.Count);
        for (int index1 = 0; index1 < regions.Count; ++index1)
        {
          FigleafReader.Regions region = regions[index1];
          long num1 = dataWriter.BaseStream.Position - (long) (index1 * 32);
          for (int index2 = 0; index2 < region.A04List.Count; ++index2)
            region.A04List[index2].Save(dataWriter);
          long num2 = dataWriter.BaseStream.Position - (long) (index1 * 32);
          foreach (FigleafReader.Zone zone in region.TableData)
            zone.Save(dataWriter);
          long position = dataWriter.BaseStream.Position;
          dataWriter.BaseStream.Position = (long) (index1 * 32);
          dataWriter.Write(region.A00);
          dataWriter.Write((uint) region.A04List.Count);
          dataWriter.Write((uint) num1);
          dataWriter.Write(region.RegionID);
          dataWriter.Write(region.A16);
          dataWriter.Write(region.A20);
          dataWriter.Write(region.Zones);
          dataWriter.Write((uint) num2);
          dataWriter.BaseStream.Position = position;
        }
        dataWriter.Write((byte) 0);
        dataWriter.Write((byte) 0);
        return memoryStream.ToArray();
      }

      public class A04Data
      {
        public uint Unk1 { get; set; }

        public uint Unk2 { get; set; }

        public uint Unk3 { get; set; }

        public uint Unk4 { get; set; }

        public void Save(BinaryWriter dataWriter)
        {
          dataWriter.Write(this.Unk1);
          dataWriter.Write(this.Unk2);
          dataWriter.Write(this.Unk3);
          dataWriter.Write(this.Unk4);
        }
      }
    }

    public class RecordList
    {
      public List<FigleafReader.Record> Records;
      public byte[] UnkData;
      public uint TableSize;
    }
  }
}
