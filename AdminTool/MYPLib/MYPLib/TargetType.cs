﻿// Decompiled with JetBrains decompiler
// Type: MYPLib.TargetType
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

namespace MYPLib
{
  public enum TargetType
  {
    NONE,
    ENEMY,
    SELF,
    ALLY,
    GROUP,
    PET,
    GROUND,
  }
}
