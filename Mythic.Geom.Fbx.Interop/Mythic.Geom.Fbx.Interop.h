// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the MYTHICGEOMFBXINTEROP_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// MYTHICGEOMFBXINTEROP_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef MYTHICGEOMFBXINTEROP_EXPORTS
#define MYTHICGEOMFBXINTEROP_API __declspec(dllexport)
#else
#define MYTHICGEOMFBXINTEROP_API __declspec(dllimport)
#endif

// This class is exported from the dll
class MYTHICGEOMFBXINTEROP_API CMythicGeomFbxInterop {
public:
	CMythicGeomFbxInterop(void);
	// TODO: add your methods here.
};

extern MYTHICGEOMFBXINTEROP_API int nMythicGeomFbxInterop;

MYTHICGEOMFBXINTEROP_API int fnMythicGeomFbxInterop(void);
