
#pragma once

#include "BSPNode.h"
#include "Config.h"

using namespace System;
using namespace System::Collections::Generic;

ref class Collision
{
public:

	Collision()
	{
		Vertices = gcnew List<Vector3>();
		Triangles = gcnew List<Triangle>();
	}

	void AddVertex(Vector3 v)
	{
		Vertices->Add(v);
	}

	unsigned int GetVertexCount()
	{
		return Vertices->Count;
	}


	Vector3 GetVertex(unsigned int i)
	{
		return Vertices[i];
	}


	int AddTriangle(Triangle t)
	{
		Triangles->Add(t);
		return Triangles->Count - 1;
	}


	unsigned int GetTriangleCount()
	{
		return Triangles->Count;
	}

	Triangle GetTriangle(unsigned int i)
	{
		return Triangles[i];
	}

	property BSPNode ^BSP;

private:

	List<Vector3> ^Vertices;
	List<Triangle> ^Triangles;
};