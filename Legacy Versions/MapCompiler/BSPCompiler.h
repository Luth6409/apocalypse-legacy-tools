
#pragma once

#include "Config.h"
#include "Triangle.h"
#include "Collision.h"
#include "BSPNode.h"

#include <limits>

ref class BSPCompiler
{
private:

	BSPNode ^_root;
	Config ^_config;
	Collision ^_collision;

public:

	BSPCompiler(Config ^config, Collision ^collision)
	{
		_config = config;
		_collision = collision;
	}

	BSPNode ^Compile()
	{
		_root = gcnew BSPNode();

		List<int> ^triangles = gcnew List<int>();
		for (int i = 0; i < (int)_collision->GetTriangleCount(); i++)
			triangles->Add(i);

		BuildBSPNode(_root, triangles, 0);
		return _root;
	}

private:

	ref class TriangleAreaComparer : public System::Collections::Generic::IComparer<int>
	{
	private:

		BSPCompiler ^_compiler;

	public:

		TriangleAreaComparer(BSPCompiler ^compiler)
		{
			_compiler = compiler;
		}

		virtual int Compare(int i, int j)
		{
			double a0 = _compiler->GetAreaOfTriangle(i);
			double a1 = _compiler->GetAreaOfTriangle(j);

			// List::Sort() sorts in increasing order, so the sense of
			// the comparison must be reversed here, in order to sort
			// the list of triangles by decreasing area

			if (a0 < a1)
				return 1;
			else if (a0 > a1)
				return -1;
			else
				return 0;
		}
	};

	double GetAreaOfTriangle(int index)
	{
		Triangle t = _collision->GetTriangle(index);
		Vector3 v0 = _collision->GetVertex(t.i0);
		Vector3 v1 = _collision->GetVertex(t.i1);
		Vector3 v2 = _collision->GetVertex(t.i2);
		Vector3 e0 = v1 - v0;
		Vector3 e1 = v2 - v0;
		return Vector3::Cross(e0, e1).Length;
	}

	void BuildBSPNode(BSPNode ^node, List<int> ^triangles, int depth)
	{
		if (triangles->Count == 0)
			return;

		if (depth >= _config->BSPDepthCutoff || triangles->Count <= _config->BSPTrianglesCutoff)
		{
			node->Triangles = triangles;

			// Sort triangles by area, largest first
			node->Triangles->Sort(gcnew TriangleAreaComparer(this));
		}
		else
		{
			ChooseSplitPlane(node, triangles);

			node->Expand();

			List<int> ^trianglesFront = gcnew List<int>(), ^trianglesBack = gcnew List<int>();

			for each (int i in triangles)
			{
				Triangle t = _collision->GetTriangle(i);

				if (!t.IsDegenerate())
				{
					Vector3 v0 = _collision->GetVertex(t.i0),
						v1 = _collision->GetVertex(t.i1),
						v2 = _collision->GetVertex(t.i2);

					// FIXME: test vertex positions as well for degenerate case

					int result = TrianglePlaneTest(v0, v1, v2, node->P);

					if (result >= 0)
						trianglesFront->Add(i);

					if (result <= 0)
						trianglesBack->Add(i);
				}
			}

			triangles->Clear();

			BuildBSPNode(node->Back, trianglesBack, depth + 1);
			BuildBSPNode(node->Front, trianglesFront, depth + 1);
		}
	}

	void ChooseSplitPlane(BSPNode ^node, List<int> ^triangles)
	{
		// Compute bounding box of all triangles
		Triangle t0 = _collision->GetTriangle(triangles[0]);
		Vector3 v0 = _collision->GetVertex(t0.i0);

		float xmin = v0.X, ymin = v0.Y, zmin = v0.Z, xmax = xmin, ymax = ymin, zmax = zmin;

		for each (int i in triangles)
		{
			Triangle t = _collision->GetTriangle(i);

			if (!t.IsDegenerate())
			{
				Vector3 v0 = _collision->GetVertex(t.i0),
					v1 = _collision->GetVertex(t.i1),
					v2 = _collision->GetVertex(t.i2);

				xmin = Math::Min(xmin, v0.X);
				xmax = Math::Max(xmax, v0.X);

				ymin = Math::Min(ymin, v0.Y);
				ymax = Math::Max(ymax, v0.Y);

				zmin = Math::Min(zmin, v0.Z);
				zmax = Math::Max(zmax, v0.Z);
			}
		}

		::Plane bestPlane;
		float bestMetric = std::numeric_limits<float>::max();

		int divisions = 10; // FIXME: this needs to come from config

		for (int i = 1; i < divisions; i++)
		{
			float t = i / (float)divisions;
			::Plane plane;
			plane.N = Vector3(1, 0, 0);
			plane.D = t * xmin + (1 - t) * xmax;
			float metric = SplitPlaneMetric(plane, triangles);
			if (metric < bestMetric)
			{
				bestPlane = plane;
				bestMetric = metric;
			}
		}

		for (int i = 1; i < divisions; i++)
		{
			float t = i / (float)divisions;
			::Plane plane;
			plane.N = Vector3(0, 1, 0);
			plane.D = t * ymin + (1 - t) * ymax;
			float metric = SplitPlaneMetric(plane, triangles);
			if (metric < bestMetric)
			{
				bestPlane = plane;
				bestMetric = metric;
			}
		}

		for (int i = 1; i < divisions; i++)
		{
			float t = i / (float)divisions;
			::Plane plane;
			plane.N = Vector3(0, 0, 1);
			plane.D = t * zmin + (1 - t) * zmax;
			float metric = SplitPlaneMetric(plane, triangles);
			if (metric < bestMetric)
			{
				bestPlane = plane;
				bestMetric = metric;
			}
		}

		node->P = bestPlane;
	}

	// Returns the metric for a split plane, a lower metric indicates a better split plane
	float SplitPlaneMetric(Plane p, List<int> ^triangles)
	{
		int front = 0, back = 0, span = 0;

		for each (int i in triangles)
		{
			Triangle t = _collision->GetTriangle(i);

			if (!t.IsDegenerate())
			{
				Vector3 v0 = _collision->GetVertex(t.i0),
					v1 = _collision->GetVertex(t.i1),
					v2 = _collision->GetVertex(t.i2);

				int result = TrianglePlaneTest(v0, v1, v2, p);

				switch (result)
				{
				case -1:
					back++;
					break;
				case 0:
					span++;
					break;
				case 1:
					front++;
					break;
				}
			}
		}

		// HACK: this is just a test metric
		return Math::Max(front, back) + (float)span;
	}

	int TrianglePlaneTest(Vector3 v0, Vector3 v1, Vector3 v2, ::Plane p)
	{
		int r0 = PointPlaneTest(v0, p);
		int r1 = PointPlaneTest(v1, p);
		int r2 = PointPlaneTest(v2, p);

		Byte back = 0, front = 0;

		if (r0 == -1)
			back++;
		else if (r0 == 1)
			front++;

		if (r1 == -1)
			back++;
		else if (r1 == 1)
			front++;

		if (r2 == -1)
			back++;
		else if (r2 == 1)
			front++;

		if (back > 0 && front == 0)
			return -1;
		else if (front > 0 && back == 0)
			return 1;
		else
			return 0;
	}

	int PointPlaneTest(Vector3 point, ::Plane plane)
	{
		float epsilon = 0.0001f;

		float d0 = Vector3::Dot(point, plane.N) - plane.D;

		if (d0 < -epsilon)
			return -1;
		else if (d0 > epsilon)
			return 1;
		else
			return 0;
	}
};