
#pragma once

using namespace System::Collections::Generic;

ref class Config
{
public:

	Config()
	{
		// ZoneDataPath = "";
		//FixturesPath = "";
		MypsPath = "";

		BSPDepthCutoff = 20;
		BSPTrianglesCutoff = 250;

		StrictMode = true;
		ExportNIFS = false;
		ExportTerrain = false;
		ExportFixtures = false;

		ExcludeList = gcnew List<String ^>();
	}


	Config(Config ^c)
	{
		// ZoneDataPath = c->ZoneDataPath;
		// FixturesPath = c->FixturesPath;
		MypsPath = c->MypsPath;

		BSPDepthCutoff = c->BSPDepthCutoff;
		BSPTrianglesCutoff = c->BSPTrianglesCutoff;

		StrictMode = c->StrictMode;
		ExportNIFS = c->ExportNIFS;
		ExportTerrain = c->ExportTerrain;
		ExportFixtures = c->ExportFixtures;
		ZoneDataPath = c->ZoneDataPath;
		FixtureFilter = c->FixtureFilter;
		Region = c->Region;
		ExcludeList = gcnew List<String ^>(c->ExcludeList);
	}

	String ^MypsPath, ^ZoneDataPath, ^FixtureFilter, ^Region; //^ZoneDataPath, ^FixturesPath, 
	int BSPDepthCutoff, BSPTrianglesCutoff;
	bool StrictMode, ExportNIFS, ExportTerrain, ExportFixtures;
	List<String ^> ^ExcludeList;
};
