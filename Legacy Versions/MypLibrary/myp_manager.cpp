#include "platform.h"

#include "zlib.h"

#include "myp.h"

#include "myp_manager.h"


MypManager::MypManager(LogCallback logCB, ProgressCallback progressCB)
{
    _logCallBack = logCB;
    _progressCallBack = progressCB;
    _path = "";
}

uint32_t MypManager::Load(std::string warpath) {
    //OutputDebugStringA(std::string("Myp Manager: Loading").c_str());

    //std::filesystem::path path = std::filesystem::path(warpath, std::filesystem::path::generic_format);
    //std::vector<std::string> files = std::vector<std::string>();

    //_path = path.native.string();

    //for (auto& p : std::filesystem::directory_iterator("sandbox"))
    //    files.emplace_back(p.path().string());

    //if (files.size() == 0)
    //    OutputDebugStringA(std::string("No files in directory" + warpath + " ").c_str());





    //_errorMyp = "";
 //   std::set<char> delims;
 //   delims.insert('\\');
 //   delims.insert('/');
 //   _allLoaded = true;

    //int MypCount = 0;
    //for (int i = 0; i < (int)files.size(); i++) {
    //    std::string path = Util::toUpper(files[i]);
    //    std::vector<std::string> parts = Util::splitpath(path, delims);

    //    auto filename = Util::toUpper(parts[parts.size() - 1]);
    //    if (Util::EndsWith(parts[parts.size() - 1], ".Myp")) {
    //        MypCount++;
    //    }
    //}




    //uint32_t result = 0;
    //int index = 0;
    //for (int i = 0; i < (int)files.size(); i++) {
    //    std::string path = Util::toUpper(files[i]);
    //    std::vector<std::string> parts = Util::splitpath(path, delims);

    //    auto filename = Util::toUpper(parts[parts.size() - 1]);

    //    if (Util::EndsWith(parts[parts.size() - 1], ".Myp")) {
    //        auto archive = Util::strToArchive(parts[parts.size() - 1].c_str());

    //        if (archive != Archive::ARCHIVE_MFT) {
    //            if (_Myps.find(archive) != _Myps.end() && _Myps[archive] != NULL) {
    //                delete _Myps[archive];
    //                _Myps[archive] = NULL;
    //            }
    //            //Util::SetFileReadOnly(path, false);

    //            _Myps[archive] = new Myp(_logger, path.c_str());
    //            if (!_Myps[archive]->isLoaded()) {
    //                _errorMyp += Util::ArchiveToStr(archive) + " ";
    //                _allLoaded = false;
    //                continue;
    //                //CloseMyps();
    //                //return false;
    //            }

    //            _logger(LogType::LogType_PATCHER, LogLevel::LogLevel_INFO, "gethashash");

    //            uint32_t hashHash = GetHashHash(_Myps[archive]);
    //            _logger(LogType::LogType_PATCHER, LogLevel::LogLevel_INFO, (std::string(path + " count:"
    //                    + std::to_string(_Myps[archive]->entryCount()) + " hash_hash:" + std::to_string(hashHash) + " extrace_size:" + std::to_string(_Myps[archive]->GetArchiveSize()))).c_str());
    //            _MypHashes[(int)(Util::strToArchive(Util::GetFilename(path).c_str()))] = hashHash;
    //            result |= (1 << (uint32_t)archive);

    //        }

    //        if (pb != nullptr) {
    //            pb(path.c_str(), index, MypCount);
    //        }
    //        index++;
    //    }
    //}
    //if (pb != nullptr) {
    //    pb("", index, MypCount);
    //}

    //_logger(LogType::LogType_PATCHER, LogLevel::LogLevel_INFO, (std::string("Myp Manager: Load completed")).c_str());

    return 0;
}


std::optional<std::map<std::string, std::vector<int32_t>>> GetFileHashe(Myp* myp) {
    //uint32_t hashash = 0;
    //for (auto& asset : Myp->GetAssets()) {
    //    if (asset.second->Offset1 != 0 || asset.second->Offset2 != 0) {
    //        hashash = adler32(hashash, (const uint8_t*)&asset.second->CRC32, 4);
    //        hashash = adler32(hashash, (const uint8_t*)&asset.second->UnCompressedSize, 4);
    //    }
    //}
    //return hashash;
}

std::optional<uint32_t> MypManager::GetHash(Myp* Myp) {
    //uint32_t hashash = 0;
    //for (auto& asset : Myp->GetAssets()) {
    //    if (asset.second->Offset1 != 0 || asset.second->Offset2 != 0) {
    //        hashash = adler32(hashash, (const uint8_t*)&asset.second->CRC32, 4);
    //        hashash = adler32(hashash, (const uint8_t*)&asset.second->UnCompressedSize, 4);
    //    }
    //}
    //return hashash;
}

bool MypManager::IsLoaded() const
{
    return false;
}

void MypManager::Close() {
    //for (auto& Myp : _Myps) {
    //    OutputDebugStringA(std::string("...try close " + Myp.second->getFileName()).c_str());
    //    Myp.second->Close();
    //}
}

void MypManager::Save() {
    //for (auto &h : _Myps) {
    //    h.second->Save();
    //}
}

std::optional<Myp*> MypManager::GetMyp(std::string name, bool create) {
    //_Myps[]

    /*if (_Myps.find(archive) != _Myps.end() && _Myps[archive] != NULL) {
        if (!_Myps[archive]->isLoaded())
            _Myps[archive]->Load();
        return _Myps[archive];
    } else if (create) {
        std::string path = Util::toLower(Util::pathAppend(_path, Util::ArchiveToStr(archive)));
        _Myps[archive] = new Myp(_logger, path.c_str(), true);
        _MypHashes[(int)(Util::strToArchive(Util::GetFilename(path).c_str()))] = 0;
        return _Myps[archive];
    }
    return NULL;
  */
}

std::optional<std::vector<Myp*>> MypManager::GetMyps(bool create) {
    //std::vector<Myp*> list;
    //for (auto& keyval : _Myps) {
    //    if (keyval.second != NULL && keyval.second->isLoaded())
    //        list.push_back(keyval.second);
    //}
}

uint8_t* MypManager::GetAssetData(const char* name) {
    //int size = 0;
    //Archive archive;
    //return GetAssetData(name, size, archive);
    return nullptr;
}

uint8_t* MypManager::GetAssetData(const char* name, int& size, Myp* firstArchive) {
    //FileEntry* found = NULL;
    //Myp* archive = GetArchive(Archive::ARCHIVE_DEV, false);

    //if (firstArchive != NULL) {
    //    found = firstArchive->GetAsset(name);
    //    archive = firstArchive;
    //}
    //if (found == NULL && archive != NULL)
    //    found = archive->GetAsset(name);

    //if (found == NULL) {
    //    for (auto& keyval : _Myps) {
    //        if (keyval.first != Archive::ARCHIVE_DEV && keyval.first != Archive::ARCHIVE_WARTEST &&  _Myps[keyval.first] != NULL && _Myps[keyval.first]->isLoaded()) {
    //            archive = keyval.second;
    //            found = _Myps[keyval.first]->GetAsset(name);
    //            if (found != NULL)
    //                break;
    //        }
    //    }
    //}

    //if (found != NULL) {
    //    archiveId = archive->getArchiveId();
    //    uint8_t* data = new uint8_t[found->UnCompressedSize];
    //    archive->GetAssetData(found, data);
    //    size = found->UnCompressedSize;
    //    return data;
    //}
    //size = 0;
    return 0;
}
