﻿using CefSharp;

namespace Launcher
{
    public class JsonResourceHandlerFactory : IResourceHandlerFactory
    {
        bool IResourceHandlerFactory.HasHandlers => true;

        IResourceHandler IResourceHandlerFactory.GetResourceHandler(IWebBrowser browserControl, IBrowser browser, IFrame frame, IRequest request)
        {
            if (request.Url.Contains(""))
            {
                return new JsonResourceHandler();
            }
            return null;
        }
    }
}
