int __userpurge sub_54926E@<eax>(int a1@<esi>, int a2, _DWORD *a3)
{
  int v3; // ecx
  int *v4; // eax
  int v5; // edx
  int *v6; // eax
  int v7; // eax
  int i; // eax
  bool v10; // [esp+8h] [ebp-4h]

  v3 = *(a2 + 4);
  v4 = *(v3 + 4);
  v10 = 1;
  while ( !*(v4 + 21) )
  {
    v3 = v4;
    v10 = *a3 < v4[3];
    if ( v10 )
      v4 = *v4;
    else
      v4 = v4[2];
  }
  v5 = v3;
  if ( v10 )
  {
    if ( v3 == **(a2 + 4) )
    {
      v6 = sub_62EC08(a2, &a3, 1, v3, a3);
      goto LABEL_9;
    }
    if ( *(v3 + 21) )
    {
      v5 = *(v3 + 8);
    }
    else
    {
      v7 = *v3;
      if ( *(*v3 + 21) )
      {
        for ( i = *(v3 + 4); !*(i + 21) && v5 == *i; i = *(i + 4) )
          v5 = i;
        if ( !*(v5 + 21) )
          v5 = i;
      }
      else
      {
        do
        {
          v5 = v7;
          v7 = *(v7 + 8);
        }
        while ( !*(v7 + 21) );
      }
    }
  }
  if ( *(v5 + 12) >= *a3 )
  {
    *a1 = v5;
    *(a1 + 4) = 0;
    return a1;
  }
  v6 = sub_62EC08(a2, &a3, v10, v3, a3);
LABEL_9:
  *a1 = *v6;
  *(a1 + 4) = 1;
  return a1;
}