#pragma once

#include <fbxsdk.h>
#include "../Property.h"

using namespace System;

namespace UnexpectedBytes
{
	public value struct Vector2
	{
	public:

		property double X;
		property double Y;

		Vector2(double x, double y)
		{
			X = x;
			Y = y;
		}

		operator FbxDouble2()
		{
			return FbxDouble2(X, Y);
		}

		virtual String ^ToString() override
		{
			return String::Format("{0}, {1}", Math::Round(X, 3), Math::Round(Y, 3));
		}

		static Vector2 operator *(Vector2 self, float value)
		{
			self.X *= value;
			self.Y *= value;
			return self;
		}

	internal:

		Vector2(FbxDouble2 vector)
		{
			X = vector[0];
			Y = vector[1];
		}
	};
}
